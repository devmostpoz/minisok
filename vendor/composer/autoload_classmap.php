<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'AdminConferenceController' => $baseDir . '/app/controllers/admin/AdminConferenceContoller.php',
    'AdminMessagesController' => $baseDir . '/app/controllers/admin/AdminMessagesController.php',
    'AdminPasswordsController' => $baseDir . '/app/controllers/admin/AdminPasswordsController.php',
    'AdminRemindersController' => $baseDir . '/app/controllers/admin/AdminRemindersController.php',
    'AdminSuperAdminController' => $baseDir . '/app/controllers/admin/AdminSuperAdminController.php',
    'AdminUserController' => $baseDir . '/app/controllers/admin/AdminUserController.php',
    'BaseController' => $baseDir . '/app/controllers/BaseController.php',
    'Conference' => $baseDir . '/app/models/Conference.php',
    'ConferenceController' => $baseDir . '/app/controllers/ConferenceController.php',
    'ConferenceRegisteredUsers' => $baseDir . '/app/models/ConferenceRegisteredUsers.php',
    'ConferencesLog' => $baseDir . '/app/models/ConferencesLog.php',
    'CreatePasswordRemindersTable' => $baseDir . '/app/database/migrations/2014_12_10_083924_create_password_reminders_table.php',
    'CreateUsersTable' => $baseDir . '/app/database/migrations/2014_12_03_112511_create_users_table.php',
    'DatabaseSeeder' => $baseDir . '/app/database/seeds/DatabaseSeeder.php',
    'ExportCsv' => $baseDir . '/app/models/ExportCsv.php',
    'ExportCsvController' => $baseDir . '/app/controllers/ExportCsvController.php',
    'HomeController' => $baseDir . '/app/controllers/HomeController.php',
    'IlluminateQueueClosure' => $vendorDir . '/laravel/framework/src/Illuminate/Queue/IlluminateQueueClosure.php',
    'Image' => $baseDir . '/app/models/Image.php',
    'Messages' => $baseDir . '/app/models/Messages.php',
    'RegisteredUser' => $baseDir . '/app/models/RegisteredUser.php',
    'RemindersController' => $baseDir . '/app/controllers/RemindersController.php',
    'SessionHandlerInterface' => $vendorDir . '/symfony/http-foundation/Symfony/Component/HttpFoundation/Resources/stubs/SessionHandlerInterface.php',
    'TestCase' => $baseDir . '/app/tests/TestCase.php',
    'User' => $baseDir . '/app/models/User.php',
    'Whoops\\Module' => $vendorDir . '/filp/whoops/src/deprecated/Zend/Module.php',
    'Whoops\\Provider\\Zend\\ExceptionStrategy' => $vendorDir . '/filp/whoops/src/deprecated/Zend/ExceptionStrategy.php',
    'Whoops\\Provider\\Zend\\RouteNotFoundStrategy' => $vendorDir . '/filp/whoops/src/deprecated/Zend/RouteNotFoundStrategy.php',
);
