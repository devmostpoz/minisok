$(document).ready(function(){
    $('#clock').countdown('2015/01/31').on('update.countdown', function(event) {
           var $this = $(this).html(event.strftime(''
             + '<span>%-w</span> tyg '
             + '<span>%-d</span> d '
			 + '<span>%H</span> h '
		 	 + '<span>%M</span> min '
			 + '<span>%S</span> s'));     
  	});
  	$('#clock2').countdown('2014/12/31').on('update.countdown', function(event) {
           var $this = $(this).html(event.strftime(''
             + '<span>%-w</span> tyg '
             + '<span>%-d</span> d '
			 + '<span>%H</span> h '
		 	 + '<span>%M</span> min '
			 + '<span>%S</span> s'));     
  	});
  	$('#clock3').countdown('2015/09/17').on('update.countdown', function(event) {
           var $this = $(this).html(event.strftime(''
             + '<span>%-w</span> tyg '
             + '<span>%-d</span> d '
			 + '<span>%H</span> h '
		 	 + '<span>%M</span> min '
			 + '<span>%S</span> s'));     
  	});
  	$('#clock4').countdown('2015/08/17').on('update.countdown', function(event) {
           var $this = $(this).html(event.strftime(''
             + '<span>%-w</span> tyg '
             + '<span>%-d</span> d '
			 + '<span>%H</span> h '
		 	 + '<span>%M</span> min '
			 + '<span>%S</span> s'));     
  	});
  	$('.other-show').on("click", function(){
  		$('.other').slideToggle(300);
  	});
})