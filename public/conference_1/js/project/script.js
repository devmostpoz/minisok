$(document).ready(function(){
    $( "#formKontakt" ).submit(function() {
            $('.imgLoader').show();
            var email = $('input[name=email]').val();
            var name = $('input[name=name]').val();
            var text = $('textarea').val();
            var id = $('input[name=id]').val();
            var url = $('input[name=link]').val();
            $.ajax({
                url: url+ '/messages/' + id,
                type: 'POST',
                data: { email:email, name:name, text:text},
                success: function (response) { 
                    $('.imgLoader').hide();
                    if(response == 'ok') {
                        $('span.success').show().delay(2000).fadeOut(1000);
                        $('input[name=email]').val('');
                        $('input[name=name]').val('');
                        $('textarea').val('');
                    } else
                        $('span.error').html(response).show().delay(2000).fadeOut(1000);
                }
            }); 
            return false;
        
    });
});