$(document).ready(function(){

$(document).on("keyup",".input-mini", function(e){
//        console.log(e.keyCode);
        $('.input-mini').each(function(){
            if($(this).val().length>1){
                    $(this).val($(this).val().substring(0,1));
            }
        });
        if((e.keyCode>=48 && e.keyCode<=57) || e.keyCode == 37 || e.keyCode == 8 || (e.keyCode>=96 && e.keyCode<=105)) {
            
            var code = parseInt(e.keyCode);
            if(code == 8 || code == 37) {
              $(this).prev().select();
            }
            else {
              $(this).next().select();
            }
        } else 
            $(this).val('');
    });
    
    $('.input-smaller').on("keyup", function(e){
        var code = parseInt(e.keyCode);
        $('.input-smaller').each(function(){
            if($(this).val().length>1){
                    $(this).val($(this).val().substring(0,1));
            }
        });
        if((e.keyCode>=48 && e.keyCode<=57) || e.keyCode == 37 || e.keyCode == 8 || (e.keyCode>=96 && e.keyCode<=105)) {
            if(code == 8 || code == 37) {
              $(this).prev().select();
            }
            else {
              $(this).next().select();
            }
        } else 
            $(this).val('');
    });
    
    
    $('.addAddress').on("click", function(){
       if($('.address1').hasClass('hide')){
           $('.address1').removeClass('hide');
           $('.address0 .addAddress').hide();
           $('.address0 .removeAddress').hide();
       } else if($('.address2').hasClass('hide')){
           $('.address2').removeClass('hide');
           $('.address1 .addAddress').hide();
           $('.address1 .removeAddress').hide();
       } else if($('.address3').hasClass('hide')){
           $('.address3').removeClass('hide');
           $('.address2 .addAddress').hide();
           $('.address2 .removeAddress').hide();
       } else if($('.address4').hasClass('hide')){
           $('.address4').removeClass('hide');
           $('.address3 .addAddress').hide();
           $('.address3 .removeAddress').hide();
           $('.address4 .addAddress').hide();
       } 
       return false;
    });
    
    $('.removeAddress').on("click", function(){
       if(!$('.address4').hasClass('hide')){
           $('.address4').addClass('hide');
           $('.address3 .addAddress').show();
           $('.address3 .removeAddress').show();
           $('.address4 input').val('');
           $('.address4 input').removeProp('checked');
       } else if(!$('.address3').hasClass('hide')){
           $('.address3').addClass('hide');
           $('.address2 .addAddress').show();
           $('.address2 .removeAddress').show();
           $('.address3 input').val('');
           $('.address3 input').removeProp('checked');
       } else if(!$('.address2').hasClass('hide')){
           $('.address2').addClass('hide');
           $('.address1 .addAddress').show();
           $('.address1 .removeAddress').show();
           $('.address2 input').val('');
           $('.address2 input').removeProp('checked');
       } else if(!$('.address1').hasClass('hide')){
           $('.address1').addClass('hide');
           $('.address0 .addAddress').show();
           $('.address1 input').val('');
           $('.address1 input').removeProp('checked');
       } 
       return false;
    });
    
    if($('.address4 input[name="street_name[4]"').val().length>0){
        $('.address4').removeClass('hide');
        $('.address3').removeClass('hide');
        $('.address2').removeClass('hide');
        $('.address1').removeClass('hide');
        $('.address4 .addAddress').hide();
        $('.address3 .addAddress').hide();
        $('.address2 .addAddress').hide();
        $('.address1 .addAddress').hide();
        $('.address0 .addAddress').hide();
        $('.address3 .removeAddress').hide();
        $('.address2 .removeAddress').hide();
        $('.address1 .removeAddress').hide();
        $('.address0 .removeAddress').hide();
    } else if($('.address3 input[name="street_name[3]"').val().length>0){
        $('.address3').removeClass('hide');
        $('.address2').removeClass('hide');
        $('.address1').removeClass('hide');
        $('.address2 .addAddress').hide();
        $('.address1 .addAddress').hide();
        $('.address0 .addAddress').hide();
        $('.address2 .removeAddress').hide();
        $('.address1 .removeAddress').hide();
        $('.address0 .removeAddress').hide();
    } else if($('.address2 input[name="street_name[2]"').val().length>0){
        $('.address2').removeClass('hide');
        $('.address1').removeClass('hide');
        $('.address1 .addAddress').hide();
        $('.address0 .addAddress').hide();
        $('.address1 .removeAddress').hide();
        $('.address0 .removeAddress').hide();
    } else if($('.address1 input[name="street_name[1]"').val().length>0){
        $('.address1').removeClass('hide');
        $('.address0 .addAddress').hide();
        $('.address0 .removeAddress').hide();
    }
        
    $('.addSpecialization').on("click", function(){
        if($('.spec3').hasClass('hide')){
            $('.spec3').removeClass('hide');
        } else if($('.spec4').hasClass('hide')){
            $('.spec4').removeClass('hide');
            $(this).css({"margin-top":"10px"});
        } else if($('.spec5').hasClass('hide')){
            $('.spec5').removeClass('hide');
            $(this).hide();
        }
        return false;
    });
    
    if($('[name="specialization[4]"] option:selected').val().length>0){
        $('.spec5').removeClass('hide');
        $('.spec4').removeClass('hide');
        $('.spec3').removeClass('hide');
        $('.addSpecialization').hide();
    } else if($('[name="specialization[3]"] option:selected').val().length>0){
        $('.spec4').removeClass('hide');
        $('.spec3').removeClass('hide');
        $('.addSpecialization').css({"margin-top":"10px"});
    } else if($('[name="specialization[2]"] option:selected').val().length>0){
        $('.spec3').removeClass('hide');
    }
    
    $('.addTelephone').on("click", function(){
        if($('.tel1').hasClass('hide')){
            $('.tel1').removeClass('hide');
        } else if($('.tel2').hasClass('hide')){
            $('.tel2').removeClass('hide');
            $(this).css({"margin-top":"10px"});
        } else if($('.tel3').hasClass('hide')){
            $('.tel3').removeClass('hide');
             $(this).css({"margin-top":"0px"});
        } else if($('.tel4').hasClass('hide')){
            $('.tel4').removeClass('hide');
            $(this).hide();
        }
        return false;
    });
    
    if($('input[name="telephone[4]"]').val().length>0){
        $('.tel4').removeClass('hide');
        $('.tel3').removeClass('hide');
        $('.tel2').removeClass('hide');
        $('.tel1').removeClass('hide');
        $('.addTelephone').hide();
    } else if($('input[name="telephone[3]"]').val().length>0){
        $('.tel3').removeClass('hide');
        $('.tel2').removeClass('hide');
        $('.tel1').removeClass('hide');
        $('.addTelephone').css({"margin-top":"0px"});
    } else if($('input[name="telephone[2]"]').val().length>0){
        $('.tel2').removeClass('hide');
        $('.tel1').removeClass('hide');
        $('.addTelephone').css({"margin-top":"10px"});
    } else if($('input[name="telephone[1]"]').val().length>0){
        $('.tel1').removeClass('hide');
    }
    
    
    $('.addMain_job').on("click", function(){
       if($('.main_job1').hasClass('hide')){
           $('.main_job1').removeClass('hide');
           $('.main_job0 .addMain_job').hide();
           $('.main_job0 .removeMain_job').hide();
       } else if($('.main_job2').hasClass('hide')){
           $('.main_job2').removeClass('hide');
           $('.main_job1 .addMain_job').hide();
           $('.main_job1 .removeMain_job').hide();
       } else if($('.main_job3').hasClass('hide')){
           $('.main_job3').removeClass('hide');
           $('.main_job2 .addMain_job').hide();
           $('.main_job2 .removeMain_job').hide();
       } else if($('.main_job4').hasClass('hide')){
           $('.main_job4').removeClass('hide');
           $('.main_job3 .addMain_job').hide();
           $('.main_job3 .removeMain_job').hide();
           $('.main_job4 .addMain_job').hide();
       } 
       return false;
    });
    
    $('.removeMain_job').on("click", function(){
       if(!$('.main_job4').hasClass('hide')){
           $('.main_job4').addClass('hide');
           $('.main_job3 .addMain_job').show();
           $('.main_job3 .removeMain_job').show();
           $('.main_job4 input').val('');
           $('.main_job4 input').removeProp('checked');
       } else if(!$('.main_job3').hasClass('hide')){
           $('.main_job3').addClass('hide');
           $('.main_job2 .addMain_job').show();
           $('.main_job2 .removeMain_job').show();
           $('.main_job3 input').val('');
           $('.main_job3 input').removeProp('checked');
       } else if(!$('.main_job2').hasClass('hide')){
           $('.main_job2').addClass('hide');
           $('.main_job1 .addMain_job').show();
           $('.main_job1 .removeMain_job').show();
           $('.main_job2 input').val('');
           $('.main_job2 input').removeProp('checked');
       } else if(!$('.main_job1').hasClass('hide')){
           $('.main_job1').addClass('hide');
           $('.main_job0 .addMain_job').show();
           $('.main_job1 input').val('');
           $('.main_job1 input').removeProp('checked');
       } 
       return false;
    });
    
    if($('.main_job4 input[name="unit_name[4]"').val().length>0){
        $('.main_job4').removeClass('hide');
        $('.main_job3').removeClass('hide');
        $('.main_job2').removeClass('hide');
        $('.main_job1').removeClass('hide');
        $('.main_job4 .addMain_job').hide();
        $('.main_job3 .addMain_job').hide();
        $('.main_job2 .addMain_job').hide();
        $('.main_job1 .addMain_job').hide();
        $('.main_job0 .addMain_job').hide();
        $('.main_job3 .removeMain_job').hide();
        $('.main_job2 .removeMain_job').hide();
        $('.main_job1 .removeMain_job').hide();
        $('.main_job0 .removeMain_job').hide();
    } else if($('.main_job3 input[name="unit_name[3]"').val().length>0){
        $('.main_job3').removeClass('hide');
        $('.main_job2').removeClass('hide');
        $('.main_job1').removeClass('hide');
        $('.main_job2 .addMain_job').hide();
        $('.main_job1 .addMain_job').hide();
        $('.main_job0 .addMain_job').hide();
        $('.main_job2 .removeMain_job').hide();
        $('.main_job1 .removeMain_job').hide();
        $('.main_job0 .removeMain_job').hide();
    } else if($('.main_job2 input[name="unit_name[2]"').val().length>0){
        $('.main_job2').removeClass('hide');
        $('.main_job1').removeClass('hide');
        $('.main_job1 .addMain_job').hide();
        $('.main_job0 .addMain_job').hide();
        $('.main_job1 .removeMain_job').hide();
        $('.main_job0 .removeMain_job').hide();
    } else if($('.main_job1 input[name="unit_name[1]"').val().length>0){
        $('.main_job1').removeClass('hide');
        $('.main_job0 .addMain_job').hide();
        $('.main_job0 .removeMain_job').hide();
    }
    
    $('.addSpecialization2').on("click", function(){
        if($('.spec22').hasClass('hide')){
            $('.spec22').removeClass('hide');
        } else if($('.spec23').hasClass('hide')){
            $('.spec23').removeClass('hide');
            $(this).css({"margin-top":"10px"});
        } else if($('.spec24').hasClass('hide')){
            $('.spec24').removeClass('hide');
            
        } else if($('.spec25').hasClass('hide')){
            $('.spec25').removeClass('hide');
            $(this).hide();
        }
        return false;
    });
    
    if($('[name="interested_specialization[4]"] option:selected').val().length>0){
        $('.spec25').removeClass('hide');
        $('.spec24').removeClass('hide');
        $('.spec23').removeClass('hide');
        $('.spec22').removeClass('hide');
        $('.addSpecialization2').hide();
    } else if($('[name="interested_specialization[3]"] option:selected').val().length>0){
        $('.spec24').removeClass('hide');
        $('.spec23').removeClass('hide');
        $('.spec22').removeClass('hide');
        $('.addSpecialization2').css({"margin-top":"10px"});
    } else if($('[name="interested_specialization[2]"] option:selected').val().length>0){
        $('.spec23').removeClass('hide');
        $('.spec22').removeClass('hide');
    } else if($('[name="interested_specialization[1]"] option:selected').val().length>0){
        $('.spec22').removeClass('hide');
    }
    
});