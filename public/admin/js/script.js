$(document).ready(function(){
    var PATH = $(location).attr('pathname');
    var url = $('#url').val();
    $('#side-menu a').each(function(){
        var href = $(this).attr('href');
        href = href.substring(url.length);
        
        if (PATH.toLowerCase().indexOf(href.toLowerCase()) >= 0 && href.length>0)
                $(this).addClass('active');
    });
});