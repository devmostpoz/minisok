<?php


Route::get('/', function(){
	return 'strona w budowie';
});
Route::get('/success', function(){
	return 'strona w budowie';
});
Route::get('/ordernotify', function(){
	return 'strona w budowie';
});
Route::get('_amoksiklav','ConferenceController@showAmoksiklav');

Route::get('konferencje/{name}','ConferenceController@conference');
Route::get('szkoly/{name}','ConferenceController@conference');
Route::get('konferencje/{name}/formularz','ConferenceController@form');
Route::get('szkoly/{name}/formularz','ConferenceController@form');
Route::get('konferencje/{name}/formularz-sukces','ConferenceController@formSuccess');
Route::get('szkoly/{name}/formularz-sukces','ConferenceController@formSuccess');
Route::get('konferencje/{name}/rejestracja','ConferenceController@conferenceRegister')->before('regular_user');
Route::get('szkoly/{name}/rejestracja','ConferenceController@conferenceRegister')->before('regular_user');
Route::post('konferencje/{name}/rejestracja','ConferenceController@postConferenceRegister')->before('regular_user');
Route::post('szkoly/{name}/rejestracja','ConferenceController@postConferenceRegister')->before('regular_user');

Route::post('konferencje/{name}/formularz','ConferenceController@create');
Route::post('szkoly/{name}/formularz','ConferenceController@create');
Route::post('konferencje/{name}/login', 'ConferenceController@postLogin');
Route::post('szkoly/{name}/login', 'ConferenceController@postLogin');

Route::post('konferencje/{name}/rejestracja/potwierdzenie', 'ConferenceController@confirmRegister');
Route::post('konferencje/{name}/rejestracja/zlozenie_platnosci', 'ConferenceController@ordered');
Route::post('konferencje/{name}', 'PasswordController@reminder');
Route::post('szkoly/{name}', 'PasswordController@reminder');
Route::post('messages/{id}','ConferenceController@messages');
Route::post('password/remind', 'RemindersController@postRemind');


//////
Route::get('conferences/{name}','ConferenceController@conference');
Route::get('conferences/{name}/formularz','ConferenceController@form');
Route::get('conferences/{name}/formularz-sukces','ConferenceController@formSuccess');
Route::get('conferences/{name}/rejestracja','ConferenceController@conferenceRegister')->before('regular_user');
Route::post('conferences/{name}/rejestracja','ConferenceController@postConferenceRegister')->before('regular_user');

Route::post('conferences/{name}/formularz','ConferenceController@create');
Route::post('conferences/{name}/login', 'ConferenceController@postLogin');

Route::post('conferences/{name}/rejestracja/potwierdzenie', 'ConferenceController@confirmRegister');
Route::post('conferences/{name}/rejestracja/zlozenie_platnosci', 'ConferenceController@ordered');
Route::post('conferences/{name}', 'PasswordController@reminder');
////
Route::controller('password', 'RemindersController');

Route::group(array('prefix' => 'admin'), function()
{
    Route::group(array('before' => 'notLogged'), function()
    {
            
            Route::get('/login', 'AdminUserController@login');
            Route::post('/login', [
                'before' => 'csrf',
                'uses' => 'AdminUserController@login'
            ]);
            Route::controller('password', 'AdminRemindersController');//nie działa
            Route::post('check-user', 'AdminUserController@postLogin');
            Route::get('register', 'AdminUserController@register');
            Route::post('register-user', 'AdminUserController@postRegister');
    });
    
    Route::group(array('before' => 'auth|admin'), function()
    {
            
            Route::post('zmiana-hasla', [
                'before' => 'csrf',
                'uses' => 'AdminPasswordsController@store'
            ]);
        
            Route::get('csv','ExportCsvController@exportCsv');
            Route::get('logout','AdminUserController@logout');
            Route::any('konferencje', 'AdminConferenceController@conferences');
            Route::get('konferencje/dodaj', 'AdminConferenceController@addConferences');
            Route::post('konferencje/dodaj', 'AdminConferenceController@createConferences');
            Route::get('konferencje/{id}/edytuj', 'AdminConferenceController@editConferences');
            Route::post('konferencje/{id}/edytuj', 'AdminConferenceController@updateConferences');
            Route::post('konferencje/usun', 'AdminConferenceController@deleteConference');
            Route::any('uzytkownicy', 'AdminConferenceController@registeredUsers');
            Route::post('uzytkownicy/faktura', 'AdminConferenceController@faktura');
            Route::any('users', 'AdminConferenceController@users');
            Route::post('users/{id}/{flag?}', 'AdminConferenceController@ajaxUser');
            Route::get('wiadomosci', 'AdminMessagesController@index');
            Route::post('wiadomosci', 'AdminMessagesController@postWiadomosci');
            Route::get('zmiana-hasla','AdminPasswordsController@index');
            Route::post('messages-status/{id}','AdminMessagesController@messageStatus');
            
            
            
            Route::get('superadmin/changeid', 'AdminSuperAdminController@updateId');
            Route::resource('/superadmin', 'AdminSuperAdminController');

            Route::get('blank', function(){return View::make('test.blank');});//
            Route::get('buttons', function(){return View::make('test.buttons');});//
            Route::get('flot', function(){return View::make('test.flot');});//
            Route::get('forms', function(){return View::make('test.forms');});//
            Route::get('grid', function(){return View::make('test.grid');});//
            Route::get('index', function(){return View::make('test.index');});//
            Route::get('morris', function(){return View::make('test.morris');});//
            Route::get('notifications', function(){return View::make('test.notifications');});//
            Route::get('panels-wells', function(){return View::make('test.panels-wells');});//
            Route::get('tables', function(){return View::make('test.tables');});//
            Route::get('typography', function(){return View::make('test.typography');});//
            Route::get('payu', function(){return View::make('test.payu');});//
    });
});


//Do usunięcia##################

            Route::get('blank', function(){return View::make('test.blank');});
            Route::get('buttons', function(){return View::make('test.buttons');});
            Route::get('flot', function(){return View::make('test.flot');});
            Route::get('forms', function(){return View::make('test.forms');});
            Route::get('grid', function(){return View::make('test.grid');});
            Route::get('index', function(){return View::make('test.index');});
            Route::get('morris', function(){return View::make('test.morris');});
            Route::get('notifications', function(){return View::make('test.notifications');});
            Route::get('panels-wells', function(){return View::make('test.panels-wells');});
            Route::get('tables', function(){return View::make('test.tables');});
            Route::get('typography', function(){return View::make('test.typography');});
            Route::get('payu', function(){return View::make('test.payu');});