@extends('admin.layout')

@section('title')
<div class="row">
    <div class="col-md-3">
        {{ $title }} 
    </div>
</div>
@stop

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        {{ Form::open(['url' => 'admin/superadmin/'.$user->id, 'method'=>'put']) }}
                                <div class="row">
                                    <div class="col-md-6">
                                        {{ Form::label('name', 'name') }}<br/>
                                        {{ Form::text('name', $user->name, array('class' => 'form-control')) }}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::label('surname', 'surname') }}<br/>
                                        {{ Form::text('surname', $user->surname, array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{ Form::label('pwz', 'pwz') }}<br/>
                                        {{ Form::text('pwz', $user->pwz, array('class' => 'form-control')) }}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::label('email', 'email') }}<br/>
                                        {{ Form::text('email', $user->email, array('class' => 'form-control')) }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{ Form::label('password', 'Hasło') }}<br/>
                                        {{ Form::text('password', null, array('class' => 'form-control')) }}
                                    </div>
                                </div>
                        
                                <div class='row'>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-sympo"><span class="glyphicon glyphicon-save"></span> Zapisz</button>
                                        <a href="{{url('/admin/superadmin')}}" class="btn btn-default">Anuluj</a>
                                    </div>
                                </div>
                            <br/>

                        {{ Form::close() }}
                        
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
@stop

@section('scripts')
    <script>
        $('[data-target="#deleteModal"]').on("click",function(){
             var conf = $(this).parent().parent().children('td.confName').html();
             var id = $(this).parent().parent().attr('id');
             $('span.delConf').text( 'Usunąć konferencje: ' + conf);
             $('span.delConf').attr('id', id);
        });
       
        $('.deleteConference').on('click',function(){
            var id = $('span.delConf').attr('id');
            $.ajax({
                url: "{{ url('admin/konferencje/usun')}}",
                type: 'POST',
                data: { id:id},
                success: function (response) {
                    if(response =="ok"){
                        $('#'+id).remove();
                        $('#deleteModal').modal('hide');
                    } else {
                        $('span.delConf').text( response);
                    }
                }
            });
        }); 
    </script>
@stop