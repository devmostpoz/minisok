@extends('admin.layout')

@section('title')
<div class="row">
    <div class="col-md-3">
        {{ $title }} 
    </div>
</div>
@stop

@section('content')
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Imię</th>
                                                <th>Nazwisko</th>
                                                <th>PWZ</th>
                                                <th>email</th>
                                                <th style="width: 233px;"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                            <tr>
                                                <td>{{$user->id}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->surname}}</td>
                                                <td>{{$user->pwz}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>
                                                    <a href="{{url('admin/superadmin/'.$user->id.'/edit')}}" class="btn btn-success">Edytuj</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
<!--                                </div>
                            </div>-->
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper --
    >
    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Potwierdzenie usunięcia</h4>
          </div>
          <div class="modal-body">
              <span class='delConf'></span>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
            <button type="button" class="btn btn-danger deleteConference">Usuń</button>
          </div>
        </div>
      </div>
    </div>
@stop

@section('scripts')
    <script>
        $('[data-target="#deleteModal"]').on("click",function(){
             var conf = $(this).parent().parent().children('td.confName').html();
             var id = $(this).parent().parent().attr('id');
             $('span.delConf').text( 'Usunąć konferencje: ' + conf);
             $('span.delConf').attr('id', id);
        });
       
        $('.deleteConference').on('click',function(){
            var id = $('span.delConf').attr('id');
            $.ajax({
                url: "{{ url('admin/konferencje/usun')}}",
                type: 'POST',
                data: { id:id},
                success: function (response) {
                    if(response =="ok"){
                        $('#'+id).remove();
                        $('#deleteModal').modal('hide');
                    } else {
                        $('span.delConf').text( response);
                    }
                }
            });
        }); 
    </script>
@stop