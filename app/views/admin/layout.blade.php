<!DOCTYPE html>
<html lang="en">
    
    @include('admin.include.head')

    <body>

        <div id="wrapper">

            @include('admin.include.navigation')
            
                    <div id="page-wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">@yield('title')</h1>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $error }}
                            </div>
                        @endforeach
                        @if (Session::has('error'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        <!-- /.row -->
                        
            @yield('content')
            
                <!-- jQuery -->
        <script src="{{ url('admin/js/jquery.js') }}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{ url('admin/js/bootstrap.min.js') }}"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="{{ url('admin/js/plugins/metisMenu/metisMenu.min.js') }}"></script>

        <!-- Custom Theme JavaScript -->
        <script src="{{ url('admin/js/sb-admin-2.js') }}"></script>
        <script src="{{ url('admin/js/script.js') }}"></script>
        @yield('scripts')

    </body>
    @yield('afterBody')
</html>