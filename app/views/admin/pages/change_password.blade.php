@extends('admin.layout')

@section('title')
    {{ $title }}
@stop
@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Formularz do zmiany hasła
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                {{ Form::open(['url' => url('admin/zmiana-hasla'), 'role' => 'form']) }}
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            {{ Form::password('old_password',['class' => 'form-control', 'placeholder' => 'Aktualne hasło', 'required'])}}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::password('password',['class' => 'form-control', 'placeholder' => 'Nowe hasło', 'required'])}}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::password('password_confirm',['class' => 'form-control', 'placeholder' => 'Powtórz nowe hasło', 'required'])}}
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-success" value="Zmień hasło">
                                        </div>
                                    </div>
                                {{ Form::close() }}
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
@stop
