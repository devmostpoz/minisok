@extends('admin.layout')

@section('title')
<div class="row">
    <div class="col-md-3">
        {{ $title }} 
    </div>
    {{ Form::open(["url" => "admin/konferencje"])}}
    <div class="col-md-6">
        {{ Form::submit('Szukaj', ["class"=>"btn btn-sympo", 'style'=>'float:right']) }}
        {{ Form::text('search_conference', Session::get('search_conference')? Session::get('search_conference') : '', ["class" => "form-control", "placeholder" => "szukaj konferencji", 'style'=>'float:right; width:75%']) }}
    </div>
    {{ Form::close()}}
    <div class="col-md-3">
        <a href="{{ url('admin/konferencje/dodaj') }}" class="btn btn-primary" style="float: right;">DODAJ</a>
    </div>
</div>
@stop

@section('content')
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
<!--                            <div class="panel-heading">
                                <a href='{{ url('konferencje/dodaj')}}' class='btn btn-sympo'> Dodaj konferencje </a> 
                            </div>-->
                            <!-- /.panel-heading -->
<!--                            <div class="panel-body">
                                <div class="table-responsive">-->
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nazwa</th>
                                                <th>Lista uczestników</th>
                                                <th style="width: 233px;"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['conference'] as $dat)
                                                <tr class="odd gradeX" id='{{$dat->id}}'>
                                                    <td>{{ $id = $dat->id }}</td>
                                                    <td class='confName'>{{ $dat->name }}</td>
                                                    <td class="center">
                                                        {{ Form::open(["url" => "admin/uzytkownicy"])}}
                                                        {{ Form::hidden('sort_konf', $dat->id )}}
                                                        {{ Form::submit($reg["$id"], ["class"=>"btn btn-default"]) }}
                                                        {{ Form::close()}}
                                                    </td>
                                                    <td>
                                                        <a href="{{ url($dat->what.'/'.$dat->link) }}" target="_blank" class="btn btn-sympo">Podgląd</a>
                                                        @if($dat->static!=1)
                                                            <a href="{{ url('admin/konferencje/'.$dat->id.'/edytuj') }}" class="btn btn-success">Edytuj</a>
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Usuń</button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
<!--                                </div>
                            </div>-->
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper --
    >
    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Potwierdzenie usunięcia</h4>
          </div>
          <div class="modal-body">
              <span class='delConf'></span>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
            <button type="button" class="btn btn-danger deleteConference">Usuń</button>
          </div>
        </div>
      </div>
    </div>
@stop

@section('scripts')
    <script>
        $('[data-target="#deleteModal"]').on("click",function(){
             var conf = $(this).parent().parent().children('td.confName').html();
             var id = $(this).parent().parent().attr('id');
             $('span.delConf').text( 'Usunąć konferencje: ' + conf);
             $('span.delConf').attr('id', id);
        });
       
        $('.deleteConference').on('click',function(){
            var id = $('span.delConf').attr('id');
            $.ajax({
                url: "{{ url('admin/konferencje/usun')}}",
                type: 'POST',
                data: { id:id},
                success: function (response) {
                    if(response =="ok"){
                        $('#'+id).remove();
                        $('#deleteModal').modal('hide');
                    } else {
                        $('span.delConf').text( response);
                    }
                }
            });
        }); 
    </script>
@stop