@extends('admin.layout')

@section('title')
    {{ $title }}
@stop

@section('scripts')
    <script>
        $('.moreModal').click(function(){
                $('#moreModal .modal-body').html('<img src="{{ url("conference/img/loader.gif")}}" style="text-align: center;"/>');
                var id = $(this).closest('tr').attr('id');
                $.ajax({
                    url: '{{Request::url()}}/' + id + '/1',
                    type: 'POST',
                    success: function (response) { 
                        console.log(response);
                        $('#moreModal .modal-body').html(response);
                    }
                }); 
           });
    </script>
@stop

@section('content')
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                
                                {{ Form::open() }}
                                <div class="row">   
                                        <div class="form-group input-group col-md-6" style='margin-left: 10px;'>
                                            <input type="text" class="form-control" name="search"placeholder="wpisz imię lub nazwisko szukanej osoby..." value="{{ $search or ''}}">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                </div>
                                {{Form::close()}}
                                
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Imię</th>
                                                <th>Nazwisko</th>
                                                <th>Email</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                                <tr class="odd gradeX" id="{{ $user->id }}">
                                                    <td>{{ $user->id }}</td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->surname }}</td>
                                                    <td>{{ $user->email}}</td>
                                                    <td  style="width: 10px;">
                                                        <button type="button" class="btn btn-sympo moreModal" data-toggle="modal" data-target="#moreModal">
                                                            więcej...
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
                                <!-- Pagination -->
                                <div class="row">
                                    <div style="text-align: center">
                                         {{ $users->links() }}
                                    </div>
                                </div>
                                <!-- /.Pagination -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- Modal -->
    <div class="modal fade" id="moreModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
@stop
