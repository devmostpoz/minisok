@extends('admin.layout')

@section('title')
    {{ $title }}
@stop

@section('content')
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                
                                {{ Form::open(['url' => url('admin/uzytkownicy'), 'role' => 'form']) }}
                                <div class="row">
                                    <div class="form-group col-md-3">
                                            {{ Form::select('sort_konf', $conf['name'], Session::get('sort_konf')? Session::get('sort_konf') : '0', ["class" => "form-control"]) }}
                                    </div>    
                                    <div class="form-group col-md-3">
                                        {{ Form::text('searchUser', Session::get('searchUser')? Session::get('searchUser') : '', ["class" => "form-control", "placeholder" => "imię lub nazwisko..."]) }}
                                    </div>    
                                    <div class="form-group col-md-2">
                                        {{ Form::select('orderBy', array(
                                                        'desc' => 'Malejąco',
                                                        'asc' => 'Rosnąco'
                                                    ), Session::get('orderBy')? Session::get('orderBy') : 'desc', ["class" => "form-control"]) }}
                                    </div>
                                    <div class="form-group col-md-2 fakturaSort">
                                        {{ Form::label('faktura', 'Faktura') }}
                                        {{ Form::checkbox('faktura', '1', Session::get('sort_faktura')? Session::get('sort_faktura') : false) }}
                                    </div> 
                                    <div class="form-group col-md-1" style="white-space: nowrap">
                                        <input type="submit" class="btn btn-sympo" value="sortuj" />                                         @if(Session::get('sort_konf'))
                                            <a href="{{ url('admin/csv') }}" class="btn btn-primary">.CSV</a>
                                        @endif
                                    </div>
                                </div>
                                {{Form::close()}}
                                
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Imię i Nazwisko</th>
                                                <th>Nazwa konferencji</th>
                                                <th></th>
                                                {{--<th>Data rejestracji</th>--}}
                                                <th>Faktura</th>
                                                <th>Uwagi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($regUser as $user)
                                                <tr class="odd gradeX" id='{{ $user->id }}' usr="{{ $user->registered_user_id }}">
                                                    <td>{{ $user->id }}</td>
                                                    <td>
                                                        <a class="moreModal" data-toggle="modal" data-target="#moreModal">
                                                        @if(isset($users[$user->registered_user_id]->name))
                                                        {{ $users[$user->registered_user_id]->name }}@endif @if(isset($users[$user->registered_user_id]->surname)){{ $users[$user->registered_user_id]->surname }}
                                                        @endif
                                                        </a>
                                                    </td>
                                                    <td>@if(isset($conf['name'][$user->conference_id])){{ $conf['name'][$user->conference_id] }}@endif</td>
                                                    <td class='confName'>
                                                        <?php $i=0; ?>
                                                            @foreach(json_decode($user->conference,true) as $confName)
                                                            <strong>{{++$i}}.</strong> {{ $confName }}<hr/>
                                                            @endforeach
                                                    </td>
                                                    {{--<td>--}}
                                                        {{--{{$user->created_at}}--}}
                                                    {{--</td>--}}
                                                    <td class="fakturaAdmin">@if($user->faktura) <i class="fa fa-check-square" data-toggle="modal" data-target="#fakturaModal"></i>@endif</td>
                                                    <td>{{substr($user->comments,0,200)}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- Pagination -->
                                <div class="row">
                                    <div style="text-align: center">
                                         {{ $regUser->links() }}
                                    </div>
                                </div>
                                <!-- /.Pagination -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- Modal -->
    <div class="modal fade" id="fakturaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Dane do faktury</h4>
          </div>
          <div class="modal-body">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="moreModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
@stop

@section('scripts')
    <script>
        $('.fa-check-square').click(function(){
                var id = $(this).parent().parent().attr('id');
                $('#fakturaModal .modal-body').html('<img src="{{ url("conference/img/loader.gif")}}" style="text-align: center;"/>');
                
                $.ajax({
                    url: '{{url("admin/uzytkownicy/faktura")}}',
                    type: 'POST',
                    data: { id: id},
                    success: function (response) { 
                        $('#fakturaModal .modal-body').html(response);
                    }
                }); 
           });
           
        $('.moreModal').click(function(){
                $('#moreModal .modal-body').html('<img src="{{ url("conference/img/loader.gif")}}" style="text-align: center;"/>');
                var id = $(this).closest('tr').attr('usr');
                $.ajax({
                    url: '{{url("admin/users")}}/' + id,
                    type: 'POST',
                    success: function (response) { 
                        $('#moreModal .modal-body').html(response);
                    }
                }); 
           });
    </script>
@stop