@extends('admin.layout')

@section('title')
    {{ $title }}
@stop

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ url('admin/js/datetimepicker/jquery.datetimepicker.css') }}"/>
    <style>
        .none{ display: none!important;}
    </style>
@stop
@section('content')
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        {{ Form::open(['url' => 'admin/konferencje/dodaj', 'files' => true]) }}
                                <div class="row">
                                    <div class="form-group col-md-2">
                                            {{ Form::label('what', 'Konferencje/szkoły?') }}
                                            {{ Form::select('what', ["szkoly" => "szkoły", "konferencje" => "konferencje", ], Input::old('what'), ["class" => "form-control"]) }}
                                    </div>
<!--                                    <div class="col-md-4">
                                            {{ Form::label('date', 'Data rozpoczęcia konferencji: ') }}
                                            <div class="form-group  input-group">
                                                {{ Form::text('date', '', array('class' => 'form-control','id' => 'date',  'maxlength' =>'10')) }}
                                                <span class="input-group-addon"><i class="fa fa-calendar-o fa-fw"></i></span>
                                            </div>			      
                                    </div>-->
                                    <div class="col-md-3">
                                        {{ Form::label('link', 'link *') }}<br/>
                                        {{ Form::text('link', Input::old('link'), array('class' => 'form-control','placeholder'=>'link', 'required')) }}

                                    </div>
                                    <div class="col-md-4">
                                            {{ Form::label('name', 'Nazwa  *') }}<br/>
                                            {{ Form::text('name', Input::old('name'), array('class' => 'form-control','placeholder'=>'nazwa', 'required')) }}
                                                			      
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                            {{ Form::label('image', 'Obrazek (1920px  X 680px) *') }}                
                                            {{ Form::file('image',array('required'))}}
                                         </div>     
                                    </div>
                                </div>
                                
                                <div class='row school'>
                                        <div class="col-md-5">
                                            {{ Form::label('place_school', 'miejsce *') }}<br/>
                                            {{ Form::text('place_school[]', null, array('class' => 'form-control','placeholder'=>'Miejsce')) }}		      
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::label('date_shool', 'data *') }}<br/>
                                            <div class="form-group  input-group">

                                                {{ Form::text('date_school[]', false, array('class' => 'form-control datetimepicker', 'placeholder'=>'Data', 'maxlength' =>'10')) }}
                                                <span class="input-group-addon"><i class="fa fa-calendar-o fa-fw"></i></span>
                                            </div>			      
                                        </div>
                                        <div class='col-md-2'>
                                            {{ Form::label('', '') }}<br/>
                                            <input class='btn btn-danger' type='button' value='Usuń miejsce'  onclick="deleteSchool($(this));">
                                        </div>
                                    <div class='col-md-12'>
                                        <button class='btn btn-primary addSchool' type='button'> Dodaj kolejne miejsce</button>
                                    </div>
                                </div>
                                
                                <div class='row conference'>
                                    <table class='table conferenceTable'>
                                        <tr>
                                            <th class='col-md-3'>
                                                Opcje *
                                            </th>
                                            <th>
                                                    {{ Form::text('date_conf[]', false, array('class' => 'form-control datetimepicker', "style" => "max-width:175px", 'placeholder'=>'Data', 'maxlength' =>'10')) }}
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                {{ Form::text('place_conf[]', false, array('class' => 'form-control','placeholder'=>'Miejsce')) }}
                                            </td>
                                            <td>
                                                {{ Form::number('price_conf[]', false, array('class' => 'form-control','placeholder'=>'cena', "style" => "max-width:175px")) }}
                                            </td>
                                        </tr>
                                    </table>
                                    <div class='col-md-12'>
                                        <button class='btn btn-primary addConference' type='button'> Dodaj kolejną opcje</button>
                                        <button class='btn btn-primary addConferenceDate' type='button'> Dodaj Datę</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        {{ Form::label('description', 'Opis *') }}
                                        {{ Form::textarea('description', Input::old('description'), ['class' => 'description',  'required']) }}
                                    </div>
                                </div>
                        
                                <div class='row'>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-sympo"><span class="glyphicon glyphicon-save"></span> Zapisz</button>
                                        <a href="{{url('/admin/konferencje')}}" class="btn btn-default">Anuluj</a>
                                    </div>
                                </div>
                            <br/>

                        {{ Form::close() }}
                        
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
</div></div>
                <!-- /.row -->
@stop
        
@section('scripts')
    <script src="{{ url('admin/ckeditor/ckeditor.js') }}"></script>
    <script>
       
        
        
         var roxyFileman = "{{url('/admin/ckeditor/fileman/index.html')}}";

        $(function(){

           CKEDITOR.replace( 'description',{
               filebrowserBrowseUrl:roxyFileman,

                                        filebrowserImageBrowseUrl:roxyFileman+'?type=image&SESSION_PATH_KEY=localhost',

//                                        filebrowserUploadUrl:roxyFileman

        });

        });

 
        function addDatePicker(){
            var date_picker = "<script>$('.datetimepicker').datetimepicker({" +
                                            "lang:'pl'," +
                                            "timepicker:false," +
                                            "format:'d-m-Y'," +
                                            "formatDate:'Y-m-d'" +
                                    "});<\/script>";
             $('body').after(date_picker);               
        }
        
        
        if($('[name=what]').val()=="szkoly"){
                $('.school').show();
                $('.conference').hide();
        } else {
                $('.school').hide();
                $('.conference').show();
        }
        
        $('[name=what]').change(function(){
            if($('[name=what]').val()=="szkoly"){
                $('.school').show();
                $('.conference').hide();
            } else {
                $('.school').hide();
                $('.conference').show();
            }
        });
        
        $('.addConference').click(function(){
            var counter = 0;
            $('.conferenceTable tr:last td').each(function(){
                    counter = counter + 1;
            });
            var td = '<td><input class="form-control" placeholder="Miejsce" name="place_conf[]" type="text" value=""></td>'
            for(i=1; i<counter; i++){
                    td = td +'<td><input class="form-control" placeholder="cena" style="max-width:175px" name="price_conf[]" type="number" value=""></td>';
            }
            $('.conferenceTable tr:last').after('<tr>' + td + '</tr>');
            
        });
        
        $('.addConferenceDate').click(function(){
                var th = '<input class="form-control datetimepicker" placeholder="Data" maxlength="10" name="date_conf[]" type="text" value="" style="max-width:175px" >';
            
                var td = '<input class="form-control" placeholder="cena" style="max-width:175px" name="price_conf[]" type="number" value="">';
            
                $('.conferenceTable').find('tr').each(function(){
                    $(this).find('th:last-child').after('<th>' + th + '</th>');
                    $(this).find('td:last-child').after('<td>' + td+ '</td>');
                });
                
                addDatePicker();
        });
        
        $('.addSchool').click(function(){
           
        
            var text = '<div class="col-md-5">' +
                            '<input class="form-control" placeholder="Miejsce" name="place_school[]" type="text">' +
                        '</div>' +
                        '<div class="col-md-3">' +
                            '<div class="form-group  input-group">' +
                                '<input class="form-control datetimepicker"  placeholder="Data" maxlength="10" name="date_school[]" type="text">' +
                                '<span class="input-group-addon"><i class="fa fa-calendar-o fa-fw"></i></span>' +
                            '</div>' +			      
                        '</div>' +
                        '<div class="col-md-2">' +
                            '<input class="btn btn-danger" type="button" value="Usuń miejsce" onclick="deleteSchool($(this));">' +
                        '</div>';
                 $('.addSchool').parent().before(text);
                 addDatePicker();
        });
        
        function deleteSchool(e){
            e.parent().prev().prev().remove();
            e.parent().prev().remove();
            e.parent().remove();
        
        }
        
        
        
    </script>
@stop
@section('afterBody')
    <script src="{{ url('admin/js/datetimepicker/jquery.datetimepicker.js') }}"></script>
    <script>
    $('.datetimepicker').datetimepicker({
            lang:'pl',
            timepicker:false,
            format:'d-m-Y',
            formatDate:'Y-m-d'
    });
    </script>
@stop