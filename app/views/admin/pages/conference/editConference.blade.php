@extends('admin.layout')

@section('title')
    {{ $title }}
@stop

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ url('admin/js/datetimepicker/jquery.datetimepicker.css') }}"/>
    <style>
        .xdsoft_prev{ display: none;}
    </style>
@stop
@section('content')
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        {{ Form::open(['files' => true]) }}
                                <div class="row">
                                        <img src="{{ url('images/conferences/'.$conf->image)}}" style="width: 100%;"/>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-2">
                                            {{ Form::hidden('what',$conf->what )}}
                                            {{ Form::label('what2', 'Konferencje/szkoły?') }}
                                            {{ Form::select('what2', ["szkoly" => "szkoły", "konferencje" => "konferencje", ], $conf->what, ["class" => "form-control", "disabled"]) }}
                                    </div>
                                    <div class="col-md-3">
                                        {{ Form::label('link1', 'link *') }}<br/>
                                        {{ Form::text('link1', $conf->link, array('class' => 'form-control','placeholder'=>'link', 'required', 'disabled')) }}

                                    </div>
                                    <div class="col-md-4">
                                            {{ Form::label('name', 'Nazwa  *') }}<br/>
                                            {{ Form::text('name', $conf->name, array('class' => 'form-control','placeholder'=>'nazwa', 'required')) }}
                                                			      
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                            {{ Form::label('image', 'Obrazek (1920px  X 680px) *') }}                
                                            {{ Form::file('image')}}
                                         </div>     
                                    </div>
                                </div>
                                <div class='row school'>
                                @if($conf->what=='szkoly')
                                        @foreach($data as $key=>$val)
                                            <div class="col-md-5">
                                                @if($i==0){{ Form::label('place_school', 'miejsce *') }}<br/> @endif
                                                {{ Form::text('place_school[]', $val, array('class' => 'form-control','placeholder'=>'Miejsce', "disabled" => "disabled")) }}		      
                                            </div>
                                            <div class="col-md-3">
                                                @if($i==0){{ Form::label('date_shool', 'data *') }}<br/>@endif
                                                <div class="form-group  input-group">

                                                    {{ Form::text('date_school[]', $key, array('class' => 'form-control datetimepicker', 'placeholder'=>'Data', 'maxlength' =>'10')) }}
                                                    {{ Form::hidden('date_school_old[]', $key) }}
                                                    <span class="input-group-addon"><i class="fa fa-calendar-o fa-fw"></i></span>
                                                </div>			      
                                            </div>
                                            <div class='col-md-2'>
                                                {{ Form::label('', '') }}@if($i==0)<br/><?php $i++;?>@endif
                                                <!--<input class='btn btn-danger' type='button' value='Usuń miejsce'  onclick="deleteSchool($(this));">-->
                                            </div>
                                        @endforeach
                                @else
                                            <div class="col-md-5">
                                                {{ Form::label('place_school', 'miejsce *') }}<br/>
                                                {{ Form::text('place_school[]', null, array('class' => 'form-control','placeholder'=>'Miejsce')) }}		      
                                            </div>
                                            <div class="col-md-3">
                                                {{ Form::label('date_shool', 'data *') }}<br/>
                                                <div class="form-group  input-group">

                                                    {{ Form::text('date_school[]', false, array('class' => 'form-control datetimepicker', 'placeholder'=>'Data', 'maxlength' =>'10')) }}
                                                     {{ Form::hidden('date_school_old[]', false) }}
                                                    <span class="input-group-addon"><i class="fa fa-calendar-o fa-fw"></i></span>
                                                </div>			      
                                            </div>
                                            <div class='col-md-2'>
                                                {{ Form::label('', '') }}<br/>
                                                <input class='btn btn-danger' type='button' value='Usuń miejsce'  onclick="deleteSchool($(this));">
                                            </div>
                                @endif
                                    <div class='col-md-12'>
                                        <!--<button class='btn btn-primary addSchool' type='button'> Dodaj kolejne miejsce</button>-->
                                    </div>
                                </div>
                        
                                <div class='row conference'>
                                @if($conf->what=='konferencje')
                                    <table class='table conferenceTable'>
                                        @foreach($data as $k=>$dat)
                                                   @if($i==0)
                                                       <tr>
                                                           <th class='col-md-3'>
                                                               Opcje *
                                                           </th>
                                                           @foreach($dat as $key=>$val)
                                                                <th>
                                                                        {{ Form::text('date_conf[]', $key, array('class' => 'form-control datetimepicker', "style" => "max-width:175px", 'placeholder'=>'Data', 'maxlength' =>'10')) }}
                                                                        {{ Form::hidden('date_conf_old[]', $key) }}
                                                                </th>
                                                           @endforeach
                                                       </tr>
                                                       <?php $i++;?>
                                                   @endif
                                                   <tr>
                                                       <td>
                                                           {{ Form::text('place_conf1[]', $k, array('class' => 'form-control','placeholder'=>'Miejsce', "disabled" => "disabled")) }}
                                                       </td>
                                                       @foreach($dat as $key=>$val)
                                                            <td>
                                                                {{ Form::number('price_conf[]', $val, array('class' => 'form-control','placeholder'=>'cena', "style" => "max-width:175px", "disabled" => "disabled")) }}
                                                            </td>
                                                       @endforeach
                                                   </tr>
                                        @endforeach
                                    </table>
                                @else
                                        <table class='table conferenceTable'>
                                            <tr>
                                                <th class='col-md-3'>
                                                    Opcje *
                                                </th>
                                                <th>
                                                        {{ Form::text('date_conf[]', false, array('class' => 'form-control datetimepicker', "style" => "max-width:175px", 'placeholder'=>'Data', 'maxlength' =>'10')) }}
                                                        {{ Form::hidden('date_conf_old[]', false) }}
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {{ Form::text('place_conf1[]', false, array('class' => 'form-control','placeholder'=>'Miejsce', "disabled" => "disabled")) }}
                                                </td>
                                                <td>
                                                    {{ Form::number('price_conf[]', false, array('class' => 'form-control','placeholder'=>'cena', "style" => "max-width:175px", "disabled" => "disabled")) }}
                                                </td>
                                            </tr>
                                        </table>
                                @endif
                                    <div class='col-md-12'>
                                        <!--<button class='btn btn-primary addConference' type='button'> Dodaj kolejną opcje</button>-->
                                        <!--<button class='btn btn-primary addConferenceDate' type='button'> Dodaj Datę</button>-->
                                    </div>
                                </div>
                                            
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        {{ Form::label('description', 'Opis *') }}
                                        {{ Form::textarea('description', $conf->description, ['class' => 'description',  'required']) }}
                                    </div>
                                </div>
                        
                                <div class='row'>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-sympo"><span class="glyphicon glyphicon-save"></span> Zapisz</button>
                                        <a href="{{url('/admin/konferencje')}}" class="btn btn-default">Anuluj</a>
                                    </div>
                                </div>
                            <br/>

                        {{ Form::close() }}
                        
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
</div></div>
                <!-- /.row -->
@stop
        
@section('scripts')
    <script src="{{ url('admin/ckeditor/ckeditor.js') }}"></script>
    <script>
         var roxyFileman = "{{url('/admin/ckeditor/fileman/index.html')}}";

        $(function(){

           CKEDITOR.replace( 'description',{filebrowserBrowseUrl:roxyFileman,

                                        filebrowserImageBrowseUrl:roxyFileman+'?type=image&SESSION_PATH_KEY=localhost',

//                                        filebrowserUploadUrl:roxyFileman+'?type=image&SESSION_PATH_KEY=localhost',

        });

        });

 
        
        
        
        if($('[name=what]').val()=="szkoly"){
                $('.school').show();
                $('.conference').hide();
        } else {
                $('.school').hide();
                $('.conference').show();
        }
        
        $('[name=what]').change(function(){
            if($('[name=what]').val()=="szkoly"){
                $('.school').show();
                $('.conference').hide();
            } else {
                $('.school').hide();
                $('.conference').show();
            }
        });
        
        
        
        
        
    </script>
@stop
@section('afterBody')
    <script src="{{ url('admin/js/datetimepicker/jquery.datetimepicker.js') }}"></script>
    <script>
    $('.datetimepicker').datetimepicker({
            lang:'pl',
            timepicker:false,
            format:'d-m-Y',
            formatDate:'Y-m-d'
    });
    </script>
@stop