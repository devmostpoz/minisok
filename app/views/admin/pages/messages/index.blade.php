@extends('admin.layout')

@section('title')
    {{ $title }}
@stop

@section('scripts')
    <script>
        
        $('body').keyup(function(e){
            console.log(e.keyCode);
        });
        
        $(".show").on('click', function(){
            if ( $(this).parent().parent().next().is( ".hide" ) ) {
                $('.success').addClass('hide');
                $(this).parent().parent().next().slideDown().removeClass( "hide" );
                
                var url = $('#url').val();
                var id = $(this).closest('tr').attr('id');
                $.ajax({
                        url: url+ '/admin/messages-status/' + id,
                        type: 'POST',
                        data:{id:id},
                        success: function (response) { 
                            $('tr#'+id).removeClass('bold');
                        }
                    }); 
            } else {
                $(this).parent().parent().next().addClass( "hide" );
            }
        });
    </script>
@stop

@section('content')
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{ Form::open(['url' => url('admin/wiadomosci'), 'role' => 'form']) }}
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        {{ Form::select('m_sort_konf', $conf, Session::get('m_sort_konf')? Session::get('m_sort_konf') : '0', ["class" => "form-control"]) }}
                                    </div>    
                                    <div class="form-group col-md-2">
                                        {{ Form::select('m_sort', Messages::$sort, Session::get('m_sort')? Session::get('m_sort') : 'id', ["class" => "form-control"]) }}
                                   </div>    
                                    <div class="form-group col-md-2">
                                            {{ Form::select('m_orderBy', array(
                                                        'desc' => 'Malejąco',
                                                        'asc' => 'Rosnąco'
                                                    ), Session::get('m_orderBy')? Session::get('m_orderBy') : 'desc', ["class" => "form-control"]) }}
                                    </div>
                                        <div class="form-group col-md-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="m_read" @if(Session::get('m_read')=='1') checked @endif>nieprzeczytane
                                                </label>
                                            </div>
                                        
                                    </div>
                                    <div class="form-group col-md-1">
                                        <input type="submit" class="btn btn-sympo" value="sortuj" />
                                    </div>
                                </div>
                                {{Form::close()}}
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Imię Nazwisko</th>
                                                <th>Konferencja</th>
                                                <th>E-mail</th>
                                                <th>Data</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($messages as $message)
                                                <tr class="odd gradeX @if(!$message->status) bold @endif" id="{{ $message->id }}">
                                                    <td>{{ $message->id }}</td>
                                                    <td>{{ $message->name }}</td>
                                                    <td>{{ $confName[$message->conference] }}</td>
                                                    <td><a href="mailto:{{ $message->email }}?body={{ nl2br($message->text) }}">{{ $message->email }}</a></td>
                                                    <td>{{ $message->created_at }}</td>
                                                    <td><a href="#" class="btn btn-sympo show">Pokaż</a></td>
                                                </tr>
                                                <tr class="success hide">
                                                    <td colspan="6">
                                                        Treść wiadmości:<br>
                                                        {{ nl2br($message->text) }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            

                                        </tbody>
                                    </table>
                                    
                                    <div class="row">
                                        <div style="text-align: center">
                                             {{ $messages->links() }}
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
@stop
