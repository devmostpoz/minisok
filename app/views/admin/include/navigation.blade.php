<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand"><img src="{{ url('admin/img/sym_logo_cms.png') }}" alt="admin" style="height: 34px; margin-top: -10px;"></a>
            </div>
            <!-- /.navbar-header -->
            <input type="hidden" value="{{ url()}}" id="url" />
            <ul class="nav navbar-top-links navbar-right">
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="{{ url('admin/zmiana-hasla') }}"><i class="fa fa-lock fa-fw"></i> Zmień hasło</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout"><i class="fa fa-sign-out fa-fw"></i> Wyloguj</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <li class="navInfo">
                    {{ Auth::user()->email}}
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
<!--                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Szukaj konferencji...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                             /input-group 
                        </li>-->
                        <li>
                            <a href="{{ url('admin/konferencje') }}"><i class="fa fa-users fa-fw"></i> Konferencje</a>
                        </li>
                        <li>
                            <a href="{{ url('admin/uzytkownicy') }}"><i class="fa fa-user fa-fw"></i> Zarejestrowani uczestnicy</a>
                        </li>
                        <li>
                            <a href="{{ url('admin/users') }}"><i class="fa fa-users fa-fw"></i> Wszyscy uczestnicy</a>
                        </li>
                        <li>
                            <a href="{{ url('admin/wiadomosci') }}"><i class="fa fa-envelope fa-fw"></i> Wiadomości</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>