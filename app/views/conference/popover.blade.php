<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<!--        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>-->
      <div class="modal-body">
        <!-- content -->
        <div id="form-olvidado">
            <div clas="row">
                <div class="col-md-12 center">
                    <p class="error"> @if (Session::has('error')){{ Session::get('error') }} @endif</p>
                    <p>Jeżeli posiadasz konto w Symposion, zaloguj się</p>
                </div>
            </div>
            {{ Form::open(["url"=> Request::url().'/login']) }}
                <div class="row">
                    <div class="form-group col-lg-6">
                        <label for="login">login (numer PWZ)</label>
                        <input type="text" id="login" class="form-control" name="login">
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="password">hasło</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </div>
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-md-12 center">
                        <input type="submit" value="zaloguj" class="btn btn-success" />
                    </div>
                    <p style="  text-align: center; font-weight: 500;">
                        Jeżeli macie Państwo jakiekolwiek pytania dotyczące rejestracji proszę o kontakt:<br>
Aleksandra Patora tel: 502-286-148 lub Michał Springer 505-114-984.
                    </p>
                </div>
            {{ Form::close() }}
<!--            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12 center padding">
                        <a href="#" id="olvidado">nie pamiętam hasła</a>
                    </div>
                </div>
            </div>-->
            <div class="row footerModal">
                <div class="col-md-12 center">
                    <div class="row">
                        <div class="col-lg-6">
                            <span style="color:#197B3C; font-weight: bold;">Jeżeli nie logowaliście się Państwo do systemu w 2015 roku - prosimy o nową rejestrację w systemie SYMPOSION.</span>
                        </div>
                        <div class="col-lg-6">
                            <a href="{{ url(Request::url().'/formularz')}}" class="btn btn-success"> zarejestruj się w symposion</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none;" id="form-olvidado">
            <div clas="row">
                <div class="col-md-12 center">
                    <p>Aby zresetować hasło wpisz swój login (numer PWZ)</p>
                </div>
            </div>
            <form action="{{ action('RemindersController@postRemind') }}" method="POST">
                <div class="row">
                    <div class="form-group col-lg-offset-3 col-lg-6 center">
                        <input type="text" id="login" class="form-control" id="login" placeholder="login" name="login">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 center">
                        <input type="submit" value="Zresetuj hasło" class="btn btn-success" />
                    </div>
                </div>
            </form>   
            <div class="row">
                    <div class="col-md-12">    
                        <div class="col-md-12 center padding">
                                    <a href="#" id="acceso">wróć do logowania</a>
                        </div>
                    </div>
            </div>
            <div class="row footerModal">
                <div class="col-md-12 center">
                    <div class="row">
                        <div class="col-lg-6">
                            <span style="color:#197B3C; font-weight: bold;">Jeżeli nie logowaliście się Państwo do systemu w 2015 roku - prosimy o nową rejestrację w systemie SYMPOSION.</span>
                        </div>
                        <div class="col-lg-6">
                            <a href="{{ url(Request::url().'/formularz')}}" class="btn btn-success"> zarejestruj się w symposion</a>
                        </div>
                    </div>
                     
                </div>
            </div>
            
        </div>    
        <!-- ./content -->
      </div>
    </div>
  </div>
</div>
    <script>
        $(document).ready(function() {
            $('#olvidado').click(function(e) {
              e.preventDefault();
              $('div#form-olvidado').toggle('500');
            });
            $('#acceso').click(function(e) {
              e.preventDefault();
              $('div#form-olvidado').toggle('500');
            });
            
            @if (Session::has('error'))
                $('#loginModal').modal('show');
                $('#loginModal p.error').show().delay(2000).fadeOut(1000);
            @endif
            
          });
    </script>