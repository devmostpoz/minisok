
<!DOCTYPE html>
<!-- Change to your language -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title }}</title>

    <!-- Core Bootstrap CSS -->
    <link href="{{ url('conference/css/style.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    
</head>

<body>
    <div class="register-form  container  relative">
        <div class="wrapper  form-container">
            @if($errors->all())
                @foreach(array_unique($errors->all()) as $error)
                <p style="color:red">{{$error}}</p>
                @endforeach
            @endif
        {{ Form::open() }}
           <div class="form-section">
                <h2>Dane osobowe</h2>
                <div class="container_100">
                    <div class="input-container">
                        {{ Form::radio('sex','Pani', Input::old('sex'),  ["required"]) }}
                        <label class="big">Pani</label>
                        {{ Form::radio('sex','Pan', Input::old('sex'),  ["required"]) }}
                        <label class="big">Pan</label>
                    </div><!--/.input-container-->
                    <div class="input-container">
                        {{ Form::label('science_degree', 'tytuł naukowy', ["class" => "small"]) }}
                        {{ Form::select('science_degree', $data['science_degree'], Input::old('science_degree'), ["class" => "input-style  input-medium"]) }}
                    </div><!--/.input-container-->
                    <div class="input-container">
                        {{ Form::label('name', 'imię', ["class" => "small"])}}
                        {{ Form::text('name', Input::old('name'), ["class" => "input-style  input-medium", "required"]) }}
                    </div><!--/.input-container-->
                    <div class="input-container">
                        {{ Form::label('name', 'nazwisko', ["class" => "small"])}}
                        {{ Form::text('surname', Input::old('surname'), ["class" => "input-style  input-medium", "required"]) }}
                    </div><!--/.input-container-->
                </div><!--/.container_100-->
                <div class="container_100">
                    <div class="input-container">
                        {{ Form::label('who', 'Jestem:', ["class" => "small"]) }}
                        {{ Form::select('who', $data['who'], Input::old('who'), ["class" => "input-style  input-medium", "id"=>"who"]) }}
                    </div>
                    <div class="input-container  left-5" id="pwz">
                        {{ Form::label('pwz', 'numer PWZ', ["class" => "small"])}}
                        {{ Form::number('pwz[0]', Input::old('pwz.0'), ["class" => "input-mini first pwz", "maxlength" => "1", "required", "min" => "0", "max" => "9"]) }}
                        @for($i=1; $i<7; $i++)
                            {{ Form::number('pwz['.$i.']', Input::old("pwz.".$i), ["class" => "input-mini pwz", "maxlength" => "1", "required", "min" => "0", "max" => "9"]) }}
                        @endfor
                        {{ Form::number('pwz[7]', Input::old('pwz.7'), ["class" => "input-mini last pwz", "maxlength" => "1", "required", "min" => "0", "max" => "9"]) }}
                    </div><!--/.input-container-->
                     <div class="input-container  left-5">
                        {{ Form::label('pesel', 'numer PESEL', ["class" => "small"])}}
                        {{ Form::number('pesel[0]', Input::old('pesel[0]'), ["class" => "input-mini first", "maxlength" => "1", "required", "min" => "0", "max" => "9"]) }}
                        @for($i=1; $i<10; $i++)
                            {{ Form::number('pesel['.$i.']', Input::old('pesel.'.$i), ["class" => "input-mini", "maxlength" => "1", "required", "min" => "0", "max" => "9"]) }}
                        @endfor
                        {{ Form::number('pesel[10]', Input::old('pesel[10]'), ["class" => "input-mini last", "maxlength" => "1", "required", "min" => "0", "max" => "9"]) }}
                    </div><!--/.input-container-->
                </div><!--/.container_100-->
                <div class="container_100">
                    <div class="input-container">
                        {{ Form::label('specialization', 'I specjalizacja', ["class" => "small"]) }}
                        {{ Form::select('specialization[0]', $data['specialization'], Input::old('specialization[0]'), ["class" => "input-style  input-medium", "required"]) }}
                    </div><!--/.input-container-->
                    <div class="input-container">
                        {{ Form::label('specialization', 'II specjalizacja', ["class" => "small"]) }}
                        {{ Form::select('specialization[1]', $data['specialization'], Input::old('specialization[1]'), ["class" => "input-style  input-medium"]) }}
                    </div><!--/.input-container-->
                    <div class="input-container spec3 hide">
                        {{ Form::label('specialization', 'III specjalizacja', ["class" => "small"]) }}
                        {{ Form::select('specialization[2]', $data['specialization'], Input::old('specialization[2]'), ["class" => "input-style  input-medium"]) }}
                    </div><!--/.input-container-->
                    <div class="input-container spec4 hide">
                        {{ Form::label('specialization', 'IV specjalizacja', ["class" => "small"]) }}
                        {{ Form::select('specialization[3]', $data['specialization'], Input::old('specialization[3]'), ["class" => "input-style  input-medium"]) }}
                    </div><!--/.input-container-->
                    <div class="input-containe spec5 hide">
                        {{ Form::label('specialization', 'V specjalizacja', ["class" => "small"]) }}
                        {{ Form::select('specialization[4]', $data['specialization'], Input::old('specialization[4]'), ["class" => "input-style  input-medium"]) }}
                    </div><!--/.input-container-->
                    <button class="btn addSpecialization">dodaj kolejną specjalizację</button>
                </div><!--/.container_100-->
           </div><!--/.form-section-->
           
           
           @for($i=0; $i<5; $i++)
            @if($i>0)
                {{ $data['required']=''}}
            @endif
                <div class="form-section address{{$i}} @if($i>0) hide @endif">
                     <h2>Adres korespondencyjny @if($i>0) #{{$i+1}} @endif</h2>
                     <div class="container_100">
                         <div class="input-container">
                             {{ Form::radio("adress[$i]","ulica", Input::old("adress.$i",  [$data['required']])) }}
                             <label class="big">ulica</label>
                              {{ Form::radio("adress[$i]","osiedle", Input::old("adress.$i"),  [$data['required']]) }}
                             <label class="big">osiedle</label>
                              {{ Form::radio("adress[$i]","aleja", Input::old("adress.$i"),  [$data['required']]) }}
                             <label class="big">aleja</label>
                              {{ Form::radio("adress[$i]","plac", Input::old("adress.$i"),  [$data['required']]) }}
                             <label class="big">plac</label>
                         </div><!--/.input-container-->
                         <div class="input-container">
                             {{ Form::label("street_name", "nazwa", ["class" => "small"])}}
                             {{ Form::text("street_name[$i]", Input::old("street_name.$i"), ["class" => "input-style  input-medium",  $data['required']]) }}
                         </div><!--/.input-container-->
                         <div class="input-container">
                             {{ Form::label("house_number", "nr domu", ["class" => "small"])}}
                             {{ Form::text("house_number[$i]", Input::old("house_number.$i"), ["class" => "input-style  input-small",  $data['required']]) }}
                         </div><!--/.input-container-->
                         <div class="input-container">
                             {{ Form::label("flat_number", "nr mieszkania", ["class" => "small"])}}
                             {{ Form::text("flat_number[$i]", Input::old("flat number.$i"), ["class" => "input-style  input-small"]) }}
                         </div><!--/.input-container-->
                     </div><!--/.container_100-->
                     <div class="container_100">
                         <div class="input-container">
                             {{ Form::label("postal_code", "kod pocztowy", ["class" => "small"])}}
                             {{ Form::number("postal_code[$i][0]", Input::old("postal_code.$i.[0]"), ["class" => "input-smaller input-style", "maxlength" => "1",  $data['required']]) }}
                             {{ Form::number("postal_code[$i][1]", Input::old("postal_code.$i.[1]"), ["class" => "input-smaller input-style", "maxlength" => "1",  $data['required']]) }}
                             -
                             {{ Form::number("postal_code[$i][2]", Input::old("postal_code.$i.[2]"), ["class" => "input-smaller input-style", "maxlength" => "1",  $data['required']]) }}
                             {{ Form::number("postal_code[$i][3]", Input::old("postal_code.$i.[3]"), ["class" => "input-smaller input-style", "maxlength" => "1",  $data['required']]) }}
                             {{ Form::number("postal_code[$i][4]", Input::old("postal_code.$i.[4]"), ["class" => "input-smaller input-style", "maxlength" => "1",  $data['required']]) }}
                         </div><!--/.input-container-->
                         <div class="input-container">
                             {{ Form::label("city", "miejscowość", ["class" => "small"])}}
                             {{ Form::text("city[$i]", Input::old("city[$i]"), ["class" => "input-style  input-medium",  $data['required']]) }}
                         </div><!--/.input-container-->
                         <div class="input-container">
                             {{ Form::label("post_office", "poczta", ["class" => "small"])}}
                             {{ Form::text("post_office[$i]", Input::old("post_office[$i]"), ["class" => "input-style  input-medium",  $data['required']]) }}
                         </div><!--/.input-container-->
                     </div><!--/.container_100-->
                     <div class="container_100">
                         <div class="input-container">
                             {{ Form::label("province", "województwo", ["class" => "small"]) }}
                             {{ Form::select("province[$i]", $data["province"], Input::old("province[$i]"), ["class" => "input-style  input-medium",  $data['required']]) }}
                         </div><!--/.input-container-->
                         <div class="input-container">
                             {{ Form::label("country", "kraj", ["class" => "small"]) }}
                             {{ Form::select("country[$i]", $data["country"], Input::old("country[$i]")? Input::old("country") : "Polska", ["class" => "input-style  input-medium",  $data['required']]) }}
                         </div><!--/.input-container-->
                         <button class="btn addAddress">dodaj kolejny adres</button>
                         <button class="btn removeAddress btn-danger @if($i=='0') hide @endif">Usuń adres</button>
                     </div><!--/.container_100-->
                </div>
           @endfor
           
           <div class="form-section">
                <div class="container_100">
                    <div class="input-container">
                        {{ Form::label('email', 'adres e-mail', ["class" => "small"])}}
                        {{ Form::email('email', Input::old('email'), ["class" => "input-style  input-medium", "required"]) }}
                    </div><!--/.input-container-->
                    <div class="input-container tel0">
                        {{ Form::label('telephone', 'numer telefonu', ["class" => "small"])}}
                        {{ Form::text('telephone[0]', Input::old('telephone[0]'), ["class" => "input-style  input-medium", "required", "pattern" => ".{3,}", "title"=> "telefon musi mieć co najmniej 9 znaków."]) }}
                    </div><!--/.input-container-->
                    <div class="input-container hide tel1">
                        {{ Form::label('telephone', 'numer telefonu #2', ["class" => "small"])}}
                        {{ Form::text('telephone[1]', Input::old('telephone[1]'), ["class" => "input-style  input-medium"]) }}
                    </div><!--/.input-container-->
                    <div class="input-container hide tel2">
                        {{ Form::label('telephone', 'numer telefonu #3', ["class" => "small"])}}
                        {{ Form::text('telephone[2]', Input::old('telephone[2]'), ["class" => "input-style  input-medium"]) }}
                    </div><!--/.input-container-->
                    <div class="input-container hide tel3">
                        {{ Form::label('telephone', 'numer telefonu #4', ["class" => "small"])}}
                        {{ Form::text('telephone[3]', Input::old('telephone[3]'), ["class" => "input-style  input-medium"]) }}
                    </div><!--/.input-container-->
                    <div class="input-container hide tel4">
                        {{ Form::label('telephone', 'numer telefonu #5', ["class" => "small"])}}
                        {{ Form::text('telephone[4]', Input::old('telephone[4]'), ["class" => "input-style  input-medium"]) }}
                    </div><!--/.input-container-->
                    <button class="btn addTelephone">dodaj kolejny telefon</button>
                </div><!--/.container_100-->
           </div><!--/.form-section-->
           
           @for($i=0; $i<5; $i++)
           <?php if($i==0) $data["required"]="required"; else $data["required"]=""; ?>
            <div class="form-section main_job{{$i}} @if($i>0) hide @endif">
                 <h2>Miejsce pracy @if($i>0) #{{$i+1}} @endif</h2>
                 <div class="container_100">
                     @if($i==0)
                        <div class="input-container">
                            {{-- Form::radio("main_job", "1", Input::old("main_job") ) --}}
                            <label class="big">Głowne miejsce pracy</label>
                        </div>
                    @endif
                 </div><!--/.container_100-->
                 <div class="container_100">
                     <div class="input-container">
                         {{ Form::label("unit_name", "nazwa jednostki", ["class" => "small"])}}
                         {{ Form::text("unit_name[$i]", Input::old("unit_name[$i]"), ["class" => "input-style  input-medium", $data["required"]]) }}
                     </div><!--/.input-container-->
                     <div class="input-container">
                         {{ Form::label("unit_type", "rodzaj jednostki", ["class" => "small"]) }}
                         {{ Form::select("unit_type[$i]", $data["unit_type"], Input::old("unit_type[$i]"), ["class" => "input-style  input-medium", $data["required"]]) }}
                     </div><!--/.input-container-->
                     <div class="input-container">
                         {{ Form::label("branch", "oddział", ["class" => "small"])}}
                         {{ Form::text("branch[$i]", Input::old("branch[$i]"), ["class" => "input-style  input-medium", $data["required"]]) }}
                     </div><!--/.input-container-->
                     <div class="input-container">
                         {{ Form::label("position", "stanowisko", ["class" => "small"]) }}
                         {{ Form::select("position[$i]", $data["position"], Input::old("position[$i]"), ["class" => "input-style  input-medium", $data["required"]]) }}
                     </div><!--/.input-container-->
                 </div><!--/.container_100-->
                 <div class="container_100">
                     <div class="input-container">
                         {{ Form::label("job_street", "ulica", ["class" => "small"])}}
                         {{ Form::text("job_street[$i]", Input::old("job_street[$i]"), ["class" => "input-style  input-big", $data["required"]]) }}
                     </div><!--/.input-container-->
                     <div class="input-container">
                         {{ Form::label("job_nr", "nr", ["class" => "small"])}}
                         {{ Form::text("job_nr[$i]", Input::old("job_nr[$i]"), ["class" => "input-style  input-small", $data["required"]]) }}
                     </div><!--/.input-container-->
                 </div><!--/.container_100-->
                 <div class="container_100">
                     <div class="input-container">
                         {{ Form::label("job_postal_code", "kod pocztowy", ["class" => "small"])}}
                         {{ Form::number("job_postal_code[$i][0]", Input::old("postal_code[$i][0]"), ["class" => "input-smaller input-style", "maxlength" => "1", $data["required"]]) }}
                         {{ Form::number("job_postal_code[$i][1]", Input::old("postal_code[$i][1]"), ["class" => "input-smaller input-style", "maxlength" => "1", $data["required"]]) }}
                         -
                         {{ Form::number("job_postal_code[$i][2]", Input::old("postal_code[$i][2]"), ["class" => "input-smaller input-style", "maxlength" => "1", $data["required"]]) }}
                         {{ Form::number("job_postal_code[$i][3]", Input::old("postal_code[$i][3]"), ["class" => "input-smaller input-style", "maxlength" => "1", $data["required"]]) }}
                         {{ Form::number("job_postal_code[$i][4]", Input::old("postal_code[$i][4]"), ["class" => "input-smaller input-style", "maxlength" => "1", $data["required"]]) }}
                     </div><!--/.input-container-->
                     <div class="input-container">
                         {{ Form::label("job_city", "miejscowość", ["class" => "small"])}}
                         {{ Form::text("job_city[$i]", Input::old("job_city[$i]"), ["class" => "input-style  input-medium", $data["required"]]) }}
                     </div><!--/.input-container-->
                 </div><!--/.container_100-->
                 <div class="container_100">
                     <div class="input-container">
                         {{ Form::label("job_province", "województwo", ["class" => "small"]) }}
                         {{ Form::select("job_province[$i]", $data["province"], Input::old("job_province[$i]"), ["class" => "input-style  input-medium", $data["required"]]) }}
                     </div><!--/.input-container-->
                     <div class="input-container">
                         {{ Form::label("job_country", "kraj", ["class" => "small"]) }}
                         {{ Form::select("job_country[$i]", $data["country"], Input::old("job_country[$i]")? Input::old("job_country") : "Polska", ["class" => "input-style  input-medium", $data["required"]]) }}
                     </div><!--/.input-container-->
                 </div><!--/.container_100-->
                 <div class="container_100 padding-30">
                     <button class="btn addMain_job">dodaj kolejne miejsce pracy</button>
                     <button class="btn removeMain_job btn-danger @if($i=="0") hide @endif">Usuń miejsce pracy</button>
                 </div><!--/.container_100-->
            </div><!--/.form-section-->
            @endfor
            
           <div class="form-section">
                <h2>Jestem zainteresowany/a następującymi specjalnościami</h2>
                <div class="input-container spec21">
                    {{ Form::select('interested_specialization[0]', $data['specialization'], Input::old('interested_specialization[0]'), ["class" => "input-style  input-medium", "required"]) }}
                </div><!--/.input-container-->
                <div class="input-container hide spec22">
                    {{ Form::select('interested_specialization[1]', $data['specialization'], Input::old('interested_specialization[1]'), ["class" => "input-style  input-medium"]) }}
                </div><!--/.input-container-->
                <div class="input-container hide spec23">
                    {{ Form::select('interested_specialization[2]', $data['specialization'], Input::old('interested_specialization[2]'), ["class" => "input-style  input-medium"]) }}
                </div><!--/.input-container-->
                <div class="input-container hide spec24">
                    {{ Form::select('interested_specialization[3]', $data['specialization'], Input::old('interested_specialization[3]'), ["class" => "input-style  input-medium"]) }}
                </div><!--/.input-container-->
                <div class="input-container hide spec25">
                    {{ Form::select('interested_specialization[4]', $data['specialization'], Input::old('interested_specialization[4]'), ["class" => "input-style  input-medium"]) }}
                </div><!--/.input-container-->
                <button class="btn addSpecialization2">dodaj kolejną specjalność</button>
           </div><!--/.form-section-->
           <div class="form-section">
                <h2>Preferowana forma kontaktu</h2>
                <div class="input-container">
                    {{ Form::checkbox('contact_email', '1', Input::old('contact_email')) }}
                    {{ Form::label('contact_email', 'e-mail', ["class" => "big"]) }}
                    {{ Form::checkbox('contact_telefon', '1', Input::old('contact_telefon')) }}
                    {{ Form::label('contact_telefon', 'telefon', ["class" => "big"]) }}
                    {{ Form::checkbox('contact_post_job', '1', Input::old('contact_post_job')) }}
                    {{ Form::label('contact_post_job', 'poczta do miejsca pracy', ["class" => "big"]) }}
                    {{ Form::checkbox('contact_post_home', '1', Input::old('contact_post_home')) }}
                    {{ Form::label('contact_post_home', 'poczta na adres korespondencyjny', ["class" => "big"]) }}
                </div><!--/.input-container-->
                <p class="grey  small">(można zaznaczyć więcej niż jedną opcję)</p>
           </div><!--/.form-section-->
<!--                <div class="form-section">
                     <h2>Opłaty</h2>
                     <div class="container_100">
                         <div class="input-container">
                             <input type="checkbox" name="pay">
                             <label class="big">udział w konferencji - X zł</label>
                             <input type="checkbox" name="pay">
                             <label class="big">osoba towarzysząca - Yzł</label>
                         </div>/.input-container
                     </div>/.container_100
                     <div class="container_100">
                         <div class="input-container">
                             <input type="checkbox" name="pay">
                             <label class="big">posiadam kupon promocyjny:</label>
                         </div>
                         <div class="input-container">
                             <input type="text"  class="input-style  input-medium">
                         </div>
                     </div>/.container_100
                     <div class="container_100">
                         <p>Suma Z zł</p>
                     </div>/.container_100
                </div>/.form-section-->
           <div class="form-section">
                <h2>Dane logowania</h2>
                <div class="container_100">
                    <div class="input-container">
                        <label class="small">login</label>
                        <p class="grey pwzCode">[tu pokaże się skopiowany PWZ]</p>
                    </div><!--/.input-container-->
                    <div class="input-container">
                        <p>Twój login to numer PWZ</p>
                    </div><!--/.input-container-->
                </div><!--/.container_100-->
                <div class="container_100">
                    <div class="input-container">
                        <label class="small">hasło</label>
                        <input type="password"  class="input-style  input-medium" required name='password'>
                    </div><!--/.input-container-->
                    <div class="input-container">
                        <label class="small">powtórz hasło</label>
                        <input type="password"  class="input-style  input-medium" required name='password_confirm'>
                    </div><!--/.input-container-->
                </div><!--/.container_100-->
           </div><!--/.form-section-->

           <div class="form-section">
                <div class="container_100">
                    <div class="input-container">
                        {{ Form::checkbox('reg1', '1', Input::old('reg1')) }}
                        <label class="smaller">Wyrażam zgodę na przetwarzanie przez Organizatorów, to jest przez: spółkę Symposion MH Sp. z o. o. sp. k. z siedzibą w Poznaniu przy ul. Obornickiej 229 oraz Małgorzatę Hein, prowadzącą działalność pod nazwą MAŁGORZATA HEIN "SYMPOSION", ul. Herbowa 17, Poznań, adres do korespondencji: ul. Obornicka 229, Poznań (administratorów danych) przekazanych Organizatorom moich danych osobowych, w celach działań marketingowych związanych ze świadczeniem obecnie i przyszłości usług oraz sprzedażą produktów przez każdego z Organizatorów jak i jego partnerów, w tym wyrażam zgodę na przekazywanie mi informacji handlowych drogą pocztową, e-mailową lub telefoniczną. Niniejsza zgoda jest dobrowolna. Mają Państwo prawo do jej cofnięcia, jak również prawo do wglądu do swoich danych i ich poprawiania. Jednocześnie informujemy, iż odmowa wyrażenia zgodny skutkować będzie brakiem możliwości przesłania Państwu ważnych dla Państwa praktyki informacji handlowych dotyczących korzystania z usług świadczonych przez Organizatorów.</label>
                    </div><!--/.input-container-->
                </div><!--/.container_100-->
                <div class="container_100">
                    <div class="input-container">
                        {{ Form::checkbox('reg2', '1', Input::old('reg2'), ["required"]) }}
                        <label class="smaller">Zapoznałem/am się z <a href="{{ url('Regulamin.pdf') }}" target="_blank">Regulaminem</a>.</label>
                    </div><!--/.input-container-->
                </div><!--/.container_100-->
                <div class="container_100">
                    <div class="input-container">
                        {{ Form::checkbox('reg3', '1', Input::old('reg3')) }}
                        <label class="smaller">Wyrażam zgodę na otrzymywanie newslettera firmy Symposion.</label>
                    </div><!--/.input-container-->
                </div><!--/.container_100-->
           </div><!--/.form-section-->

           <div class="form-section  center">
               <p class="controllMessages" style="color:red"></p>
                <input type="submit" value="Zarejestruj">
                <a href="{{ URL::previous() }}" class="cancel">Anuluj</a>
           </div><!--/.form-section-->
        {{ Form::close() }}

        </div><!--/.wrapper-->
    </div>
    

    <!-- JavaScript -->
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery/core/jquery-1.11.1.min.js') }}"></script>
    <!-- Jquery plugins:-->
    <script src="{{ url('conference/js/plugins/countdown.js') }}"></script>
    
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery-ui/core/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/transition.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/alert.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/button.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/carousel.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/collapse.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/dropdown.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/modal.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/scrollspy.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tab.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tooltip.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/popover.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/affix.js') }}"></script>
    <script src="{{ url('conference/js/project/project.js') }}"></script>
    <script src="{{ url('conference/js/project/form.js') }}"></script>
    <script>
        $( "form" ).submit(function() {
            var password = $('input[name=password]').val();
            var password_confirm = $('input[name=password_confirm]').val();
            
            if (password_confirm != password) {
              $('p.controllMessages').text('Hasła muszą być takie same');
              return false;
            } else if (checkPwz() == false){
                $('p.controllMessages').text('Wpisz poprawnie kod pwz');
                return false;
            }
        });
         
        $(document).on('keyup', 'input.pwz', function(e){
                $('p.pwzCode').text(checkPwz());
        });
        
        $(document).on('keyup', 'input[name=email]', function(e){
                $('p.pwzCode').text(checkPwz());
        });
        
        function checkPwz(){
            var option = $('#who').find(":selected").val();
            
            var pwz1 = $('input[name="pwz[0]"').val();
            var pwz2 = $('input[name="pwz[1]"').val();
            var pwz3 = $('input[name="pwz[2]"').val();
            var pwz4 = $('input[name="pwz[3]"').val();
            var pwz5 = $('input[name="pwz[4]"').val();
            var pwz6 = $('input[name="pwz[5]"').val();
            var pwz7 = $('input[name="pwz[6]"').val();
            if(option == 'lekarz'){
                pwz1 = pwz1.substr(pwz1.length - 1);
                pwz2 = pwz2.substr(pwz2.length - 1);
                pwz3 = pwz3.substr(pwz3.length - 1);
                pwz4 = pwz4.substr(pwz4.length - 1);
                pwz5 = pwz5.substr(pwz5.length - 1);
                pwz6 = pwz6.substr(pwz6.length - 1);
                pwz7 = pwz7.substr(pwz7.length - 1);

                var controll = 1;

                if($.inArray(pwz1, ["1","2","3","4","5","6","7","8","9"])<0)
                    controll = 0;
                if($.inArray(pwz2, ["0","1","2","3","4","5","6","7","8","9"])<0)
                    controll = 0;
                if($.inArray(pwz3, ["0","1","2","3","4","5","6","7","8","9"])<0)
                    controll = 0;
                if($.inArray(pwz4, ["0","1","2","3","4","5","6","7","8","9"])<0)
                    controll = 0;
                if($.inArray(pwz5, ["0","1","2","3","4","5","6","7","8","9"])<0)
                    controll = 0;
                if($.inArray(pwz6, ["0","1","2","3","4","5","6","7","8","9"])<0)
                    controll = 0;
                if($.inArray(pwz7, ["0","1","2","3","4","5","6","7","8","9"])<0)
                    controll = 0;

                var con = (pwz2 * 1) + (pwz3 * 2) + (pwz4 * 3) + (pwz5 * 4) + (pwz6 * 5) + (pwz7 * 6);
                var modulo = con%11;
                if(modulo != pwz1)
                    controll = 0;
                if(controll == 0)
                    return 'Wprowadź poprawnie kod PWZ.';
                else
                   return pwz1+pwz2+pwz3+pwz4+pwz5+pwz6+pwz7;
            } else if(option == 'diagnosta'){
                if(pwz1 && pwz2 && pwz3 && pwz4 && pwz5)
                    return pwz1+pwz2+pwz3+pwz4+pwz5;
                else
                    return 'Wprowadź poprawnie kod PWZ.';
            } else if(option == 'pielęgniarka'){
                if(pwz1 && pwz2 && pwz3 && pwz4 && pwz5 && pwz6 && pwz7)
                    return pwz1+pwz2+pwz3+pwz4+pwz5+pwz6+pwz7+'P';
                else
                    return 'Wprowadź poprawnie kod PWZ.';
            } else if(option == 'dietetyk' || option == 'student'){
                if($('input[name=email]').val())
                    return $('input[name=email]').val();
                else
                    return 'Wprowadź poprawnie adres email.'
            }
        }
        
        function checkWho(){
            var option = $('#who').find(":selected").val();
            if(option == 'lekarz') {
                var text = '<label for="pwz" class="small">numer PWZ</label>';
                text = text + '<input class="input-mini first pwz" maxlength="1" required="required" min="0" max="9" name="pwz[0]" type="number">';
                for(var i =1; i<6; i++) {
                    text = text + '<input class="input-mini pwz" maxlength="1" required="required" min="0" max="9" name="pwz['+i+']" type="number">';
                }
                text = text + '<input class="input-mini last pwz" maxlength="1" required="required" min="0" max="9" name="pwz[6]" type="number">';
            } else if(option == 'diagnosta') {
                var text = '<label for="pwz" class="small">numer PWZ</label>';
                text = text + '<input class="input-mini first pwz" maxlength="1" required="required" min="0" max="9" name="pwz[0]" type="number">';
                for(var i =1; i<4; i++) {
                    text = text + '<input class="input-mini pwz" maxlength="1" required="required" min="0" max="9" name="pwz['+i+']" type="number">';
                }
                text = text + '<input class="input-mini last pwz" maxlength="1" required="required" min="0" max="9" name="pwz[4]" type="number">';
            } else if(option == 'pielęgniarka') {
                var text = '<label for="pwz" class="small">numer PWZ</label>';
                text = text + '<input class="input-mini first pwz" maxlength="1" required="required" min="0" max="9" name="pwz[0]" type="number">';
                for(var i =1; i<7; i++) {
                    text = text + '<input class="input-mini pwz" maxlength="1" required="required" min="0" max="9" name="pwz['+i+']" type="numbert">';
                }
                text = text + '<input class="input-mini last pwz" maxlength="1" required="required" min="0" max="9" name="pwz[7]" type="text" value="P" disabled style="background-color: #d3d3d3;">';
            } else if(option == 'dietetyk' || option == 'student') {
                var text = '<label for="pwz" class="small">numer PWZ</label>';
                text = text + '<input class="input-mini first pwz" maxlength="1" required="required" min="0" max="9" name="pwz[0]" type="text" value="-" readonly style="background-color: #d3d3d3;">';
                for(var i =1; i<6; i++) {
                    text = text + '<input class="input-mini pwz" maxlength="1" required="required" min="0" max="9" name="pwz['+i+']" type="text" value="-" readonly style="background-color: #d3d3d3;">';
                }
                text = text + '<input class="input-mini last pwz" maxlength="1" required="required" min="0" max="9" name="pwz[6]" type="text" value="-" readonly style="background-color: #d3d3d3;">';
            } 
            $('#pwz').html(text);
        }
        
        checkWho();
        $('#who').change(function(){
            checkWho();
            $('p.pwzCode').text(checkPwz());
        });
        
    </script>
</body>

</html>
