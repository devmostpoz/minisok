
<!DOCTYPE html>
<html lang="pl">
    
    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <title>{{ $title}}</title>
    <style>
        img.baner {width: 100%;}
        .btn-sympo       {color: #fff;background-color: #197B3C;border-color: #17793A ;}
        .btn-sympo:hover {color: #fff!important;background-color: #075031;border-color: #197B3C;}
    </style>
    <link href="{{ url('conference/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('conference/css/bootstrap.mini.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    </head>
    <body>

                <div class="row">
                    <div class="col-lg-12">
                        <img class="baner" src='{{ url('images/conferences/'.$conference->image)}}' >
                    </div>
                </div>
                @if($conference->registerButton!=1)
                    <div class='row' style="margin-top: 15px;">
                        <div class="col-lg-2 col-lg-offset-5">
                            <a href="" class="btn btn-sympo" data-toggle="modal" data-target="#loginModal">
                                @if($conference->lang!='en')
                                    ZAREJESTRUJ SIĘ
                                @else
                                    SIGN UP
                                @endif
                            </a>
                        </div>
                    </div>
                @endif
                <div class='row descriptionText' >
                    <div class="col-lg-12">
                        {{ $conference->description }}
                    </div>
                </div>
                @if($conference->registerButton!=1)
                    <div class='row'>
                        <div class="col-lg-2 col-lg-offset-5">
                            <a href="" class="btn btn-sympo" data-toggle="modal" data-target="#loginModal">
                            @if($conference->lang!='en')
                                    ZAREJESTRUJ SIĘ
                            @else
                                SIGN UP
                            @endif
                            </a>
                        </div>
                    </div>
                @endif
    <!-- JavaScript -->
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery/core/jquery-1.11.1.min.js') }}"></script>
    <!-- Jquery plugins:-->
    <script src="{{ url('conference/js/plugins/countdown.js') }}"></script>
    
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery-ui/core/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/transition.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/alert.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/button.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/carousel.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/collapse.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/dropdown.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/modal.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/scrollspy.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tab.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tooltip.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/popover.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/affix.js') }}"></script>
    <script src="{{ url('conference/js/project/project.js') }}"></script>
    <script src="{{ url('conference/js/project/script.js') }}"></script>
        @include('conference.popover')
        
        @if(Session::has('success') or isset($success))
            <!-- Modal -->
            <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                      <h1>Zostałeś poprawnie zarejestrowany</h1>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                  </div>
                </div>
              </div>
            </div>
            
            <script>
                $('#successModal').modal('show')
            </script>
        @endif
    </body>
    </html>