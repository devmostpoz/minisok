<!DOCTYPE html>
<!-- Change to your language -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title }}</title>

    <!-- Core Bootstrap CSS -->
    <link href="{{ url('conference/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('conference/css/bootstrap.mini.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
        <script>
          function initialize() {
             var styles = [
            {
                  stylers: [
                    { saturation: -100 }
                  ]
                },
              ];
            var mapCanvas = document.getElementById('map-canvas');
            var myLatlng = new google.maps.LatLng(50.0632782, 19.9276706);
            var mapOptions = {
              center: new google.maps.LatLng(50.0632782, 19.9276706),
              zoom: 16,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              scrollwheel: false,
              draggable: false,
            }
            var styledMap = new google.maps.StyledMapType(styles,
            {name: "Styled Map"});
            var map = new google.maps.Map(mapCanvas, mapOptions)
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                });
            }
          google.maps.event.addDomListener(window, 'load', initialize);
        </script>
</head>

<body>
    <header class="header  container  relative">
        <div class="wrapper">
           <div class="row">
               <div class="col-lg-6  col-md-6  col-sm-12">
               </div><!--/.col-->
               <div class="col-lg-6  col-md-6  col-sm-12">
                   <div class="container">
                       <img src="{{ url('conference/img/symposion_logo.png') }}">
                   </div>
                   <div class="container  relative">
                        <img src="{{ url('conference/img/herb.png') }}"  class="absolute  herb"/>
                        <h3 class="blue">VIII Ogólnopolska Konferencja</h3>
                        <h1 class="white  main-header">Kontrowersje<br/> w pediatrii</h1>
                        <h1 class="white  eng">3rd Controversies in Neonatology</h1>
                   </div>
                   <div class="container  countdown">
                       <h3 class="white">Otwarcie konferencji za:</h3>
                       <div id="clock"></div>
                   </div>
                   <div class="container">
                        <a href="" class="button" data-toggle="modal" data-target="#loginModal">zarejestruj się</a>
                   </div>
               </div><!--/.col-->
           </div><!--/.row-->
           <div class="row">
                <div class="col-lg-6  col-md-6  col-sm-12">
                    <div class="info  container">
                        <img src="{{ url('conference/img/icon_date.png') }}">
                        <h2 class="white" style="font-size:33px;line-height:55px">30 stycznia - 31 stycznia 2015</h2>
                    </div><!--/.info-->
               </div><!--/.col-->
               <div class="col-lg-6  col-md-6  col-sm-12">
                    <div class="info  container">
                        <img src="{{ url('conference/img/icon_spot.png') }}">
                        <h2 class="white">Kraków <br/>
                            <span>Auditorium Maximum UJ</span>
                        </h2>
                    </div><!--/.info-->
               </div><!--/.col-->
           </div><!--/.row-->
        </div><!--/.wrapper-->
    </header>

    <section class="greetings  container">
        <div class="wrapper  relative">
            <div class="row">
                <div class="col-lg-12  col-md-12  col-sm-12">
                    <h2 class="blue">Szanowni Państwo,</h2>
                    <p class="grey just">Serdecznie zapraszam Państwa do udziału w VIII Ogólnopolskiej konferencji „Kontrowersje w pediatrii”, która odbędzie się w dniach 30 – 31 stycznia 2015 roku w Krakowie , w Auditorium Maximum UJ przy ul. Krupniczej. </p>
                    <p class="grey just">Zeszłoroczna konferencja rozpoczęła obchody Jubileuszu 150 lat Kliniki Chorób Dzieci, będącej najstarszą kliniką pediatryczną na ziemiach polskich i chciałbym, aby VIII „Kontrowersje” zakończyły nasze uroczystości. </p>
                </div><!--/.col-->
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-8  col-md-8  col-sm-12">
                    <p class="grey just">W trakcie konferencji zapraszam również do obejrzenia wystawy z okazji Jubileuszu Kliniki Chorób Dzieci, która będzie dostępna w Bibliotece Jagiellońskiej (wejście od ul. Oleandry 3).
                        <br><br>Tegoroczny program opracowaliśmy zgodnie z Państwa sugestiami i propozycjami tematów. Poruszymy więc temat kontrowersji w leczeniu choroby Kawasaki, bólów stawów u dzieci oraz zmian skórnych w chorobach zakaźnych. Wraz z zaproszonymi psychiatrami i psychologami omówimy zagadnienia dotyczące diagnostyki i terapii autyzmu. Rozpatrzymy także problem zaburzeń wzrastania oraz nawracających infekcji u dzieci. 
                        <br><br>Po raz trzeci mam również przyjemność zaprosić Państwa do udziału w międzynarodowych sesjach neonatologicznych, które w tej edycji „Kontrowersji” przebiegać będą równolegle do obrad polskojęzycznych. Tegoroczny zakres tematyczny „Controversies in Neonatology” obejmie tematykę wentylacji noworodków, wad wrodzonych oraz terapii in – vitro, bólu u noworodków, a także karmienia niemowląt po wypisie z oddziału intensywnej terapii. Podobnie jak w poprzednich latach, w sesjach neonatologicznych wykłady wygłoszą wybitni neonatolodzy z całego świata.
                    </p>
                    <p class="grey just">Jeszcze raz serdecznie zapraszam do Krakowa.</p>
                </div><!--/.col-->
                <div class="col-lg-8  col-md-8  col-sm-12">
                </div><!--/.col-->

                <div class="person_big absolute" style='bottom: 0px; margin-bottom: 0px;'>
                    <img src="{{ url('conference/img/lekarz.png') }}" class="photo" />
                    <p>prof. dr hab. Jacek Józef Pietrzyk </p>
                    <p class="dark">Przewodniczący Komitetu Naukowego <br>VIII Ogólnopolskiej Konferencji <br>„Kontrowersje w pediatrii” <br>oraz „ 3rd Controversies in Neonatology”</p>
                    <img src="{{ url('conference/img/pietrzyk_podpis.jpg') }}" style='max-height: 72px;'>                    
                </div>
            </div><!--/.row-->
        </div><!--/.wrapper-->
    </section><!--/.greetings-->

    <section class="commitee  container">
        <div class="wrapper">
            <h2 class="white  capital">komitet naukowy</h2>
            <h3 class="white text-center">Przewodniczący:</h3>
            <div class="row">
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    
                </div>
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <!--<img src="../../img/person_small.png') }}">-->
                    <p class="blue">prof. dr hab. n. med. Jacek J. Pietrzyk<br/>
                    </p>
                </div><!--/.col-->
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    
                </div>
            </div>
            <h3 class="white text-center">Członkowie:</h3>
            <div class="row">
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <!--<img src="../../img/person_small.png') }}">-->
                    <p class="blue">prof. dr hab. Jerzy B. Starzyk<br/>
                    </p>
                </div><!--/.col-->
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <!--<img src="../../img/person_small.png') }}">-->
                    <p class="blue">prof. dr hab. Tomasz Wolańczyk<br/>
                    </p>
                </div><!--/.col-->
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <!--<img src="../../img/person_small.png') }}">-->
                    <p class="blue">prof. dr hab. Jacek Wysocki<br/>
                    </p>
                </div><!--/.col-->
                <div class="col-lg-4  col-md-4  col-sm-4  person">
                   <!--<img src="../../img/person_small.png') }}">-->
                    <p class="blue">dr hab. Szymon Skoczeń<br/>
                    </p>
                </div><!--/.col-->
                <div class="col-lg-4  col-md-4  col-sm-4  person">
                   <!--<img src="../../img/person_small.png') }}">-->
                    <p class="blue">dr Bożena Pilch<br/>
                    </p>
                </div><!--/.col-->
            </div><!--/.row-->
<!--            <div class="row  other">
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Maciej Kaczmarski  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Ryszard Kurzawa  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Ewa Małecka-Tendera  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. Michał Pirożyński  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Anna Pituch-Noworolska  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Andrzej Radzikowski  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Andrzej Rudziński  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Lidia Rutkowska-Sak  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Jerzy B. Starzyk  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Leszek Szenborn  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Tomasz Wolańczyk  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">prof. dr hab. Jacek Wysocki  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">dr hab. Grzegorz Lis  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">dr hab. Szymon Skoczeń  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">dr hab. Jerzy Sułko  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">dr Anita Bryńska  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">dr Urszula Kania  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">dr Bożena Pilch  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">dr Maciej Pilecki  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">dr Zofia Szepczyńska  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">dr Jon Vanderhoof  <br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                   <img src="../../img/person_small.png') }}">
                    <p class="blue">mgr Maria Marczyk  <br/>
                    </p>
                </div>/.col
            </div>/.row-->
        </div><!--/.wrapper-->
<!--        <div class="button-container">
            <p class="button  other-show">zobacz wszystkich członków komitetu naukowego</p>
        </div>-->
    </section><!--/.comitee-->

    <section class="programme  container">
        <div class="wrapper">
            <h2 class="blue  capital">program</h2>

            <div role="tabpanel">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#day1" aria-controls="day1" role="tab" data-toggle="tab">
                        Dzień I <br/><span>30.01.2015</span>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#day2" aria-controls="day2" role="tab" data-toggle="tab">
                        Dzień II <br/><span>31.01.2015</span>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#eng" aria-controls="eng" role="tab" data-toggle="tab" style="font-size:14px; line-height:14px;padding:31px 0px;">
                        <span>3rd Controversies in Neonatology</span>
                    </a>
                </li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="day1">
                    <h3 class="blue">Aula Duża A</h3>
                    <div class="row">
                        <div class="col-lg-6  col-md-6  col-sm-12">

                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>00</sup></p>
                                <p class="dark">Rozpoczęcie konferencji</p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue  hour"></p>
                                <p class="dark2">Sesja 1<br /><span>Choroba Kawasaki – postać poronna i atypowa – kontrowersje w leczeniu – przypadki<br/>moderator: <i>prof.dr hab. Jacek Wysocki</i></span></p> 
                            </div><!--/.schedule-->                            
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>15</sup></p>
                                <p class="dark"><span>Powikłania sercowo-naczyniowe w chorobie Kawasaki u dzieci i ich wczesne i odległe następstwa.<br/>prowadzący: <i>prof. dr hab.n.med Andrzej Rudziński</i></span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>35</sup></p>
                                <p class="dark"><span>Choroba Kawasaki – czy dziś wiemy więcej o jej leczeniu?<br/>prowadzący: <i>dr Urszula Kania</i></span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>55</sup></p>
                                <p class="dark"><span>Dyskusja</span></p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue  hour"></p>
                                <p class="dark2">Sesja 2<br /><span>Zaburzenia wzrastania jako problem wielodyscyplinarny. Kontrowersje wokół leczenia hormonem wzrostu.<br/>moderator: <i>prof. dr hab. n.med. Jerzy B.Starzyk</i></span></p> 
                            </div><!--/.schedule-->                            
                            <div class="schedule  container">
                                <p class="blue  hour">11<sup>15</sup></p>
                                <p class="dark"><span>Terapia hormonem wzrostu – leczenie choroby czy poprawianie natury?<br/>prowadzący: <i>prof. dr hab.n.med. Ewa Małecka-Tendera</i></span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">11<sup>35</sup></p>
                                <p class="dark"><span>Kiedy podejrzewać niedobór hormonu wzrostu - u dziecka z niskim wzrostem czy z zahamowaniem wzrastania?<br/>prowadzący: <i>prof. dr hab. n. med. Jerzy B. Starzyk</i></span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">11<sup>55</sup></p>
                                <p class="dark"><span>Dyskusja</span></p> 
                            </div><!--/.schedule-->  
                           <div class="schedule  container">
                                <p class="blue  hour">12<sup>15</sup></p>
                                <p class="dark"><span>Wymioty u noworodka-prezentacja przypadku<br/>prowadzący: <i>dr Katarzyna Hrnčiar</i></span></p> 
                            </div><!--/.schedule-->  
						   <div class="schedule  container">
                                <p class="blue  hour">12<sup>30</sup></p>
                                <p class="dark"><span>Lunch</span></p> 
                            </div><!--/.schedule-->                                                       
                            <div class="schedule  container">
                                <p class="blue  hour"></p>
                                <p class="dark2">Sesja 3<br /><span>Bóle stawów- pediatra, reumatolog czy ortopeda?<br/>moderator: <i>dr Bożena Pilch</i></span></p> 
                            </div><!--/.schedule-->                            
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>15</sup></p>
                                <p class="dark"><span>Kiedy kierować dziecko do reumatologa?<br/>prowadzący: <i>prof. dr hab. Lidia Rutkowska-Sak</i></span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>35</sup></p>
                                <p class="dark"><span>Bóle stawów u dzieci- przyczyny ortopedyczne i ich charakterystyka.<br/>prowadzący: <i>dr hab. Jerzy Sułko</i></span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>55</sup></p>
                                <p class="dark"><span>Dyskusja</span></p> 
                            </div><!--/.schedule-->   
                            
                          
                        </div><!--/.col-->
                        
                        
                        
                        
                        <div class="col-lg-6  col-md-6  col-sm-12">

                              <div class="schedule  container">
                                <p class="blue  hour">14<sup>15</sup></p>
                                <p class="dark2">Sesja 4 <br /> <span>Bezpieczeństwo w nebulizacji – panel pytań i odpowiedzi<br/>prowadzący: <i>prof. dr hab. Michał Pirożyński</i></span></p> 
                            </div><!--/.schedule-->        
							<div class="schedule  container">
                                <p class="blue  hour">15<sup>15</sup></p>
                                <p class="dark"><span>Przerwa na kawę</span></p> 
                            </div><!--/.schedule-->      

   							
							<div class="schedule  container">
                                <p class="blue  hour"></p>
                                <p class="dark2">Sesja 5 <br /> <span>Ważne aspekty chorób obturacyjnych u najmłodszych dziecii</span></p> 
                            </div><!--/.schedule--> 
                            <div class="schedule  container">
                                <p class="blue  hour">15<sup>30</sup></p>
                                <p class="dark"><span>Czy częsta antybiotykoterapia we wczesnym okresie życia może być
																	tylko markerem nierozpoznanej astmy, czy wręcz sprzyja jej wystąpieniu?
																	Kilka słów o teorii makrobiotycznej<br/>prowadzący: <i>prof. dr hab. Ryszard Kurzawa</i></span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                 <p class="blue  hour"><sup></sup></p>
                                <p class="dark"><span>Zespół krupu – głos do consensusu w sprawie diagnostyki i możliwości terapii<br/>prowadzący: <i>prof. dr hab. Andrzej Fal</i></span></p> 
                            </div><!--/.schedule-->
							
							<div class="schedule  container">
                                <p class="blue  hour"></p>
                                <p class="dark2">Sesja 6 <br /> <span>Kontrowersje w alergii pokarmowej</span></p> 
                            </div><!--/.schedule--> 
							    
							
							<div class="schedule  container">
                                <p class="blue  hour">16<sup>10</sup></p>
                                <p class="dark"><span>Food allergy management – controversies in our practice<br/>prowadzący: <i>dr Jon Vanderhoof /Holandia/</i></span></p> 
                            </div><!--/.schedule-->
							
							<div class="schedule  container">
                                <p class="blue  hour">16<sup>30</sup></p>
                                <p class="dark"><span>Czy myślimy o kosztach w leczeniu alergii pokarmowej?<br/>prowadzący: <i>prof. dr hab. Maciej Kaczmarski</i></span></p> 
                            </div><!--/.schedule-->
							
							<div class="schedule  container">
                                <p class="blue  hour">16<sup>50</sup></p>
                                <p class="dark"><span>Dyskusja</span></p> 
                            </div><!--/.schedule-->
							
						<div class="schedule  container">
                                <p class="blue  hour">17<sup>10</sup></p>
                                <p class="dark"><span>Przerwa na kawę</span></p> 
                            </div><!--/.schedule--> 
							
							<div class="schedule  container">
                                <p class="blue  hour">17<sup>25</sup></p>
                                <p class="dark"><span>Leczenie bólu i gorączki. Ibuprofen i paracetamol – razem czy osobno?<br/>prowadzący: <i>prof. dr hab. Andrzej Radzikowski</i></span></p>  
                            </div><!--/.schedule--> 
							<div class="schedule  container">
                                <p class="blue  hour"><sup></sup></p>
                                <p class="dark"><span>Zastosowanie fitoterapii w leczeniu infekcji górnych dróg oddechowych u dzieci<br/>prowadzący: <i>prof. dr hab. Jacek Piątek</i></span></p>  
                            </div><!--/.schedule--> 
							<div class="schedule  container">
                                <p class="blue  hour">17<sup>25</sup></p>
                                <p class="dark"><span>Innowacyjne i tradycyjne ekstrakty roślinne we wspomaganiu fizjologii dróg oddechowych<br/>prowadzący: <i>mgr farm. Kamil Żarłok</i></span></p>  
                            </div><!--/.schedule--> 
							
							
							
							<div class="schedule  container">
                                <p class="blue  hour">18<sup>40</sup></p>
                                <p class="dark"><span>Wieczór kulturalny</span></p> 
                            </div><!--/.schedule--> 
							
							<div class="schedule  container">
                                <p class="blue  hour">19<sup>30</sup></p>
                                <p class="dark"><span>Koktajl</span></p> 
                            </div><!--/.schedule--> 
                        </div><!--/.col-->
                    </div><!--/.row-->
                </div>

                <div role="tabpanel" class="tab-pane" id="day2">
                    <h3 class="blue">Aula Duża A</h3>
                    <div class="row">
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">9<sup>30</sup></p>
                                <p class="dark">Sesja 7 <br/> 
                                <span>Autyzm i spektrum autyzmu-trudności diagnostyczne, możliwości terapii <br/>moderator: <i>prof. dr hab. Tomasz Wolańczyk</i>
                                </span></p> 
                            </div><!--/.schedule-->
                        
						<div class="schedule  container">
                                <p class="blue  hour">9<sup>30</sup></p>
                                <p class="dark"><span>Zaburzenia spektrum autyzmu – co powinno niepokoić pediatrów w pierwszych dwóch latach życia dziecka?<br/>prowadzący: <i>dr Maciej Pilecki</i></span></p>  
                            </div><!--/.schedule--> 

						<div class="schedule  container">
                                <p class="blue  hour">9<sup>50</sup></p>
                                <p class="dark"><span>Terapia autyzmu - czy wszystko jedno?<br/>prowadzący: <i>prof. dr hab. Tomasz Wolańczyk</i></span></p>  
                            </div><!--/.schedule--> 
						
						<div class="schedule  container">
                                <p class="blue  hour">10<sup>10</sup></p>
                                <p class="dark"><span>Szczepienia a spektrum zaburzeń autystycznych<br/>prowadzący: <i>dr Anita Bryńska</i></span></p>  
                            </div><!--/.schedule--> 

						<div class="schedule  container">
                                <p class="blue  hour">10<sup>30</sup></p>
                                <p class="dark"><span>Plusy i minusy wczesnej diagnozy zaburzeń ze spektrum autyzmu – perspektywa psychologiczna<br/>prowadzący: <i>mgr Maria Marczyk</i></span></p>  
                            </div><!--/.schedule--> 
							
						
							<div class="schedule  container">
                                <p class="blue  hour">10<sup>50</sup></p>
                                <p class="dark"><span>Możliwości diagnozy i terapii w Krakowie i województwie małopolskim<br/>prowadzący: <i>dr Zofia Szepczyńska</i></span></p>  
                            </div><!--/.schedule--> 
							
								<div class="schedule  container">
                                <p class="blue  hour">11<sup>10</sup></p>
                                <p class="dark"><span>Dyskusja</span></p>  
                            </div><!--/.schedule--> 
							
								<div class="schedule  container">
                                <p class="blue  hour">11<sup>40</sup></p>
                                <p class="dark"><span>Przerwa na kawę</span></p>  
                            </div><!--/.schedule--> 
							
							
							

						  <div class="schedule  container">
                                <p class="blue  hour">12<sup>00</sup></p>
                                <p class="dark">Sesja 8 <br/> 
                                <span>Czy możemy pomóc dziecku z nawracającymi  infekcjami dróg oddechowych?<br/>moderator: <i>dr hab. Szymon Skoczeń</i>
                                </span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">12<sup>00</sup></p>
                                <p class="dark"><span>Nawracające infekcje górnych dróg oddechowych u dzieci – leczenie chirurgiczne? <br/> prowadzący: <i>prof. dr hab. Elżbieta Hassman-Poznańska</i></span></p> 
                            </div><!--/.schedule-->
							<div class="schedule  container">
                                <p class="blue  hour">12<sup>20</sup></p>
                                <p class="dark"><span>Czy i kiedy diagnozować dzieci z nawracającymi infekcjami układu oddechowego? <br/> prowadzący: <i>prof. dr hab. Anna Bręborowicz</i></span></p> 
                            </div><!--/.schedule-->
							<div class="schedule  container">
                                <p class="blue  hour">12<sup>40</sup></p>
                                <p class="dark"><span>Nawracające infekcje dróg oddechowych u dzieci a niedobory odporności <br/> prowadzący: <i>prof. dr hab. Anna Pituch-Noworolska</i></span></p> 
                            </div><!--/.schedule-->
							<div class="schedule  container">
                                <p class="blue  hour">13<sup>00</sup></p>
                                <p class="dark"><span>Nawracające zapalenia oskrzeli a norma budowlana <br/> prowadzący: <i>dr hab. Grzegorz Lis</i></span></p> 
                            </div><!--/.schedule-->
							<div class="schedule  container">
                                <p class="blue  hour">13<sup>20</sup></p>
                                <p class="dark"><span>Dyskusja </span></p> 
                            </div><!--/.schedule-->
                        </div><!--/.col--> 
                        
						
						<div class="col-lg-6  col-md-6  col-sm-12">
						

						<div class="schedule  container">
                                <p class="blue  hour">13<sup>40</sup></p>
                                <p class="dark">Sesja 9 <br/> 
                                <span>Panel ekspertów Kontrowersje w szczepieniach – fakty i mity<br/>Uczestnicy: <i>prof. dr hab. Piotr Albrecht, <br/> prof. dr hab. Leszek Szenborn, <br/>prof. dr hab. Waleria Hryniewicz </i> 
                                </span></p> 
                            </div><!--/.schedule-->
								<div class="schedule  container">
                                <p class="blue  hour">14<sup>40</sup></p>
                                <p class="dark"><span>Lunch </span></p> 
                            </div><!--/.schedule-->
                       
						
						
						
						<div class="schedule  container">
                                <p class="blue  hour">15<sup>40</sup></p>
                                <p class="dark">Sesja 10 <br/> 
                                <span>Enigmatyczny obraz zmian skórnych w chorobach zakaźnych<br/>Moderator: <i>prof. dr hab. Jacek Wysocki</i> 
                                </span></p> 
                            </div><!--/.schedule-->
							
								<div class="schedule  container">
                                <p class="blue  hour">15<sup>40</sup></p>
                                <p class="dark"><span>prof. dr hab. Jacek Wysocki </span></p> 
                            </div><!--/.schedule-->
                       
						 
								<div class="schedule  container">
                                <p class="blue  hour">16<sup>00</sup></p>
                                <p class="dark"><span>prof. dr hab. Leszek Szenborn </span></p> 
                            </div><!--/.schedule-->
						
								<div class="schedule  container">
                                <p class="blue  hour">16<sup>20</sup></p>
                                <p class="dark"><span>Dyskusja </span></p> 
                            </div><!--/.schedule-->
						
								<div class="schedule  container">
                                <p class="blue  hour">16<sup>40</sup></p>
                                <p class="dark"><span>Zakończenie konferencji </span></p> 
                            </div><!--/.schedule-->
                      
							
                            
                        </div><!--/.col-->
                    </div><!--/.row-->
                </div>

                <div role="tabpanel" class="tab-pane" id="eng">
                    <div class="row margin-top">
                        <img src="{{ url('conference/img/kontrowersje_logo.png') }}" class="img-responsive center-block" >
                    </div>
                    
                    <h3 class="blue">Patronat - Polskie Towarzystwo Neonatologiczne. </h3>
                    <h3 class="blue">30-31 January 2015 – Aula Średnia<br> Tłumaczenie symultaniczne.</h3>
                    <div class="row">
                        
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  miniHour"></p>
                                <p class="blue"><h3>January 30, 2015</h3></span></p> 
                            </div><!--/.schedule-->  
                            
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>30</sup> </p>
                                <p class="dark3">Opening Conference <br/> </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">13<sup>30</sup> - 14<sup>50</sup></p>
                                <p class="dark3">Session 1<br/> 
                                    <span>Volume vs pressure ventilation-which is better?
                                    </span><br/> 
                                    <span>Chairman: professor Andrzej Piotrowski
                                    </span><br/> 
                                </p> 
                                <p class="blue miniHour">13<sup>30</sup> - 14<sup>00</sup></p>
                                <p class="dark3">
                                    <span>Watching for Pressure or Volume when Caring for Newborns
is More than Shaken, not Stirred Martini <br/> professor Jan Mazela
                                    </span>
                                </p> 
                                <p class="blue miniHour">14<sup>00</sup> - 14<sup>30</sup></p>
                                <p class="dark3">
                                    <span>
									Neonatal Ventilation: Volume versus Pressure Ventilation
is More than Just Coke versus Pepsi <br/> Dr. Steven Donn
                                    </span>
                                </p> 
                                <p class="blue miniHour">14<sup>30</sup> - 14<sup>50</sup></p>
                                <p class="dark3">
                                    <span>Discussion
                                    </span><br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">14<sup>50</sup> - 15<sup>00</sup></p>
                                <p class="dark3">Coffee break<br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                             <div class="schedule  container">
                                <p class="blue miniHour">15<sup>00</sup> - 16<sup>20</sup></p>
                                <p class="dark3">Session 2<br/> 
                                    <span>Do we know how to feed the preterm/SGA infants after discharge from NICU?
                                    </span><br/> 
                                    <span>Chairman: professor Przemko Kwinta
                                    </span>
                                </p> 
                                <p class="blue miniHour">15<sup>00</sup> - 15<sup>30</sup></p>
                                <p class="dark3">
                                    <span>Complementary feeding for preterm infants after discharge.
                                    </span><br>
                                    <span>What, when and how?
                                    </span><br>
                                    <span>professor Hanna Szajewska
                                    </span>
                                </p> 
                                <p class="blue miniHour">15<sup>30</sup> - 16<sup>00</sup></p>
                                <p class="dark3">
                                    <span>Initial nutritional mamagement of the preterm infant
                                    </span><br>
                                    <span>professor Hans van Goudoever
                                    </span>
                                </p> 
                                <p class="blue miniHour">16<sup>00</sup> - 16<sup>20</sup></p>
                                <p class="dark3">
                                    <span>Discussion
                                    </span>
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">16<sup>20</sup> - 16<sup>40</sup></p>
                                <p class="dark3">Coffee break<br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">16<sup>40</sup> - 18<sup>00</sup></p>
                                <p class="dark3">Session 3<br/> 
                                    <span>Controversies around maintaining of glucose homeostasis in neonate
                                    </span><br/> 
                                    <span>Chairman: professor Maria Katarzyna Borszewska-Kornacka
                                    </span>
                                </p> 
                                <p class="blue miniHour">16<sup>40</sup> - 17<sup>10</sup></p>
                                <p class="dark3">
                                    <span>Hyperglycaemia in NICU: physiology or pathology <br/> Dr. Kathryn Beardsall
                                    </span>
                                </p> 
                                <p class="blue miniHour">17<sup>10</sup> - 17<sup>40</sup></p>
                                <p class="dark3">
                                    <span>Hyperglycemia in neonate – should we treat it?
                                    </span><br>
                                    <span>Dr. Mateusz Jagła
                                    </span>
                                </p> 
                                <p class="blue miniHour">17<sup>40</sup> - 18<sup>00</sup></p>
                                <p class="dark3">
                                    <span>Discussion
                                    </span>
                                </p> 
                            </div><!--/.schedule-->
							 <div class="schedule  container">
                                <p class="blue  hour"></p>
                                <p class="blue"><h3>January 31, 2015</h3></span></p> 
                            </div><!--/.schedule-->  
                            
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">9<sup>00</sup> - 10<sup>20</sup></p>
                                <p class="dark3">Session 4<br/> 
                                    <span>Controversies in neonatal pain management.
                                    </span><br/> 
                                    <span>Chairman: professor Ryszard Lauterbach
                                    </span>
                                </p> 
                                <p class="blue miniHour">9<sup>00</sup> - 9<sup>30</sup></p>
                                <p class="dark3">
                                    <span>Long-term impact of neonatal pain
                                    </span><br>
                                    <span>Dr. Suellen Walker
                                    </span>
                                </p> 
                                <p class="blue miniHour">9<sup>30</sup> - 10<sup>00</sup></p>
                                <p class="dark3">
                                    <span>The Balance Between Adequate Pain Management
and the Risk of Adverse Effects in the Neonate <br/> professor Tracy Harrison
                                    </span>
                                </p> 
                                <p class="blue miniHour">10<sup>00</sup> - 10<sup>20</sup></p>
                                <p class="dark3">
                                    <span>Discussion
                                    </span><br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">10<sup>20</sup> - 10<sup>35</sup></p>
                                <p class="dark3">Coffee break<br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                             <div class="schedule  container">
                                <p class="blue miniHour">10<sup>35</sup> - 12<sup>55</sup></p>
                                <p class="dark3">Session 5<br/> 
                                    <span>Birth defects
                                    </span><br/> 
                                    <span>Chairman: professor Jacek J. Pietrzyk
                                    </span>
                                </p> 
                                <p class="blue miniHour">10<sup>35</sup> - 11<sup>05</sup></p>
                                <p class="dark3">
                                    <span>Why birth defects still represent a population burden <br/>professor Anna Latos-Bieleńska
                                    </span>
                                </p> 
								
								 <p class="blue miniHour">11<sup>05</sup> - 11<sup>35</sup></p>
                                <p class="dark3">
                                    <span>Neonatal and long-term follow-up of children born after PGD/PGS <br/>professor Maryse Bonduelle
                                    </span>
                                </p> 
                                <p class="blue miniHour">11<sup>35</sup> - 12<sup>05</sup></p>
                                <p class="dark3">
                                    <span>What is the risk of birth defects following assisted reproduction – facts and myths
                                    </span><br>
                                    <span>professor Joe Leigh Simpson
                                    </span>
                                </p> 
                                <p class="blue miniHour">12<sup>05</sup> - 12<sup>35</sup></p>
                                <p class="dark3">
                                    <span>Outcome of ART babies - medical, ethical and economical aspects.
                                    </span><br>
                                    <span>professor Janusz Gadzinowski
                                    </span><br>
                                    <span>professor Allen Merritt
                                    </span>
                                </p> 
                                <p class="blue miniHour">12<sup>35</sup> - 12<sup>55</sup></p>
                                <p class="dark3">
                                    <span>Discussion
                                    </span>
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">12<sup>55</sup> - 13<sup>55</sup></p>
                                <p class="dark3">Lunch<br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">13<sup>55</sup> - 15<sup>15</sup></p>
                                <p class="dark3">Session 6<br/> 
                                    <span>Effective neuroprotection a reality or still elusive goal?
                                    </span><br/> 
                                    <span>Chairman: Dr. Piotr Kruczek
                                    </span>
                                </p> 
                                <p class="blue miniHour">13<sup>55</sup> - 14<sup>25</sup></p>
                                <p class="dark3">
                                    <span>Neuroprotective Hypothermia: A Cool Idea <br/> Dr. Steven Donn
                                    </span>
                                </p> 
                                <p class="blue miniHour">14<sup>25</sup> - 14<sup>55</sup></p>
                                <p class="dark3">
                                    <span>Mild hypothermia and new therapeutic perspectives
												in hypoxic- ischemic encephalopathy of newborn <br/>professor Ewa Gulczyńska
                                    </span>
                                </p> 
                                <p class="blue miniHour">14<sup>55</sup> - 15<sup>15</sup></p>
                                <p class="dark3">
                                    <span>Discussion
                                    </span>
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">15<sup>15</sup> - 15<sup>55</sup></p>
                                <p class="dark3">Session 7<br/> 
                                    <span>Delivery room handling
                                    </span>
                                </p> 
                                <p class="blue miniHour">15<sup>15</sup> - 15<sup>45</sup></p>
                                <p class="dark3">
                                    <span>Delivery room handling <br/> professor Ola Didrik Saugstad
                                    </span>
                                </p> 
                                <p class="blue miniHour">15<sup>45</sup> - 15<sup>55</sup></p>
                                <p class="dark3">
                                    <span>Discussion
                                    </span>
                                </p>
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue hour">15<sup>55</sup></p>
                                <p class="dark3">Closing Conference<br/> 
                                </p> 
                            </div><!--/.schedule-->
                        </div>
                        
                        <div class="col-lg-6  col-md-6  col-sm-12">
                           
						   
						   
						     <div class="schedule  container">
                                <p class="blue  miniHour"></p>
                                <p class="blue"><h3>30 stycznia 2015</h3></span></p> 
                            </div><!--/.schedule-->  
                            
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>30</sup> </p>
                                <p class="dark3">Otwarcie konferencji <br/> </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">13<sup>30</sup> - 14<sup>50</sup></p>
                                <p class="dark3">Sesja  1<br/> 
                                    <span>Ciśnieniowa i objętościowa wentylacja mechaniczna – która jest lepsza?
                                    </span><br/> 
                                    <span>Przewodniczący: prof. Andrzej Piotrowski
                                    </span><br/> 
                                </p> 
                                <p class="blue miniHour">13<sup>30</sup> - 14<sup>00</sup></p>
                                <p class="dark3">
                                    <span>
									Obserwacja ciśnienia lub objętości podczas opieki nad noworodkiem to coś		więcej niż wstrząśnięte, niezmieszane Martini <br/>professor Jan Mazela
                                    </span>
                                </p> 
                                <p class="blue miniHour">14<sup>00</sup> - 14<sup>30</sup></p>
                                <p class="dark3">
                                    <span>Wentylacja noworodków: wybór wentylacji ciśnieniowej lub objętościowej to		coś więcej niż dylemat pomiędzy Coca-Colą a Pepsi <br/>Dr. Steven Donn
                                    </span>
                                </p> 
                                <p class="blue miniHour">14<sup>30</sup> - 14<sup>50</sup></p>
                                <p class="dark3">
                                    <span>Dyskusja
                                    </span><br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">14<sup>50</sup> - 15<sup>00</sup></p>
                                <p class="dark3">Przerwa na kawę<br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                             <div class="schedule  container">
                                <p class="blue miniHour">15<sup>00</sup> - 16<sup>20</sup></p>
                                <p class="dark3">Session 2<br/> 
                                    <span>Czy wiemy, jak karmić noworodki niedonoszone / ze zbyt niską masą urodzeniową względem wieku płodowego po wypisaniu z OITN?
                                    </span><br/> 
                                    <span>Przewodniczący: prof. Przemko Kwinta
                                    </span>
                                </p> 
                                <p class="blue miniHour">15<sup>00</sup> - 15<sup>30</sup></p>
                                <p class="dark3">
                                    <span>Karmienie uzupełniające noworodka niedonoszonego po wypisie. 
                                    </span><br>
                                    <span>Co, kiedy i		jak?
                                    </span><br>
                                    <span>prof. Hanna Szajewska
                                    </span>
                                </p> 
                                <p class="blue miniHour">15<sup>30</sup> - 16<sup>00</sup></p>
                                <p class="dark3">
                                    <span>Początkowa gospodarka żywieniowa niedonoszonego noworodka
                                    </span><br>
                                    <span>prof. Hans von Goudoever
                                    </span>
                                </p> 
                                <p class="blue miniHour">16<sup>00</sup> - 16<sup>20</sup></p>
                                <p class="dark3">
                                    <span>Dyskusja
                                    </span>
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">16<sup>20</sup> - 16<sup>40</sup></p>
                                <p class="dark3">Przerwa na kawę<br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">16<sup>40</sup> - 18<sup>00</sup></p>
                                <p class="dark3">Sesja 3<br/> 
                                    <span>Kontrowersje wokół utrzymywania homeostazy glukozy u noworodka
                                    </span><br/> 
                                    <span>Przewodniczący: prof. Maria Katarzyna Borszewska-Kornacka
                                    </span>
                                </p> 
                                <p class="blue miniHour">16<sup>40</sup> - 17<sup>10</sup></p>
                                <p class="dark3">
                                    <span>Hiperglikemia na OITN: fizjologia czy patologia <br/> Dr. Kathryn Beardsall
                                    </span>
                                </p> 
                                <p class="blue miniHour">17<sup>10</sup> - 17<sup>40</sup></p>
                                <p class="dark3">
                                    <span>Hiperglikemia u noworodka – czy powinniśmy leczyć?
                                    </span><br>
                                    <span>Dr. Mateusz Jagła
                                    </span>
                                </p> 
                                <p class="blue miniHour">17<sup>40</sup> - 18<sup>00</sup></p>
                                <p class="dark3">
                                    <span>Dyskusja
                                    </span>
                                </p> 
                            </div><!--/.schedule-->
							 <div class="schedule  container">
                                <p class="blue  hour"></p>
                                <p class="blue"><h3>31 stycznia 2015</h3></span></p> 
                            </div><!--/.schedule-->  
                            
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">9<sup>00</sup> - 10<sup>20</sup></p>
                                <p class="dark3">Session 4<br/> 
                                    <span>Kontrowersje w leczeniu bólu u noworodka
                                    </span><br/> 
                                    <span>Przewodniczący: prof. Ryszard Lauterbach
                                    </span>
                                </p> 
                                <p class="blue miniHour">9<sup>00</sup> - 9<sup>30</sup></p>
                                <p class="dark3">
                                    <span>Długoterminowe działanie bólu u noworodka
                                    </span><br>
                                    <span>Dr. Suellen Walker
                                    </span>
                                </p> 
                                <p class="blue miniHour">9<sup>30</sup> - 10<sup>00</sup></p>
                                <p class="dark3">
                                    <span>Równowaga pomiędzy dostatecznym leczeniem bólu u noworodka a ryzykiem		wywołania skutków ubocznych <br/> prof. Tracy Harrison
                                    </span>
                                </p> 
                                <p class="blue miniHour">10<sup>00</sup> - 10<sup>20</sup></p>
                                <p class="dark3">
                                    <span>Dyskusja
                                    </span><br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">10<sup>20</sup> - 10<sup>35</sup></p>
                                <p class="dark3">Przerwa na kawę<br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                             <div class="schedule  container">
                                <p class="blue miniHour">10<sup>35</sup> - 12<sup>55</sup></p>
                                <p class="dark3">Sesja 5<br/> 
                                    <span>Uszkodzenia płodu
                                    </span><br/> 
                                    <span>Przewodniczący: prof. Jacek J. Pietrzyk
                                    </span>
                                </p> 
                                <p class="blue miniHour">10<sup>35</sup> - 11<sup>05</sup></p>
                                <p class="dark3">
                                    <span>Dlaczego uszkodzenia płodu nadal ciążą na naszym społeczeństwie? <br/>prof. Anna Latos-Bieleńska
                                    </span>
                                </p> 
								
								 <p class="blue miniHour">11<sup>05</sup> - 11<sup>35</sup></p>
                                <p class="dark3">
                                    <span>Neonatologiczna i długoterminowa obserwacja dzieci urodzonych po			diagnostyce PGD/PGS<br/>prof. Maryse Bonduelle
                                    </span>
                                </p> 
                                <p class="blue miniHour">11<sup>35</sup> - 12<sup>05</sup></p>
                                <p class="dark3">
                                    <span>Ryzyko uszkodzenia płodu w przypadku wspomaganego rozrodu – fakty i mity
                                    </span><br>
                                    <span>prof. Joe Leigh Simpson
                                    </span>
                                </p> 
                                <p class="blue miniHour">12<sup>05</sup> - 12<sup>35</sup></p>
                                <p class="dark3">
                                    <span>Dzieci urodzone w wyniku wspomaganego rozrodu – aspekty medyczne,			etyczne oraz ekonomiczne
                                    </span><br>
                                    <span>prof. Janusz Gadzinowski
                                    </span><br>
                                    <span>prof.  Allen Merritt
                                    </span>
                                </p> 
                                <p class="blue miniHour">12<sup>35</sup> - 12<sup>55</sup></p>
                                <p class="dark3">
                                    <span>Dyskusja
                                    </span>
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">12<sup>55</sup> - 13<sup>55</sup></p>
                                <p class="dark3">Przerwa na lunch<br/> 
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">13<sup>55</sup> - 15<sup>15</sup></p>
                                <p class="dark3">Sesja 6<br/> 
                                    <span>Skuteczna neuroprotekcja – rzeczywistość czy nadal nieuchwytny cel?
                                    </span><br/> 
                                    <span>Przewodniczący: dr Piotr Kruczek
                                    </span>
                                </p> 
                                <p class="blue miniHour">13<sup>55</sup> - 14<sup>25</sup></p>
                                <p class="dark3">
                                    <span>Neuroprotekcyjne działanie hipotermii<br/> Dr. Steven Donn
                                    </span>
                                </p> 
                                <p class="blue miniHour">14<sup>25</sup> - 14<sup>55</sup></p>
                                <p class="dark3">
                                    <span>Hipotermia umiarkowana i nowe perspektywy terapeutyczne encefalopatii		niedotlenieniowo-niedokrwiennej u noworodka<br/>prof. Ewa Gulczyńska
                                    </span>
                                </p> 
                                <p class="blue miniHour">14<sup>55</sup> - 15<sup>15</sup></p>
                                <p class="dark3">
                                    <span>Dyskusja
                                    </span>
                                </p> 
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue miniHour">15<sup>15</sup> - 15<sup>55</sup></p>
                                <p class="dark3">Sesja 7<br/> 
                                    <span>Zarządzanie salą porodową
                                    </span>
                                </p> 
                                <p class="blue miniHour">15<sup>15</sup> - 15<sup>45</sup></p>
                                <p class="dark3">
                                    <span>Zarządzanie salą porodową <br/> prof. Ola Didrik Saugstad
                                    </span>
                                </p> 
                                <p class="blue miniHour">15<sup>45</sup> - 15<sup>55</sup></p>
                                <p class="dark3">
                                    <span>Dyskusja
                                    </span>
                                </p>
                            </div><!--/.schedule-->
                            
                            <div class="schedule  container">
                                <p class="blue hour">15<sup>55</sup></p>
                                <p class="dark3">Zamknięcie konferencji<br/> 
                                </p> 
                            </div><!--/.schedule-->
                        </div>  
						   
						   
						   
						   
						   
						   
						   
						   
						   
						   
						   
						   
						   
                        </div>
<!--                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">9<sup>00</sup></p>
                                <p class="dark">Inauguration<br/> </p> 
                            </div>/.schedule
                            <div class="schedule  container">
                                <p class="blue  hour">9<sup>15</sup></p>
                                <p class="dark">Restricted or liberal approach to the treatment with blood and blood products. Are growth factors good alternative?<br/> 
                                Chairman: Professor Barbara Królak-Olejnik</p> 
                            </div>/.schedule
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>35</sup></p>
                                <p class="dark">Coffee break<br/> </p> 
                            </div>/.schedule
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>50</sup></p>
                                <p class="dark">Seizures in the newborns - when to treat, when to observe?<br/> 
                                Chairman: Professor Sergiusz Jóźwiak</p> 
                            </div>/.schedule
                            <div class="schedule  container">
                                <p class="blue  hour">12<sup>10</sup></p>
                                <p class="dark">Lunch<br/> </p> 
                            </div>/.schedule
                        </div>/.col
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>10</sup></p>
                                <p class="dark">Shock in the newborn - is the current algorithm for diagnosis and treatment still valid?<br/> 
                                Chairman: Professor Przemko Kwinta</p> 
                            </div>/.schedule
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>30</sup></p>
                                <p class="dark">Coffee break<br/> </p> 
                            </div>/.schedule
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>45</sup></p>
                                <p class="dark">Prophylactic, empiric and prolonged administration of antibiotics and antimycotics in the NICU, do the benefits outweigh the harm?<br/> 
                                Chairman: Professor Ryszard Lauterbach</p> 
                            </div>/.schedule
                            <div class="schedule  container">
                                <p class="blue  hour">16<sup>05</sup></p>
                                <p class="dark">Close of the Conference<br/> </p> 
                            </div>/.schedule
                        </div>/.col-->
                    </div><!--/.row-->
                </div>
              </div>

            </div>
  <section class="greetings  container">
        <div class="wrapper  relative">
            <div class="row">
                <div class="col-lg-12  col-md-12  col-sm-12">
                    <h2 class="blue">Spotkanie towarzyszące</h2>
					<h2 class="blue" style="font-size: 28px; margin-bottom:2px;">Recital Natalii Niemen</h2>
					<h2 class="blue" style="font-size: 28px; margin-bottom:20px;">Niemen śpiewa Niemena</h2>
                    <p class="grey just"><b>Data:</b> 30 stycznia 2015 roku, godz. 18.40</p>
                    <p class="grey just"><b>Miejsce:</b> Auditorium Maximum Uniwersytetu Jagiellońskiego, Aula Duża A</p>
                     <p class="grey just"><i>Wstęp na podstawie zaproszeń, które otrzymają Państwo w Biurze Konferencji przy rejestracji</i></p>
                
				</div><!--/.col-->
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-8  col-md-8  col-sm-12">
                    <p class="grey just">
                        <br><b>Niemen śpiewa Niemena</b> to wyjątkowy koncert podczas którego, oprócz swoich autorskich kompozycji, Natalia zaprezentuje utwory swojego Taty. Usłyszymy kompozycje m.in. z dwóch ostatnich albumów Czesława Niemena:
						<i>Terra Deflorata</i> oraz <i>Spod chmury kapelusza</i>. Podczas koncertu nie zabraknie też największych przebojów Czesława Niemena. takich jak <i>Dziwny jest ten świat </i> czy <i>Jednego serca </i>.
						<br><br>Natalia Niemen - wokal
						<br>Paweł Bzim Zarecki - piano
                        <br><br><b>Natalia Niemen</b> to piosenkarka, autorka muzyki i tekstów. Z muzyką związana jest od najmłodszych lat. Jest absolwentką warszawskiej szkoły muzycznej II stopnia im Karola Szymanowskiego w klasie altówki.
						Od ponad dekady współpracuje z zespołami New life'm i TGD. Zarówno w tworzeniu muzyki, jak i w wyrazie wokalnym, artystka inspiruje się starym rhytm'n'bluesem, rock'n'rollem, ekspresją pieśniarzy flamenco, a także 
						chorałem gregoriańskim i muzyką cerkiewną. Osobowość i głos artystki należą do jednych z najbardziej charakterystycznych i nietuzinkowych na dzisiejszej polskiej scenie muzycznej.
						<br><br><b>Paweł Bzim Zarecki</b> to pianista, aranżer oraz producent muzyczny od lat współpracujący z artystami w Polsce i zagranicą, m.in. z Anną Marią Jopek, Mietkiem Szcześniakiem, TGD.
                    </p>
                    
                </div><!--/.col-->
                <div class="col-lg-8  col-md-8  col-sm-12">
                </div><!--/.col-->

                <div class="person_big absolute" style='bottom: 0px; margin-bottom: 146px;'>
                    <img src="{{ url('conference/img/niemenowa.jpg') }}" class="photo" style="width:360px;"/>
                    
                                        
                </div>
            </div><!--/.row-->
        </div><!--/.wrapper-->
    </section><!--/.greetings-->
            <div class="button-container">
                <a href="{{ url('conference/Kontrowersje2015_komunikat_20141219vAKCEPT2.pdf') }}" target="_blank" class="button">pobierz szczegółowy program konferencji</a>
                <p class="grey">Informujemy Państwa, iż ostateczny program konferencji może ulec zmianie z przyczyn od nas niezależnych</p>
            </div>
        </div><!--/.wrapper-->
    </section><!--/.programme-->
    
    <section class="price  container">
        <div class="wrapper">
            <h3 class="white">Preferencyjne ceny rejestracji jeszcze przez:</h3>
            <div id="clock2"></div>
            <div class="row">
                <div class="col-lg-3  col-md-6  col-sm-12  price-column">
                    <p class="blue">VIII Ogólnopolska Konferencja Kontrowersje w Pediatrii</p>
                    <p class="blue" style="margin-bottom: -34px;">cena do 29.01</p>
                    <h2 class="white">450 zł</h2>
                    <p class="blue">550 zł w dniu konferencji</p>
                </div><!--/.col-->
                <div class="col-lg-3  col-md-6  col-sm-12  price-column">
                    <p class="blue">3rd Controversies in Neonatology<br>&nbsp</p>
                    <p class="blue" style="margin-bottom: -34px;">cena do 29.01</p>
                    <h2 class="white">200 zł</h2>
                    <p class="blue">300 zł w dniu konferencji</p>
                </div><!--/.col-->
                <div class="col-lg-3  col-md-6  col-sm-12  price-column">
                    <p class="blue">Osoba<br/> towarzysząca</p>
                    <h2 class="white">200 zł</h2>
                </div><!--/.col-->
                <div class="col-lg-3  col-md-6  col-sm-12  price-column">
                    <p class="blue">Koncert<br/>&nbsp</p>
                    <h2 class="white">25 zł</h2>
                </div><!--/.col-->
            </div><!--/.row-->
            <div class="button-container" style="margin-top: 35px;">
                <a href="" class="button" data-toggle="modal" data-target="#loginModal">zarejestruj się</a>
            </div>
        </div><!--/.wrapper-->
    </section><!--/.price-->

    <section class="map  container  relative">
        <div class="map-navigation"><!--część ukryta na mapie-->
            <div class="map-nav  container">
                <h3 class="blue">Lokalizacja</h3>
                <p class="grey">Auditorium Maximum Uniwersytetu Jagiellońskiego<br/>Kraków, Krupnicza 33</p>
                <form>
                    <input type="text" placeholder="wpisz, skąd chcesz dojechać" required>
                    <input type="submit" value="wyznacz trasę">
                </form>
                <div class="button-container">
                    <a href="" class="button">wyświetl lokalizacje hoteli</a>
                </div>
            </div>
            <div class="map-nav  container">
                <h3 class="blue">Parking</h3>
                <p class="grey">Uwaga, Auditorium Maximum nie posiada własnego parkingu!</p>
                <div class="button-container">
                    <a href="" class="button">wyświetl lokalizacje parkingów</a>
                </div>
            </div>
        </div><!--/.map-navigation-->
        <div id="map-canvas"></div>
    </section><!--/.map-->

    <section class="sponsors  container">
        <div class="wrapper">
            <h2 class="blue  capital">Głowny Partner Konferencji </h2>
            <div class="row">
                <div class="col-lg-12  col-md-12  col-sm-12">
                    <center><img style="width:100%" src="{{ url('conference/img/kontrowersje/GłownyPartnerKonferencji.jpg') }}" /></center>
                </div>
            </div><!--/.row-->
            <h2 class="blue  capital">Partnerzy Konferencji</h2>
            <div class="row">
                <div class="col-lg-12  col-md-12  col-sm-12">
                    <center><img style="width:100%" src="{{ url('conference/img/kontrowersje/PartnerzyKonferencji.jpg') }}" /></center>
                </div>
                <div class="col-lg-12  col-md-12  col-sm-12 text-center blue">
                    BAYER
                    <br>BIOMED-LUBLIN
                    <br>DUNA
                    <br>HF SOLINEA
                    <br>KREWEL MEUSELBACH
                    <br>NESTLE
                    <br>NOVASCON
                    <br>QPHARMA
                    <br>RECKITT BENCKISER
                    <br>SALVEO
                    <br>SENSILAB
                    <br>SEQUOIA
                    <br>TYMOFARM
                    <br>VITAMED
                </div>
            </div><!--/.row-->
            <h2 class="blue  capital">Partnerzy Medialni Konferencji</h2>
            <div class="row">
                <div class="col-lg-12  col-md-12  col-sm-12">
                    <center><img style="width:100%" src="{{ url('conference/img/kontrowersje/PartnerzyMedialniKonferencji.jpg') }}" /></center>
					<center><img style="width:37%; margin-bottom: -77px;" src="{{ url('conference/img/kontrowersje/helpmed.jpg') }}" /></center>
                </div>
            </div><!--/.row-->
            
        </div><!--/.wrapper-->
    </section>

@include('conference.kontakt')

    <!-- JavaScript -->
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery/core/jquery-1.11.1.min.js') }}"></script>
    <!-- Jquery plugins:-->
    <script src="{{ url('conference/js/plugins/countdown.js') }}"></script>
    
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery-ui/core/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/transition.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/alert.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/button.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/carousel.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/collapse.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/dropdown.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/modal.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/scrollspy.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tab.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tooltip.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/popover.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/affix.js') }}"></script>
    <script src="{{ url('conference/js/project/project.js') }}"></script>
    <script src="{{ url('conference/js/project/script.js') }}"></script>
    
    @include('conference.popover')
</body>

</html>
