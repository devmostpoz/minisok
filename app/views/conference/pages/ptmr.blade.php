<!DOCTYPE html>
<!-- Change to your language -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title }}</title>

    <!-- Core Bootstrap CSS -->
    <link href="{{ url('conference/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('conference/bootstrap.mini.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    
</head>

<body>
    <header class="header  container  relative  ptp  szkola">
        <img src="{{ url('conference/img/szkolaptmr.jpg') }}">     
    </header>

    <section class="greetings  container  szkola">
        <div class="wrapper  relative">
            <div class="row">
                <div class="col-lg-9  col-md-9  col-sm-12">
                    <h2 class="blue">Szanowni Państwo,</h2>
                    <p class="grey">Szanowne Koleżanki i Koledzy, Drodzy Przyjaciele,</p>
                    <p class="grey">Lorem ipsum dolor sit amet consectetuer tristique diam Curabitur enim massa. Felis gravida et wisi id mattis Vestibulum egestas tellus rhoncus risus. Ultrices mollis libero rutrum elit nunc sociis laoreet semper egestas orci. Hendrerit consectetuer tincidunt aliquet lacus ac nibh semper at eget dapibus. Sit id elit fames orci dui massa ac quis ut Nam. Non ligula habitasse velit dolor libero aliquam pretium sed adipiscing urna.</p>
                </div><!--/.col-->
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-9  col-md-9  col-sm-12">
                    <p class="grey">Lorem ipsum dolor sit amet consectetuer tristique diam Curabitur enim massa. Felis gravida et wisi id mattis Vestibulum egestas tellus rhoncus risus. Ultrices mollis libero rutrum elit nunc sociis laoreet semper egestas orci. Hendrerit consectetuer tincidunt aliquet lacus ac nibh semper at eget dapibus. Sit id elit fames orci dui massa ac quis ut Nam. Non ligula habitasse velit dolor libero aliquam pretium sed adipiscing urna.</p>
                    <p class="grey">Lorem ipsum dolor sit amet consectetuer tristique diam Curabitur enim massa. Felis gravida et wisi id mattis Vestibulum egestas tellus rhoncus risus. Ultrices mollis libero rutrum elit nunc sociis laoreet semper egestas orci. Hendrerit consectetuer tincidunt aliquet lacus ac nibh semper at eget dapibus. Sit id elit fames orci dui massa ac quis ut Nam. Non ligula habitasse velit dolor libero aliquam pretium sed adipiscing urna.</p>                    
                </div><!--/.col-->
                <div class="col-lg-9  col-md-9  col-sm-12">
                </div><!--/.col-->

                <div class="person_big  absolute">
                    <img src="{{ url('conference/img/nophoto.jpg') }}" class="photo">
                    <p>prof. dr hab. n. med. Lorem Ipsum</p>
                    <p class="dark">Lorem Ipsum Dolor Sit Amet<br />Consectetuer Tristique Diam</p>                    
                </div>
            </div><!--/.row-->
        </div><!--/.wrapper-->
    </section><!--/.greetings-->

    <section class="commitee container ptmr" style="background:#c14776">
        <div class="wrapper">
            <h2 class="white  capital">PATRONAT NAUKOWY</h2>
            <ul class="white">
                <li><strong>dr hab. Jarosław Drobnik</strong> - Prezes Zarządu Głównego PTMR</li>
                <li><strong>dr Agnieszka Mastalerz-Migas</strong> - Wiceprezes Zarządu Głównego PTMR</li>
            </ul>
            <h2 class="white  capital">TEMATY WIODĄCE</h2>
            <ul class="white">
                <li>Zakażenia układu oddechowego</li>
                <li>Interakcje leków w pediatrii</li>
                <li>Biegunka i czynnościowe zaparcie stolca u dzieci</li>
                <li>Astma oskrzelowa u dzieci, różnicowanie astmy i infekcji</li>
                <li>Szczepienia ochronne</li>
                <li>Standardy postępowania terapeutycznego w gorączce i bólu u dzieci ze szczególnym uwzględnieniem bólów głowy</li>
                <li>...oraz inne ciekawe tematy</li>
            </ul>            
            <h2 class="white  capital">PUNKTY EDUKACYJNE</h2>
            <ul class="white">
                <li>Lekarz uczestniczący w sympozjum uzyskuje punkty edukacyjne zgodnie z Rozporządzeniem Ministra Zdrowia w sprawie sposobów dopełnienia obowiązków doskonalenia zawodowego lekarzy i lekarzy dentystów z dnia 06.10.2004r. zał. nr 3. <a href="http://static1.money.pl/d/akty_prawne/pdf/DU/2004/231/DU20042312326.pdf" target="_blank">Pełen tekst rozporządzenia</a></li>
            </ul>
            <h2 class="white  capital">CERTYFIKATY</h2>
            <ul class="white">
                <li>Warunkiem otrzymania <strong>certyfikatu</strong> potwierdzającego uzyskanie punktów edukacyjnych jest Państwa obecność na sympozjum oraz rejestracja przy stoisku firmy SYMPOSION. Certyfikat otrzymują Państwo na zakończenie sympozjum.</li>
            </ul>
            <h2 class="white  capital">ANKIETY</h2>
            <ul class="white">
                <li>Uprzejmie prosimy o wypełnianie <strong>ankiet</strong> wręczanych Państwu przy rejestracji. Dokładnie wypełnione ankiety umożliwiają nam poznanie Państwa indywidualnych preferencji co do tematyki sympozjów. Informacje te posłużą nam do celowanego informowania o spotkaniach będących w zakresie Państwa zainteresowań.</li>
            </ul>
            <h2 class="white  capital">LOSOWANIE NAGRÓD-NIESPODZIANEK</h2>
            <ul class="white">
                <li>Na zakończenie sympozjum wśród osób, które wypełniły ankietę i znajdują się na sali obrad, losujemy atrakcyjne <strong>nagrody-niespodzianki</strong>.</li>
            </ul>
        </div><!--/.wrapper-->
    </section><!--/.comitee-->

    <section class="programme  container">
        <div class="wrapper">
            <h2 class="blue  capital">program</h2>

            <div role="tabpanel">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#day1" aria-controls="day1" role="tab" data-toggle="tab">
                        Dzień I <br/><span>31.01.2015</span>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#day2" aria-controls="day2" role="tab" data-toggle="tab">
                        Dzień II <br/><span>1.02.2015</span>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">
                        Controversies in Neonatology Programme
                    </a>
                </li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="day1">
                    <h3 class="blue">Aula Duża A</h3>
                    <div class="row">
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>00</sup></p>
                                <p class="dark">Rozpoczęcie konferencji</p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>15</sup></p>
                                <p class="dark">Sesja 1 <br/> 
                                <span>Choroba Kawasaki-postać poronna i atypowa-kontrowersje w leczeniu + przypadki </span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">11<sup>15</sup></p>
                                <p class="dark">Sesja 2 <br/> 
                                <span>Zaburzenia wzrastania jako problem wielodyscyplinarny. Kontrowersje wokół leczenia hormonem wzrostu. </span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">12<sup>15</sup></p>
                                <p class="dark">Przerwa na lunch</p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>15</sup></p>
                                <p class="dark">Sesja 3 <br/> 
                                <span>Bóle stawów- pediatra, reumatolog czy ortopeda?</span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>00</sup></p>
                                <p class="dark">Przerwa na obiad</p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>15</sup></p>
                                <p class="dark">Sesja 4 <br/> 
                                <span>Bezpieczeństwo w nebulizacji – panel pytań i odpowiedzi<br/>
                                Patronat: Teva 
                                </span></p> 
                            </div><!--/.schedule-->
                        </div><!--/.col-->
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">15<sup>15</sup></p>
                                <p class="dark">Przerwa na kawę</p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">15<sup>30</sup></p>
                                <p class="dark">Sesja firmowa <br/> 
                                <span>Kontrowersje w alergii pokarmowej<br/>
                                Patronat: Mead Johnson Nutrition
                                </span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">16<sup>30</sup></p>
                                <p class="dark">Sesja firmowa <br/> 
                                <span>Profilaktyka zakażeń pneumokokowych – panel ekspertów<br/>
                                Patronat: Pfizer
                                </span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">17<sup>30</sup></p>
                                <p class="dark">Przerwa na kawę</p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">17<sup>45</sup></p>
                                <p class="dark">Sesja firmowa <br/> 
                                <span>Kontrowersje w alergologii dziecięcej<br/>- Astma wczesnodziecięca<br/>- Zespół krupu<br/>
                                Patronat: Astra Zeneca
                                </span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">19<sup>00</sup></p>
                                <p class="dark">Bankiet - wieczór artystyczny</p> 
                            </div><!--/.schedule-->
                        </div><!--/.col-->
                    </div><!--/.row-->
                </div>

                <div role="tabpanel" class="tab-pane" id="day2">
                    <h3 class="blue">Aula Duża A</h3>
                    <div class="row">
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">9<sup>30</sup></p>
                                <p class="dark">Sesja 5 <br/> 
                                <span>Autyzm i spektrum autyzmu-trudności diagnostyczne, możliwości terapii 
                                </span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">12<sup>00</sup></p>
                                <p class="dark">Sesja 6 <br/> 
                                <span>Czy możemy pomóc dziecku z nawracającymi  infekcjami dróg oddechowych?
                                </span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>25</sup></p>
                                <p class="dark">Przerwa na lunch <br/> </p> 
                            </div><!--/.schedule-->
                        </div><!--/.col-->
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>45</sup></p>
                                <p class="dark">Sesja 7<br/> 
                                <span>Sesja satelitarna 
                                </span></p>
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">15<sup>45</sup></p>
                                <p class="dark">Przerwa na kawę <br/> </p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">16<sup>00</sup></p>
                                <p class="dark">Sesja 8<br/> 
                                <span>Enigmatyczny obraz zmian skórnych w chorobach zakaźnych 
                                </span></p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">17<sup>05</sup></p>
                                <p class="dark">Zakończenie konferencji<br/> </p> 
                            </div><!--/.schedule-->
                        </div><!--/.col-->
                    </div><!--/.row-->
                </div>

                <div role="tabpanel" class="tab-pane" id="eng">
                    <h3 class="blue">Saturday 1st February 2014 – Aula Średnia</h3>
                    <div class="row">
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">9<sup>00</sup></p>
                                <p class="dark">Inauguration<br/> </p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">9<sup>15</sup></p>
                                <p class="dark">Restricted or liberal approach to the treatment with blood and blood products. Are growth factors good alternative?<br/> 
                                Chairman: Professor Barbara Królak-Olejnik</p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>35</sup></p>
                                <p class="dark">Coffee break<br/> </p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>50</sup></p>
                                <p class="dark">Seizures in the newborns - when to treat, when to observe?<br/> 
                                Chairman: Professor Sergiusz Jóźwiak</p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">12<sup>10</sup></p>
                                <p class="dark">Lunch<br/> </p> 
                            </div><!--/.schedule-->
                        </div><!--/.col-->
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>10</sup></p>
                                <p class="dark">Shock in the newborn - is the current algorithm for diagnosis and treatment still valid?<br/> 
                                Chairman: Professor Przemko Kwinta</p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>30</sup></p>
                                <p class="dark">Coffee break<br/> </p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>45</sup></p>
                                <p class="dark">Prophylactic, empiric and prolonged administration of antibiotics and antimycotics in the NICU, do the benefits outweigh the harm?<br/> 
                                Chairman: Professor Ryszard Lauterbach</p> 
                            </div><!--/.schedule-->
                            <div class="schedule  container">
                                <p class="blue  hour">16<sup>05</sup></p>
                                <p class="dark">Close of the Conference<br/> </p> 
                            </div><!--/.schedule-->
                        </div><!--/.col-->
                    </div><!--/.row-->
                </div>
              </div>

            </div>

            <div class="button-container">
                <a href="{{ url('conference/kalendarium.pdf') }}" target="_blank" class="button">pobierz szczegółowy program konferencji</a>
                <p class="grey">Informujemy Państwa, iż ostateczny program konferencji może ulec zmianie z przyczyn od nas niezależnych</p>
            </div>
        </div><!--/.wrapper-->
    </section><!--/.programme-->
    
    <section class="price container ptp2" style="background:#c14776">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-12  col-md-12  col-sm-12">
                    <h2 class="white">Udział w konferencji jest bezpłatny, jednak wymaga rejestracji.</h3> 
                </div><!--/.col-->
            </div><!--/.row-->
            <div class="button-container">
                <a href="" class="button" data-toggle="modal" data-target="#loginModal">zarejestruj się</a>
            </div>
        </div><!--/.wrapper-->
    </section><!--/.price-->

    <section class="map  container  relative  center">
        <div class="wrapper">
            <img src="{{ url('conference/img/mapa3.png') }}" class="mapka">
        </div><!--/.wrapper-->
    </section><!--/.map-->

    <section class="sponsors  container">
        <div class="wrapper">
            <h2 class="blue  capital">sponsorzy i patroni</h2>
            <div class="row">
              <center><img src="{{ url('conference/img/logo_terapia.jpg') }}" /></center>
            </div><!--/.row-->
        </div><!--/.wrapper-->
    </section>

@include('conference.kontakt')

    <!-- JavaScript -->
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery/core/jquery-1.11.1.min.js') }}"></script>
    <!-- Jquery plugins:-->
    <script src="{{ url('conference/js/plugins/countdown.js') }}"></script>
    
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery-ui/core/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/transition.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/alert.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/button.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/carousel.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/collapse.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/dropdown.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/modal.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/scrollspy.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tab.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tooltip.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/popover.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/affix.js') }}"></script>
    <script src="{{ url('conference/js/project/project.js') }}"></script>
    <script src="{{ url('conference/js/project/script.js') }}"></script>
    @include('conference.popover')
</body>

</html>
