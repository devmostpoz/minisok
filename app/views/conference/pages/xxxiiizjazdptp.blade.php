<!DOCTYPE html>
<!-- Change to your language -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title }}</title>

    <!-- Core Bootstrap CSS -->
    
    <link href="{{ url('conference/bootstrap.mini.css') }}" rel="stylesheet">
    <link href="{{ url('conference/css/style.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
        <script>
          function initialize() {
             var styles = [
            {
                  stylers: [
                    { saturation: -100 }
                  ]
                },
              ];
            var mapCanvas = document.getElementById('map-canvas');
            var myLatlng = new google.maps.LatLng(51.1069107, 17.077300);
            var mapOptions = {
              center: new google.maps.LatLng(51.1069107, 17.077300),
              zoom: 16,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              scrollwheel: false,
              draggable: false,
            }
            var styledMap = new google.maps.StyledMapType(styles,
            {name: "Styled Map"});
            var map = new google.maps.Map(mapCanvas, mapOptions)
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                });
            }
          google.maps.event.addDomListener(window, 'load', initialize);
        </script>
</head>

<body>
    
    <header class="header  container  relative  ptp">
        <img src="{{ url('conference/img/ptp_baner.jpg') }}">
        <div class="button-container">
            <div id="clock3"></div>
            <a href="" class="button" data-toggle="modal" data-target="#loginModal">zarejestruj się</a>
        </div>       
    </header>

    <section class="greetings  container">
        <div class="wrapper  relative">
            <div class="row">
                <div class="col-lg-12  col-md-12  col-sm-12">
                    <h2 class="blue">Szanowni Państwo,</h2>
                    <p class="grey">Drogie Koleżanki i Koledzy,</p>
                    <p class="grey just">W imieniu Komitetu Naukowego i Organizacyjnego mam wielki zaszczyt  i przyjemność powitać Państwa we Wrocławiu, który w dniach 17-19 września gości Państwa na XXXIII Ogólnopolskim Zjeździe Polskiego Towarzystwa Pediatrycznego. Stolica Dolnego Śląska już po raz trzeci jest gospodarzem tego najważniejszego naukowego wydarzenia, adresowanego nie tylko do pediatrów, ale również do lekarzy, dla których zdrowie dziecka jest dobrem najwyższym. Mam nadzieję, że Zjazd spełni Państwa oczekiwania pod każdym względem. Dołożyliśmy wszelkich starań, aby program naukowy obejmował najważniejsze  problemy i wyzwania współczesnej pediatrii przed jakimi stają lekarze w codziennej praktyce. W celu zacieśnienia lepszej współpracy, do udziału w Zjeździe  zaprosiliśmy lekarzy podstawowej opieki zdrowotnej, lekarzy innych specjalności oraz pielęgniarki, a także przedstawicieli  wszystkich  zawodów, którzy uczestniczą w sprawowaniu opieki nad chorym dzieckiem. Bogaty jest również program sesji satelitarnych, organizowanych przez firmy farmaceutyczne. Jako wykładowcy wystąpią wybitni naukowcy i praktycy, eksperci z  rożnych dziedzin  pediatrii. Zapewni to nie tylko możliwość wzbogacenia wiedzy ale także skonfrontowania własnych doświadczeń. Liczymy, że ożywiona dyskusja pozwoli wypracować stanowiska w problematycznych kwestiach, z korzyścią dla naszych pacjentów.</p>                    
                </div><!--/.col-->
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-8  col-md-8  col-sm-12">
                    <p class="grey just">Podczas Zjazdu pragniemy zapewnić Państwu chwile rozrywki i wytchnienia. Wierzymy, że zaplanowane imprezy towarzyskie będą okazją do wspólnej zabawy, odnowienia starych i zawarcia nowych przyjaźni. W wolnych chwilach zachęcamy do spacerów. Wrocław to wyjątkowe miasto, gdzie bogata historia przeplata się z nowoczesnością, i tętniącym życiem gospodarczym. To bogactwo wysp, mostów, przepięknych parków i wspaniałej atmosfery, która przyciąga nie tylko turystów ale również krasnale. Ukochały one nasze miasto i są jego ozdobą.  Mała wycieczka tropem tych przyjaznych stworzeń dostarczy z pewnością miłych wrażeń.</p>
                    <p class="grey just">Naszym i naszego partnera organizacyjnego firmy Symposion pragnieniem jest, aby wrześniowy pobyt we Wrocławiu był przez każdego uczestnika XXXIII Zjazdu  PTP  wspominany  jako uczta naukowa połączona z  odrobiną rozrywki i uśmiechu.</p>
                    <p class="grey just">Z życzeniami udanych obrad<br />Przewodnicząca Komitetu Naukowego i Organizacyjnego</p>
                </div><!--/.col-->
                <div class="col-lg-8  col-md-8  col-sm-12">
                </div><!--/.col-->

                <div class="person_big absolute">
                    <img src="{{ url('conference/img/profzwolinska.jpg') }}" class="photo" />
                    <p>prof. dr hab. Danuta Zwolińska Przewodnicząca</p>
                    <p class="dark">Komitetu Naukowego i Organizacyjnego</p>                    
                </div>
            </div><!--/.row-->
        </div><!--/.wrapper-->
    </section><!--/.greetings-->

    <section class="commitee  container ptp1">
        <div class="wrapper">
            <h2 class="white  capital">komitet naukowy</h2>
            <div class="row">
                <div class="col-lg-2  col-md-2  col-sm-2  person">
                    
                </div>
                <div class="col-lg-4  col-md-4  col-sm-12  person ">
                    <!--<img src="{{ url('conference/img/person_small.png') }}">-->
                    <p class="blue">prof. dr hab. n. med. Alicja Chybicka <br/>
                    </p>
                </div>
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <!--<img src="{{ url('conference/img/person_small.png') }}">-->
                    <p class="blue">prof. dr hab. n. med. Danuta Zwolińska<br/>
                    </p>
                </div>
<!--                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">prof. dr hab. n. med. Jacek J. Pietrzyk<br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">prof. dr hab. n. med. Barbara Królak-Olejnik<br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">prof. dr hab. n. praw. Krzysztof Krajewski<br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">prof. dr hab. n. med. Artur Mazur<br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">prof. dr hab. n. med. Jacek A. Pietrzyk<br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">prof. dr hab. n. med. Dariusz Patkowski<br/>
                    </p>
                </div>/.col-->
            </div><!--/.row-->
            <div class="row  other">
<!--                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">prof. dr hab. n. med. Sergiusz Jóźwiak<br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">prof. dr hab. n. med. Ryszard Lauterbach<br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">dr hab. n. med. Grzegorz Lis<br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">dr hab. n. med. Przemko Kwinta<br/>
                    </p>
                </div>/.col
                <div class="col-lg-4  col-md-4  col-sm-12  person">
                    <img src="{{ url('conference/img/person_small.png') }}">
                    <p class="blue">dr n. med. Piotr Kruczek<br/>
                    </p>
                </div>/.col-->
            </div><!--/.row-->
        </div><!--/.wrapper-->
<!--        <div class="button-container">
            <p class="button  other-show">zobacz wszystkich członków komitetu naukowego</p>
        </div>-->
    </section><!--/.comitee-->

    <section class="programme  container">
        <div class="wrapper">
            <h2 class="blue capital">program</h2>
            <center><h3>PROGRAM WKRÓTCE</h3></center>
<!--
            <div role="tabpanel">


              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#day1" aria-controls="day1" role="tab" data-toggle="tab">
                        Dzień I <br/><span>31.01.2015</span>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#day2" aria-controls="day2" role="tab" data-toggle="tab">
                        Dzień II <br/><span>1.02.2015</span>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">
                        Controversies in Neonatology Programme
                    </a>
                </li>
              </ul>

              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="day1">
                    <h3 class="blue">Aula Duża A</h3>
                    <div class="row">
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>00</sup></p>
                                <p class="dark">Rozpoczęcie konferencji</p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>15</sup></p>
                                <p class="dark">Sesja 1 <br/> 
                                <span>Endokrynologia - subkliniczna niedoczynność tarczycy - leczyć czy nie leczyć?</span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">11<sup>15</sup></p>
                                <p class="dark">Przerwa na kawę</p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">11<sup>30</sup></p>
                                <p class="dark">Sesja 2 <br/> 
                                <span>Kontrowersje wokół marihuany: czy uda się połączyć pragmatyzm z rozsądkiem?</span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>10</sup></p>
                                <p class="dark">Sesja 3 <br/> 
                                <span>Astma oskrzelowa u dzieci</span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>00</sup></p>
                                <p class="dark">Przerwa na obiad</p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">15<sup>00</sup></p>
                                <p class="dark">Sesja 4 <br/> 
                                <span>Borelioza i kleszczowe zapalenie mózgu jako rosnący problem epidemiologiczny i kliniczny - kiedy obserwować? Kiedy leczyć? Jak zapobiegać?<br/>
                                </span></p> 
                            </div>
                        </div>
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">16<sup>00</sup></p>
                                <p class="dark">Przerwa na kawę</p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">16<sup>15</sup></p>
                                <p class="dark">Sesja 5 <br/> 
                                <span>Alergia pokarmowa<br/>
                                Patronat: Mead Johnson Nutrition
                                </span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">17<sup>05</sup></p>
                                <p class="dark">Sesja 6 <br/> 
                                <span>Spotkanie z ekspertem <br/>Wczesne rozpoznawanie stwardnienia guzowatego u dzieci<br/>
                                Patronat: Novartis
                                </span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">17<sup>25</sup></p>
                                <p class="dark">Przerwa na kawę</p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">17<sup>35</sup></p>
                                <p class="dark">Sesja 7 <br/> 
                                <span>Terapia przeciwbólowa, przeciwzapalna i przeciwgorączkowa u dzieci – pytania i odpowiedzi, praktyczne wskazówki, aktualne zalecenia<br/>
                                Patronat: Reckitt Benckiser
                                </span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">18<sup>20</sup></p>
                                <p class="dark">Poczęstunek<br/>
                                Wieczór inauguracyjny</p> 
                            </div>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="day2">
                    <h3 class="blue">Aula Duża A</h3>
                    <div class="row">
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">9<sup>00</sup></p>
                                <p class="dark">Sesja 8 <br/> 
                                <span>Spotkanie z praktykiem<br/>Porozmawiajmy o alergii na białka mleka krowiego. Przypadki kliniczne.
                                </span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">9<sup>30</sup></p>
                                <p class="dark">Sesja 9 <br/> 
                                <span>Bezpieczeństwo nerek w farmakoterapii
                                </span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>50</sup></p>
                                <p class="dark">Przerwa na kawę <br/> </p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">11<sup>05</sup></p>
                                <p class="dark">Sesja 10<br/> 
                                <span>Ostry ból brzucha - punkt widzenia pediatry i chirurga
                                </span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">12<sup>05</sup></p>
                                <p class="dark">Sesja 11<br/> 
                                <span>Praktyczne aspekty antybiotykoterapii i nebulizacji u dzieci
                                </span></p> 
                            </div>
                        </div>
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">12<sup>55</sup></p>
                                <p class="dark">Przerwa na obiad<br/> </p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>55</sup></p>
                                <p class="dark">Sesja 12<br/> 
                                <span>Epidemiologia i profilaktyka zakażeń pneumokokowych<br/>
                                Patronat: Pfizer
                                </span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>45</sup></p>
                                <p class="dark">Przerwa na kawę<br/> </p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">15<sup>00</sup></p>
                                <p class="dark">Sesja 13<br/> 
                                <span>Ból w klatce piersiowej - kiedy wskazana konsultacja kardiologiczna?
                                </span></p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">16<sup>00</sup></p>
                                <p class="dark">Zakończenie konferencji<br/> </p> 
                            </div>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="eng">
                    <h3 class="blue">Saturday 1st February 2014 – Aula Średnia</h3>
                    <div class="row">
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">9<sup>00</sup></p>
                                <p class="dark">Inauguration<br/> </p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">9<sup>15</sup></p>
                                <p class="dark">Restricted or liberal approach to the treatment with blood and blood products. Are growth factors good alternative?<br/> 
                                Chairman: Professor Barbara Królak-Olejnik</p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>35</sup></p>
                                <p class="dark">Coffee break<br/> </p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">10<sup>50</sup></p>
                                <p class="dark">Seizures in the newborns - when to treat, when to observe?<br/> 
                                Chairman: Professor Sergiusz Jóźwiak</p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">12<sup>10</sup></p>
                                <p class="dark">Lunch<br/> </p> 
                            </div>
                        </div>
                        <div class="col-lg-6  col-md-6  col-sm-12">
                            <div class="schedule  container">
                                <p class="blue  hour">13<sup>10</sup></p>
                                <p class="dark">Shock in the newborn - is the current algorithm for diagnosis and treatment still valid?<br/> 
                                Chairman: Professor Przemko Kwinta</p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>30</sup></p>
                                <p class="dark">Coffee break<br/> </p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">14<sup>45</sup></p>
                                <p class="dark">Prophylactic, empiric and prolonged administration of antibiotics and antimycotics in the NICU, do the benefits outweigh the harm?<br/> 
                                Chairman: Professor Ryszard Lauterbach</p> 
                            </div>
                            <div class="schedule  container">
                                <p class="blue  hour">16<sup>05</sup></p>
                                <p class="dark">Close of the Conference<br/> </p> 
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              
              ukryty program -->

            </div>
<!--
            <div class="button-container">
                <a href="{{ url('conference/kalendarium.pdf') }}" target="_blank" class="button">pobierz szczegółowy program konferencji</a>
                <p class="grey">Informujemy Państwa, iż ostateczny program konferencji może ulec zmianie z przyczyn od nas niezależnych</p>
            </div>
-->            
        </div><!--/.wrapper-->
    </section><!--/.programme-->
    
    <section class="price  container  ptp2">
        <div class="wrapper">
            <h3 class="white">Preferencyjne ceny rejestracji jeszcze przez:</h3>
            <div id="clock4"></div>
            <div class="row">
                <div class="col-lg-3  col-md-6  col-sm-12  price-column">
                    <p class="blue">Członkowie<br/> PTP</p>
                    <h2 class="white">600 zł</h2>
                    <h3 class="blue">700 zł</h3>
                </div><!--/.col-->
                <div class="col-lg-3  col-md-6  col-sm-12  price-column">
                    <p class="blue">Lekarze niebędący<br/> członkami PTP</p>
                    <h2 class="white">700 zł</h2>
                    <h3 class="blue">800 zł</h3>
                </div><!--/.col-->
                <div class="col-lg-3  col-md-6  col-sm-12  price-column">
                    <p class="blue">Lekarze w trakcie<br/>specjalizacji</p>
                    <h2 class="white">500 zł</h2>
                    <h3 class="blue">600 zł</h3>
                </div><!--/.col-->
                <div class="col-lg-3  col-md-6  col-sm-12  price-column">
                    <p class="blue">Studenci<br/> medycyny</p>
                    <h2 class="white">400 zł</h2>
                    <h3 class="blue">500 zł</h3>
                </div><!--/.col-->
                <div class="col-lg-3  col-md-6  col-sm-12  price-column"></div><!--/.col-->
                <div class="col-lg-3  col-md-6  col-sm-12  price-column">
                    <p class="blue">Impreza<br/> towarzysząca</p>
                    <h2 class="white">150 zł</h2>
                </div><!--/.col-->
                <div class="col-lg-3  col-md-6  col-sm-12  price-column">
                    <p class="blue">Osoba<br/> towarzsząca</p>
                    <h2 class="white">250 zł</h2>
                </div><!--/.col-->
                <div class="col-lg-3  col-md-6  col-sm-12  price-column"></div><!--/.col-->
            </div><!--/.row-->
            
            <div class="button-container" style="margin-top: 35px;">
                <a href="" class="button" data-toggle="modal" data-target="#loginModal">zarejestruj się</a>
            </div>
        </div><!--/.wrapper-->
    </section><!--/.price-->

    <section class="map  container  relative">
        <div class="map-navigation"><!--część ukryta na mapie-->
            <div class="map-nav  container">
                <h3 class="blue">Lokalizacja</h3>
                <p class="grey">Auditorium Maximum Uniwersytetu Jagiellońskiego<br/>Kraków, Krupnicza 33</p>
                <form>
                    <input type="text" placeholder="wpisz, skąd chcesz dojechać" required>
                    <input type="submit" value="wyznacz trasę">
                </form>
                <div class="button-container">
                    <a href="" class="button">wyświetl lokalizacje hoteli</a>
                </div>
            </div>
            <div class="map-nav  container">
                <h3 class="blue">Parking</h3>
                <p class="grey">Uwaga, Auditorium Maximum nie posiada własnego parkingu!</p>
                <div class="button-container">
                    <a href="" class="button">wyświetl lokalizacje parkingów</a>
                </div>
            </div>
        </div><!--/.map-navigation-->
        <div id="map-canvas"></div>
    </section><!--/.map-->

    <section class="sponsors  container">
        <div class="wrapper">
            <h2 class="blue  capital">sponsorzy i patroni</h2>
            <div class="row">
              <center><img src="{{ url('conference/img/logo_terapia.jpg') }}" /></center>
            </div><!--/.row-->
        </div><!--/.wrapper-->
    </section>

@include('conference.kontakt')

    <!-- JavaScript -->
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery/core/jquery-1.11.1.min.js') }}"></script>
    <!-- Jquery plugins:-->
    <script src="{{ url('conference/js/plugins/countdown.js') }}"></script>
    
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery-ui/core/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/transition.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/alert.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/button.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/carousel.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/collapse.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/dropdown.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/modal.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/scrollspy.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tab.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tooltip.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/popover.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/affix.js') }}"></script>
    <script src="{{ url('conference/js/project/project.js') }}"></script>
    <script src="{{ url('conference/js/project/script.js') }}"></script>
    @include('conference.popover')
</body>

</html>
