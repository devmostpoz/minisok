<!DOCTYPE html>
<!-- Change to your language -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title }}</title>

    <!-- Core Bootstrap CSS -->
    <link href="{{ url('conference/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('conference/bootstrap.mini.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>

<body>
    <header class="header  container  relative  ptp  szkola">
        <img src="{{ url('conference/img/szkolaptp.jpg') }}">     
    </header>

    <section class="greetings  container  szkola">
        <div class="wrapper  relative">
            <div class="row">
                <div class="col-lg-12  col-md-12  col-sm-12">
                    <h2 class="blue">Szanowni Państwo,</h2>
                    <p class="grey">Drogie Koleżanki i Koledzy,</p>
                    <p class="grey just">mam przyjemność zaprosić Państwa do udziału w kolejnej, siedemnastej edycji Podyplomowej Szkoły Polskiego Towarzystwa Pediatrycznego. W 2015 roku mija osiem lat wspólnej historii! To powód do dumy, z tego co razem do tej pory osiągnęliśmy oraz wyzwanie względem przyszłości.</p>
                </div><!--/.col-->
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-8  col-md-8  col-sm-12">
                    <p class="grey just">Nadrzędnym celem Szkoły jest ustawiczne kształcenie lekarzy pediatrów oraz lekarzy rodzinnych. Uczestnicząc w Podyplomowej Szkole PTP otrzymujecie Państwo unikatową okazję poszerzania wiedzy w zakresie pediatrii, z najwybitniejszymi specjalistami w naszym kraju. Na podkreślenie zasługuje fakt, że tematy, które są poruszane w Szkole, są odpowiedzią na zgłaszane zapotrzebowanie środowiska pediatrycznego.Bacząc na naszą wspaniałą historię i tradycję przygotowaliśmy, na bazie dotychczasowych doświadczeń, cykl kilkunastu sesji o wyjątkowo praktycznym wymiarze. Tegorocznym motto Szkoły jest "Wiedza, Praktyka, Prestiż".</p>
                    <p class="grey just">Żywię głęboką nadzieję, że tematyka oraz praktyczny charakter Szkoły, zachęcą Państwa do udziału w Podyplomowej Szkole Polskiego Towarzystwa Pediatrycznego i pozwolą na aktywne uczestnictwo w zajęciach, do czego serdecznie namawiam.<br/>
                    Z poważaniem</p>
                    <p class="grey">Mieczysława Czerwionka - Szaflarska, prof. zw. dr hab. n. med., Kierownik Katedry i Kliniki Pediatrii, Alergologii i Gastroenterologii Collegium Medicum im. L. Rydygiera w Bydgoszczy Uniwersytetu Mikołaja Kopernika w Toruniu. Lekarz, nauczyciel akademicki z czterdziestoletnim stażem. Specjalista II stopnia w zakresie chorób dzieci, alergologii i gastroenterologii.</p>
                </div><!--/.col-->
                <div class="col-lg-8  col-md-8  col-sm-12">
                </div><!--/.col-->
                <div class="person_big  absolute">
                    <img src="{{ url('conference/img/profczerwionka.jpg') }}" class="photo" />
                    <p>prof. dr hab. n. med.<br />Mieczysława Czerwionka-Szaflarska</p>
                    <p class="dark">Dyrektor Podyplomowej Szkoły<br />Polskiego Towarzystwa Pediatrycznego</p>                    
                </div>
            </div><!--/.row-->
        </div><!--/.wrapper-->
    </section><!--/.greetings-->

    <section class="commitee  container ptp">
        <div class="wrapper">
            <h2 class="white  capital">TEMATY WIODĄCE </h2>
            <ul class="white">
                <li>Zakażenia dróg oddechowych u dzieci</li>
                <li>Leki mukoaktywne</li>
                <li>Zespół nerczycowy u dzieci</li>
                <li>Poalkoholowy zespół płodowy</li>
                <li>Nadwrażliwość IgE</li>
                <li>Alergia pokarmowa</li>
                <li>Wyzwania współczesnej antybiotykoterapii</li>
                <li>Wyzwania w leczeniu alergii u dzieci</li>
                <li>Astma u nastolatków</li>
                <li>Gorączka i ból w praktyce pediatrycznej</li>
            </ul>
            <h2 class="white  capital">PUNKTY EDUKACYJNE</h2>
            <ul class="white">
                <li>Lekarz uczestniczący w sympozjum uzyskuje punkty edukacyjne zgodnie z Rozporządzeniem Ministra Zdrowia w sprawie sposobów dopełnienia obowiązków doskonalenia zawodowego lekarzy i lekarzy dentystów z dnia 06.10.2004r. zał. nr 3. <a href="http://static1.money.pl/d/akty_prawne/pdf/DU/2004/231/DU20042312326.pdf" target="_blank">Pełen tekst rozporządzenia</a></li>
            </ul>
            <h2 class="white  capital">CERTYFIKATY </h2>
            <ul class="white">
                <li>Warunkiem otrzymania certyfikatu potwierdzającego uzyskanie punktów edukacyjnych jest Państwa obecność na sympozjum oraz rejestracja przy stoisku firmy SYMPOSION. Certyfikat otrzymują Państwo na zakończenie sympozjum. </li>
            </ul>
            <h2 class="white  capital">ANKIETY</h2>
            <ul class="white">
                <li>Uprzejmie prosimy o wypełnianie ankiet wręczanych Państwu przy rejestracji. Dokładnie wypełnione ankiety umożliwiają nam poznanie Państwa indywidualnych preferencji co do tematyki sympozjów. Informacje te posłużą nam do celowanego informowania o spotkaniach będących w zakresie Państwa zainteresowań. </li>
            </ul>
            <h2 class="white  capital">LOSOWANIE NAGRÓD-NIESPODZIANEK</h2>
            <ul class="white">
                <li>Na zakończenie sympozjum wśród osób, które wypełniły ankietę i znajdują się na sali obrad, losujemy atrakcyjne nagrody - niespodzianki.</li>
            </ul>
        </div><!--/.wrapper-->
    </section><!--/.comitee-->

    <section class="programme  container">
        <div class="wrapper">
            <h2 class="blue  capital">program</h2>
            <h3 class="blue  center">7.30 - 8.00 Rejestracja uczestnictwa<br/>8.00 - 16.00 Sesje wykładowe</h3>
           
            <div class="button-container">
                <a href="{{ url('conference/kalendarium.pdf') }}" target="_blank" class="button">pobierz kalendarium konferencji</a>
                <p class="grey">Informujemy Państwa, iż ostateczny program konferencji może ulec zmianie z przyczyn od nas niezależnych</p>
            </div>
        </div><!--/.wrapper-->
    </section><!--/.programme-->
    
    <section class="price  container  ptp2">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-12  col-md-12  col-sm-12">
                    <h2 class="white">Udział w konferencji jest bezpłatny, jednak wymaga rejestracji.</h3> 
                </div><!--/.col-->
            </div><!--/.row-->
            <div class="button-container">
                <a href="" class="button" data-toggle="modal" data-target="#loginModal">zarejestruj się</a>
            </div>
        </div><!--/.wrapper-->
    </section><!--/.price-->

    <section class="map  container  relative  center">
        <div class="wrapper">
            <img src="{{ url('conference/img/mapa.png') }}" class="mapka">
        </div><!--/.wrapper-->
    </section><!--/.map-->

    <section class="sponsors  container">
        <div class="wrapper">
            <h2 class="blue  capital">sponsorzy i patroni</h2>
            <div class="row">
              <center><img src="{{ url('conference/img/logo_terapia.jpg') }}" /></center>
            </div><!--/.row-->
        </div><!--/.wrapper-->
    </section>

@include('conference.kontakt')

    <!-- JavaScript -->
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery/core/jquery-1.11.1.min.js') }}"></script>
    <!-- Jquery plugins:-->
    <script src="{{ url('conference/js/plugins/countdown.js') }}"></script>
    
    <!-- Jquery core: -->
    <script src="{{ url('conference/js/jquery-ui/core/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/transition.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/alert.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/button.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/carousel.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/collapse.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/dropdown.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/modal.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/scrollspy.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tab.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tooltip.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/popover.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/affix.js') }}"></script>
    <script src="{{ url('conference/js/project/project.js') }}"></script>
    <script src="{{ url('conference/js/project/script.js') }}"></script>
    @include('conference.popover')
</body>

</html>
