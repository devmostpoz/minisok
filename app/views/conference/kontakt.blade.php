    <footer class="footer  container">
        <div class="wrapper">
            <h2 class="blue  capital">kontakt</h2>
            <div class="row">
                <div class="col-lg-4  col-md-6  col-sm-12">
                    <h3 class="blue">Zadaj pytanie</h3>
                    {{ Form::open(['url' => Request::url(), 'id' => 'formKontakt'])}}
                        {{ Form::hidden('id', $conference->id) }}
                        {{ Form::hidden('link', url()) }}
                        {{ Form::text('name', Input::old('name'), ['placeholder' => 'Imię i Nazwisko', 'required'])}}
                        {{ Form::email('email', Input::old('email'), ['placeholder' => 'Adres e-mail', 'required'])}}
                        {{ Form::textarea('text', Input::old('text'), ['placeholder' => 'Treść pytania', 'required'])}}
                        
                            <span class="error"></span>
                            <span class="success">Pytanie zostało wysłane.</span>
                            <div>
                                {{ Form::submit("wyślij") }}
                                <img src=" {{ url('conference/img/loader.gif') }}" alt="loader" class="imgLoader"/>
                            </div>
                        
                    {{ Form::close() }}
                </div><!--/.col-->
                <div class="col-lg-4  col-md-6  col-sm-12">
                    <h3 class="blue">Kontakt dla wystawców</h3>
                    <p>
                        <span>Bartosz Krupiński<br/>
                        Senior Account Manager</span><br/>
                        tel. 61 66 28 192, 504 038 531<br/>
                        bartosz.krupinski@symposion.pl<br/>
                    </p>
                    <p>
                        <span>Małgorzata Nowakowska<br/>
                        Account Manager</span><br/>
                        tel. 61 66 28 173, 504 038 512<br/>
                        malgorzata.nowakowska@symposion.pl<br/>
                    </p>
<!--                    <p>
                        <span>Bartosz Nowak<br/>
                        Account Manager</span><br/>
                        tel. 61 66 28 178, 501 937 041<br/>
                        bartosz.nowak@symposion.pl<br/>
                    </p>                                        -->
                    <h3 class="blue">Media i promocja</h3>
                    <p>
                        <span>Michał Springer<br/>
                        Marketing Manager</span><br/>
                        tel. 505 114 984<br/>
                        michal.springer@symposion.pl<br/>
                   </p>                    
                </div><!--/.col-->
                <div class="col-lg-4  col-md-6  col-sm-12">
                   <h3 class="blue">Kontakt w sprawach organizacyjnych</h3>
                    <p>
                        <span> Agata Krych<br/>
                        Project Manager</span><br/>
                        tel. 506 082 429<br/>
                        agata.krych@symposion.pl<br/>
                   </p>                   
                    <p>
                        <span> Symposion</span><br/>        
                        ul. Obornicka 229, 60-650 Poznań<br/>
                        tel. 61 662 81 70<br/>
                        fax 61 662 81 71<br/>
                        biuro@symposion.pl<br/>
                        www.symposion.pl<br/>
                   </p>
                </div><!--/.col-->
            </div><!--/.row-->
        </div><!--/.wrapper-->
    </footer>

<script>
    
</script>