<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Generated Order Form - OpenPayU v2</title>
    <link rel="stylesheet" href="../../layout/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../layout/css/style.css">
    <style type="text/css">
        #payu-payment-form button[type=submit]{
            border: 0px;
            height: 35px;
            width: 140px;
            background: url('http://static.payu.com/pl/standard/partners/buttons/payu_account_button_long_03.png');
            background-repeat: no-repeat;
            cursor: pointer;
        }
    </style>
</head>
</head>

<body>
<div class="container">
    <div class="page-header">
        <h1>Generated Order Form - OpenPayU v2.1</h1>
    </div>
    <div id="message"></div>
    <div id="unregisteredCardData">
        <pre>
            <?php echo htmlentities($rsp); ?>
        </pre>
        {{ $rsp }}

    </div>
</div>
</html>