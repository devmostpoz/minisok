<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Resetowanie hasła.</title>

    <!-- Core Bootstrap CSS -->
    <link href="http://78.138.97.142/2_symposion/public/conference/css/style.css" rel="stylesheet">
    <link href="http://78.138.97.142/2_symposion/public/conference/bootstrap.mini.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>

<body>
    
        <div class="wrapper">
           <div class="row">
        {{ Form::open(['url' => action('AdminRemindersController@postReset')]) }}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group">
                {{ Form::label('email', 'Twój adres email') }}
                {{ Form::email('email', Input::old('email'), ['class' => 'form-control', 'autofocus', 'required']) }}
            </div>
            <div class="form-group">
                {{ Form::label('password', 'Nowe hasło') }}
                {{ Form::password('password', ['class' => 'form-control', 'required']) }}
            </div>
            <div class="form-group">
                {{ Form::label('password_confirmation', 'Powtórz hasło') }}
                {{ Form::password('password_confirmation', ['class' => 'form-control', 'required']) }}
            </div>
            <button type="submit" class="btn btn-primary">Zachowaj</button>
            <a href="{{ url('konferencja/ptp') }}" class="btn btn-default">Anuluj</a>
        {{ Form::close() }}

           </div>
        </div>
    <!-- JavaScript -->
    <!-- Jquery core: -->
    <script src="http://78.138.97.142/2_symposion/public/conference/js/jquery/core/jquery-1.11.1.min.js"></script>
   
    <script src="http://78.138.97.142/2_symposion/public/conference/js/bootstrap/alert.js"></script>
    <script src="http://78.138.97.142/2_symposion/public/conference/js/bootstrap/button.js"></script>
    <script src="http://78.138.97.142/2_symposion/public/conference/js/bootstrap/modal.js"></script>
    <script src="http://78.138.97.142/2_symposion/public/conference/js/bootstrap/popover.js"></script>
    <script src="http://78.138.97.142/2_symposion/public/conference/js/bootstrap/affix.js"></script>
    <script src="http://78.138.97.142/2_symposion/public/conference/js/project/project.js"></script>
    <script src="http://78.138.97.142/2_symposion/public/conference/js/project/script.js"></script>
    
   </body>

</html>
