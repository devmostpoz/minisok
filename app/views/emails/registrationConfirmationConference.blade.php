@if($conference_id==72)
    @if (strpos($conference, 'specjalizacji do 32 roku') !== FALSE)
        <br>Szanowna Pani Doktor,
        <br>Szanowny Panie Doktorze,
        
        <br><br>Uprzejmie dziękujemy za rejestrację udziału w 67 Kongresie Towarzystwa Chirurgów Polskich w dniach 9-12 września br. Będzie nam bardzo miło gościć Państwa w Lublinie w trakcie Kongresu.
        <br>Państwa wniosek o refundację kosztów udziału w 67 Kongresie Towarzystwa Chirurgów Polskich z tytułu grantu edukacyjnego Towarzystwa zostanie rozpatrzony w przeciągu 14 dni od momentu zgłoszenia. 
        <br>Informacja o udzieleniu grantu zostanie przekazana Państwu via e-mail
        
        <br><br>W razie jakichkolwiek pytań lub wątpliwości prosimy o kontakt
        
        <br><br>Życząc Państwu udanego Kongresu pozostaję
        
        <br><br>z poważaniem
        <br>Aleksandra Patora 
        <br>Specjalista ds. konferencji 
        <br>aleksandra.patora@symposion.pl 
        <br>tel. kom. 502 286 148
    @else
        <br>Szanowna Pani Doktor,
        <br>Szanowny Panie Doktorze,
        
        <br><br>Uprzejmie dziękujemy za rejestrację udziału w 67 Kongresie Towarzystwa Chirurgów Polskich w dniach 9-12 września br. Będzie nam bardzo miło gościć Państwa w Lublinie w trakcie Kongresu.
        <br>Aby dopełnić procesu rejestracji uprzejmie prosimy o dokonanie płatności
        
        <br><br>Poniżej znajduje się numer konta, który został przypisany do zgłoszenia. 
        
        <br><br>Prosimy o dokonanie wpłaty przelewem w ciągu 14 dni. Jeżeli wpłata nie zostanie zaksięgowana na naszym koncie w tym terminie, zgłoszenie automatycznie wygaśnie! 
        
        <br><br>Bank: mBank S.A.
        
        <br><br>Numer konta: 93 1140 1124 0000 3114 7700 1002
        
        <br><br>Odbiorca: Symposion Sp. z o.o., ul. Obornicka 229, 60-650 Poznań 
        
        <br><br>Tytułem: 67 Kongres Towarzystwa Chirurgów Polskich
        
        <br><br>{{$price}}
        
        <br><br>W razie jakichkolwiek pytań lub wątpliwości prosimy o kontakt
        
        <br><br>Życząc Państwu udanego Kongresu pozostaję
        
        <br><br>z poważaniem
        
        <br><br>Aleksandra Patora 
        <br>Specjalista ds. konferencji 
        <br>aleksandra.patora@symposion.pl 
        <br>tel. kom. 502 286 148
    @endif


@else
    <strong>Dziękujemy za rejestrację!</strong>

    <br><br>Bardzo cieszymy się że będziecie Państwo uczestniczyć w naszej konferencji.

    <br><br>Poniżej znajduje się <strong>numer konta</strong>, który został przypisany do zgłoszenia. 

    <br><br>Prosimy o dokonanie wpłaty przelewem <strong>w ciągu 14 dni</strong>. Jeżeli wpłata nie zostanie zaksięgowana na naszym koncie w tym terminie, zgłoszenie automatycznie wygaśnie! 

    <br><br>Bank: mBank S.A.

    <br><br>Numer konta: <strong>93 1140 1124 0000 3114 7700 1002</strong>

    <br><br>Odbiorca: <strong>Symposion Sp. z o.o., ul. Obornicka 229, 60-650 Poznań </strong>

    <br><br>Tytułem: <strong>{{ $title }}</strong>

    <br><br>{{$price}}

    <br><br>W razie jakichkolwiek pytań lub wątpliwości prosimy o kontakt:
    <br>Aleksandra Patora
    <br>Specjalista ds. konferencji
    <br><a href="mailto:aleksandra.patora@symposion.pl">aleksandra.patora@symposion.pl</a>
    <br>tel. kom. 502 286 148
@endif