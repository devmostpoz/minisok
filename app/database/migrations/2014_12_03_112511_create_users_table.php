<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->string('name', 32);
                        $table->string('username', 32);
                        $table->string('email', 320);
                        $table->string('password', 100);
                        $table->string('remember_token', 100)->nullable();
                        $table->timestamp('last_activity')->nullable();
			$table->timestamps();
		});
                $date = new \DateTime;
                DB::table('users')->insert([
                    [
                        'id' => 1,
                        'email' => 'admin@admin.com',
                        'password' => '$2y$10$qKmYMTZY51HloXDhZc31vOuIW7uKs2J0AfIBqwhF7prYapZFR0Pt6',
                        'name' => 'Administrator',
                        'created_at' => $date,
                        'updated_at' => $date
                    ]
                ]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
