<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title }}</title>

    <!-- Core Bootstrap CSS -->
    <link href="{{ url('conference/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('conference/css/bootstrap.mini.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <style>
        input[type=checkbox] {float: left;}
        
    </style>
</head>
<body>
    <div class="register-form  container  relative">
        <div class="wrapper  form-container">
            @if (Session::has('error'))
                <div class="form-section">
                    <h2  style="color:red">{{ Session::get('error') }}</h2>
               </div>
            @elseif(Session::has('success'))
                <div class="form-section">
                    <h2 style="color:green">{{ Session::get('success') }}</h2>
               </div>
            @else
                    @if($errors->all())
                        @foreach(array_unique($errors->all()) as $error)
                        <p style="color:red">{{$error}}</p>
                        @endforeach
                    @endif
                        
                           <div class="form-section">
                                <h2>{{ $title }} - rejestracja.</h2>
                           </div>     
                        <input name="conference_id" type="hidden" value="{{ $conference->id}}" />
                           <div class="form-section">
                                <div class="input-container">
                                    @if(Request::is('szkoly/*'))
                                    {{ Form::open() }}
                                        @foreach($table as $key=>$row)
                                            <label for="{{$key}}" class="big">{{$key}} - {{$row}}</label>
                                            <input type="checkbox" name="checkbox[]" id="{{$key}}" value="{{$key}}" class="confCheck">
                                            <br>
                                        @endforeach
                                        <input type="submit" style="display:none" />
                                    {{ Form::close() }}
                                    @endif
                                    <table class="table table-striped">
                                        @if(Request::is('konferencje/*'))
                                            <?php $i=0 ?>
                                            @foreach($table as $key=>$val)
                                                @if($i==0)
                                                    <tr>
                                                        <td></td>
                                                        <td style="text-align: right;">do:</td>
                                                        @foreach($val as $k=>$v)
                                                            <th>{{$k}}</th>
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        <td><input type="checkbox" name="checkbox[]" id="{{$key}}" value="{{$key}}" class="confCheck"></td>
                                                        <td><label for="{{$key}}">{{$key}}</label></td>
                                                        @foreach($val as $k=>$v)
                                                            <td>{{$v}}</td>
                                                        @endforeach
                                                    </tr>
                                                    <?php $i++ ?>
                                                @else
                                                    <tr>
                                                        <td><input type="checkbox" name="checkbox[]" id="{{$key}}" value="{{$key}}"  class="confCheck"></td>
                                                        <td><label for="{{$key}}">{{$key}}</label></td>
                                                        @foreach($val as $k=>$v)
                                                            <td>{{$v}}</td>
                                                        @endforeach
                                                    </tr>
                                                @endif

                                            @endforeach
                                        @endif
                                    </table>
                                </div><!--/.input-container-->
                                <p class="grey  small">(można zaznaczyć więcej niż jednen termin)</p>
                           <div class="form-section">
                               @if(Request::is('konferencje/*'))
                                <div class="input-container">
                                    <div class="checkboxFaktura">
                                                <label for="faktura" class="big">Wystaw fakturę VAT</label>
                                                <input type="checkbox" name="checkFaktura" id="faktura"><br>
                                    </div>
                                    <div class="faktura">
                                                <div class="container_100">
                                                    <div class="input-container">
                                                        {{ Form::label("name", "Nazwa firmy", ["class" => "small"])}}
                                                        {{ Form::text("name", Input::old("name"), ["class" => "input-style  input-big"]) }}
                                                    </div>
                                                    <div class="input-container">
                                                        {{ Form::label("nip", "NIP", ["class" => "small"])}}
                                                        {{ Form::number("nip[0]", Input::old("nip[0]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        {{ Form::number("nip[1]", Input::old("nip[1]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        {{ Form::number("nip[2]", Input::old("nip[2]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        -
                                                        {{ Form::number("nip[3]", Input::old("nip[3]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        {{ Form::number("nip[4]", Input::old("nip[4]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        {{ Form::number("nip[5]", Input::old("nip[5]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        -
                                                        {{ Form::number("nip[6]", Input::old("nip[6]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        {{ Form::number("nip[7]", Input::old("nip[7]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        -
                                                        {{ Form::number("nip[8]", Input::old("nip[8]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        {{ Form::number("nip[9]", Input::old("nip[9]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                    </div>
                                                </div><!--/.container_100-->
                                                <div class="container_100">
                                                    <div class="input-container">
                                                        {{ Form::radio("adress","ulica", Input::old("adress")) }}
                                                        <label class="big">ulica</label>
                                                         {{ Form::radio("adress","osiedle", Input::old("adress")) }}
                                                        <label class="big">osiedle</label>
                                                         {{ Form::radio("adress","aleja", Input::old("adress")) }}
                                                        <label class="big">aleja</label>
                                                         {{ Form::radio("adress","plac", Input::old("adress")) }}
                                                        <label class="big">plac</label>
                                                    </div><!--/.input-container-->
                                                    <div class="input-container">
                                                        {{ Form::label("street_name", "nazwa", ["class" => "small"])}}
                                                        {{ Form::text("street_name", Input::old("street_name"), ["class" => "input-style  input-medium"]) }}
                                                    </div><!--/.input-container-->
                                                    <div class="input-container">
                                                        {{ Form::label("house_number", "nr", ["class" => "small"])}}
                                                        {{ Form::text("house_number", Input::old("house_number"), ["class" => "input-style  input-small"]) }}
                                                    </div><!--/.input-container-->
                                                    <div class="input-container">
                                                        {{ Form::label("flat_number", "nr lokalu", ["class" => "small"])}}
                                                        {{ Form::text("flat_number", Input::old("flat number"), ["class" => "input-style  input-small"]) }}
                                                    </div><!--/.input-container-->
                                                </div><!--/.container_100-->
                                                <div class="container_100">
                                                    <div class="input-container">
                                                        {{ Form::label("postal_code", "kod pocztowy", ["class" => "small"])}}
                                                        {{ Form::number("postal_code[0]", Input::old("postal_code[0]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        {{ Form::number("postal_code[1]", Input::old("postal_code[1]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        -
                                                        {{ Form::number("postal_code[2]", Input::old("postal_code[2]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        {{ Form::number("postal_code[3]", Input::old("postal_code[3]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                        {{ Form::number("postal_code[4]", Input::old("postal_code[4]"), ["class" => "input-smaller input-style", "maxlength" => "1"]) }}
                                                    </div><!--/.input-container-->
                                                    <div class="input-container">
                                                        {{ Form::label("city", "miejscowość", ["class" => "small"])}}
                                                        {{ Form::text("city", Input::old("city"), ["class" => "input-style  input-medium"]) }}
                                                    </div><!--/.input-container-->
                                                    <div class="input-container">
                                                        {{ Form::label("post_office", "poczta", ["class" => "small"])}}
                                                        {{ Form::text("post_office", Input::old("post_office"), ["class" => "input-style  input-medium"]) }}
                                                    </div><!--/.input-container-->
                                                </div><!--/.container_100-->
                                    </div>
                               </div>
                               @endif
                            <div>

                            </div>
                           <div class="form-section  center">
                               <p class="controllMessages" style="color:red; display: none">Musisz zaznaczyć co najmniej jeden termin.</p>
                               <p class="controllMessagesFaktura" style="color:red; display: none">Proszę wypełnić wszystkie pola w fakturze.</p>
                                @if(Request::is('konferencje/*'))
                                    <input type="submit" value="Zarejestruj" class="confirmModal" onclick="check()">
                                @else
                                    <input type="submit" value="Zarejestruj" onclick="$('form').submit();">
                                @endif
                                
                                <a href="{{ URL::previous() }}" class="cancel">Anuluj</a>
                           </div><!--/.form-section-->
            @endif
        </div><!--/.wrapper-->
    </div>
  <!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Potwierdzenie płatności</h4>
      </div>
      <div class="modal-body">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#dataConfirm" onclick="payuConfirm()" >Potwierdź</button>
        <!--<button type="button" class="btn btn-primary" onclick="payuConfirm();">Potwierdź</button>-->
      </div>
    </div>
  </div>
</div> 
  <div class="modal fade" id="dataConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Potwierdzenie rejestracji na konferencje. </h4>
      </div>
      <div class="modal-body">
            <strong>Dziękujemy za rejestrację!</strong>

            <br><br>Poniżej znajduje się <strong>numer konta</strong>, który został przypisany do zgłoszenia. 

            <br><br>Prosimy o dokonanie wpłaty przelewem <strong>w ciągu 14 dni</strong>. Jeżeli wpłata nie zostanie zaksięgowana na naszym koncie w tym terminie, zgłoszenie automatycznie wygaśnie! 

            <br><br>Bank: mBank S.A.

            <br><br>Numer konta: <strong>93 1140 1124 0000 3114 7700 1002</strong>

            <br><br>Odbiorca: <strong>Symposion Sp. z o.o., ul. Obornicka 229, 60-650 Poznań </strong>

            <br><br>Tytułem: <strong>{{ $title }}</strong>
            
            <br><br><span class="toPay"></span>
      </div>
      <div class="modal-footer">
            <a href="{{ url($url.'/'.$name)}}" type="button" class="btn btn-default">Zakończ</a>
      </div>
    </div>
  </div>
</div> 
    <script src="{{ url('conference/js/jquery/core/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ url('conference/js/plugins/countdown.js') }}"></script>
    <script src="{{ url('conference/js/jquery-ui/core/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/transition.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/alert.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/button.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/carousel.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/collapse.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/dropdown.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/modal.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/scrollspy.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tab.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/tooltip.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/popover.js') }}"></script>
    <script src="{{ url('conference/js/bootstrap/affix.js') }}"></script>
    <script src="{{ url('conference/js/project/project.js') }}"></script>
    <script>
        
        $('.input-smaller').on("keyup", function(e){
            var code = parseInt(e.keyCode);
            $('.input-smaller').each(function(){
                if($(this).val().length>1){
                        $(this).val($(this).val().substring(0,1));
                }
            });
            if((e.keyCode>=48 && e.keyCode<=57) || e.keyCode == 37 || e.keyCode == 8 || (e.keyCode>=96 && e.keyCode<=105)) {
                if(code == 8 || code == 37) {
                  $(this).prev().select();
                }
                else {
                  $(this).next().select();
                }
            } else 
                $(this).val('');
        });
    
    
        $('#faktura').on("change",function(){
            if($('#faktura').prop('checked'))
                $('div.faktura').slideDown(500);
            else
                $('div.faktura').slideUp(500);
        });
        
        
        
            function payuConfirm(){
                var checked = [];
                $('input[type="checkbox"].confCheck').each(function(){
                    
                    if($(this).prop('checked')){
                        checked.push($(this).val());
                    }
                });
                if(checked.length == 0)
                    return false;
//                $('#payu-payment-form button').click();

                    var name = $('input[name=name]').val();
                    var nip = $('input[name="nip[0]"]').val();
                    nip = nip + $('input[name="nip[1]"]').val();
                    nip = nip + $('input[name="nip[2]"]').val();
                    nip = nip + "-";
                    nip = nip + $('input[name="nip[3]"]').val();
                    nip = nip + $('input[name="nip[4]"]').val();
                    nip = nip + $('input[name="nip[5]"]').val();
                    nip = nip + "-";
                    nip = nip + $('input[name="nip[6]"]').val();
                    nip = nip + $('input[name="nip[7]"]').val();
                    nip = nip + "-";
                    nip = nip + $('input[name="nip[8]"]').val();
                    nip = nip + $('input[name="nip[9]"]').val();
                    var address = $("input[name=adress]:checked").val();
                    var street_name = $('input[name=street_name]').val();
                    var house_number = $('input[name=house_number]').val();
                    var flat_number = $('input[name=flat_number]').val();
                    var postal_code = $('input[name="postal_code[0]"]').val();
                    postal_code = postal_code + $('input[name="postal_code[1]"]').val();
                    postal_code = postal_code + "-";
                    postal_code = postal_code + $('input[name="postal_code[2]"]').val();
                    postal_code = postal_code + $('input[name="postal_code[3]"]').val();
                    postal_code = postal_code + $('input[name="postal_code[4]"]').val();
                    var city = $('input[name=city]').val();
                    var post_office = $('input[name=post_office]').val();
                    if($('#faktura').prop('checked'))
                        var faktura = 1;
                    else 
                        var faktura = 0;
                    var toPay = $('span.toPay').text();
                $.ajax({
                    url: '{{Request::url()}}/zlozenie_platnosci',
                    type: 'POST',
                    data: { conference: checked, faktura:faktura, name:name, nip:nip, address:address, street_name:street_name, house_number:house_number, flat_number:flat_number, postal_code:postal_code, city:city, post_office:post_office, toPay:toPay },
                    success: function (response) { 
                    }
                });
            }
            
            function check(){
                var controll=1;
                if($('#faktura').prop('checked')){
                    var name = $('input[name=name]').val();
                    var nip = $('input[name="nip[0]"]').val();
                    nip = nip + $('input[name="nip[1]"]').val();
                    nip = nip + $('input[name="nip[2]"]').val();
                    nip = nip + "-";
                    nip = nip + $('input[name="nip[3]"]').val();
                    nip = nip + $('input[name="nip[4]"]').val();
                    nip = nip + $('input[name="nip[5]"]').val();
                    nip = nip + "-";
                    nip = nip + $('input[name="nip[6]"]').val();
                    nip = nip + $('input[name="nip[7]"]').val();
                    nip = nip + "-";
                    nip = nip + $('input[name="nip[8]"]').val();
                    nip = nip + $('input[name="nip[9]"]').val();
                    var address = $("input[name=adress]:checked").val();
                    var street_name = $('input[name=street_name]').val();
                    var house_number = $('input[name=house_number]').val();
                    var flat_number = $('input[name=flat_number]').val();
                    var postal_code = $('input[name="postal_code[0]"]').val();
                    postal_code = postal_code + $('input[name="postal_code[1]"]').val();
                    postal_code = postal_code + "-";
                    postal_code = postal_code + $('input[name="postal_code[2]"]').val();
                    postal_code = postal_code + $('input[name="postal_code[3]"]').val();
                    postal_code = postal_code + $('input[name="postal_code[4]"]').val();
                    var city = $('input[name=city]').val();
                    var post_office = $('input[name=post_office]').val();
                    
                    if(!name || nip.length!=13 || !address || !street_name || !house_number || postal_code.length!=6  || !city || !post_office)
                        controll=0;
                }
                
                
                var checked = [];
                $('input[type="checkbox"].confCheck').each(function(){
                    if($(this).prop('checked')){
                        checked.push($(this).val());
                    }
                });
                
                if(controll==0){
                    $('.controllMessagesFaktura').show();
                    $('.controllMessages').hide();
                }else if(checked.length==0){
                    $('.controllMessages').show();
                    $('.controllMessagesFaktura').hide();
                } else{
                    $('.controllMessages').hide();
                    $('.controllMessagesFaktura').hide();
                    $('#confirmModal').modal('show');
                }
            }
            
           $('.confirmModal').click(function(){
                $('#confirmModal .modal-body').html('<img src="{{ url("conference/img/loader.gif")}}" style="text-align: center;"/>');
                var checked = [];
                $('input[type="checkbox"].confCheck').each(function(){
                    
                    if($(this).prop('checked')){
                        checked.push($(this).val());
                    }
                });
                
                    
                $.ajax({
                    
                    url: '{{Request::url()}}/potwierdzenie',
                    type: 'POST',
                    data: { conference: checked},
                    success: function (response) { 
                        $('#confirmModal .modal-body').html(response);
                        $('#dataConfirm .modal-body .toPay').html(response);
                    }
                }); 
           });

//        function basketCount(){
//             $('.basket').remove();
//               var xay = $('input[name=xay]').val();
//               //alert(xay);
//               var suma = 0;
//               var counter = 0;
//               $('input[type="checkbox"].confCheck').each(function(){
//                   
//                    if($(this).prop('checked')){
//                        var element = $(this).parent().next().next();
//                        for(i=0; i<xay; i++){
//                            element = element.next();
//                        }
//                        suma = suma + parseInt(element.text().replace(/[^0-9]/g, ''));
//                        
//                      
//                        text = '<input class="basket" type="hidden" name="products[' + counter + '].name" value="' + $(this).parent().next().text() + '">\n\
//                                <input class="basket" type="hidden" name="products[' + counter + '].unitPrice" value="' + element.text().replace(/[^0-9]/g, '') + '00">\n\
//                                <input class="basket" type="hidden" name="products[' + counter + '].quantity" value="1">';
//                        $('#basket').append(text);
//                        counter++;
//                    }
//            });
//            $('input[name=totalAmount]').val(suma +'00');
//        }
        $( "form" ).submit(function() {
            var checker = false;
            $('input[type="checkbox"].confCheck').each(function(e){
                if($(this).prop('checked'))
                    checker = true;
            });
            if(!checker){
                $('p.controllMessages').show().delay(2000).fadeOut(1000);
                return false;
            }

        });
    </script>
</body>

</html>
