<?php

class ExportCsvController extends \BaseController {
    
    public static $file = "images/contacts.csv";
    
    public function exportCsv(){
        if(Session::get('sort_konf')){
            $regUsr = RegisteredUser::all()->toArray();
            $data = ExportCsv::getData();

            $file = fopen("images/contacts.csv","w");
            
            header('Content-Type: text/html; charset=utf-8');
            fputcsv($file,  ExportCsv::polishLetter(ExportCsv::headers()),";");

            foreach ($data as $line)
              {
//              dd($line);
              fputcsv($file,ExportCsv::polishLetter($line),";");
              }
            fclose($file); 
             $headers = array(
                    'Content-Type' => 'text/csv',
            );  
            return Response::download("images/contacts.csv", 'zarejestrowani_uzytkownicy.csv', $headers);
        }
        
    }
    
    
}
