<?php

class AdminUserController extends \BaseController {

	public function login(){
                $title = 'Logowanie';
                return View::make('admin.pages.login', compact('title'));
        }
        
        public function postLogin(){
                $remember = (Input::has('remember')) ? true : false;
                
                $userdata = array(
                    'email' => Input::get('email'),
                    'password' => Input::get('password')
                );

                if(Auth::attempt($userdata, $remember)){
                    return Redirect::to('admin/konferencje')->withSuccess('Zostałeś zalogowany');
                } else {
                    return Redirect::back()->withInput()->withErrors('Zły login lub hasło');
                }
        }
        
        public function register(){
                $title = "Rejestracja";
                return View::make('admin.pages.register', compact('title'));
        }
        
        public function postRegister(){
                $validator = Validator::make(Input::all(), User::$rules);
                if ($validator->fails()) {
                    return Redirect::back()->withErrors($validator)->withInput();
                }

                $user = User::create([
                    'email' => Input::get('email'),
                    'password' => Input::get('password')
                ]);

                return Redirect::to('admin/login')->withSuccess('Konto zostało dodane');
        }
        
        public function logout(){
                Auth::logout();
                return Redirect::to('admin/login')->with('success', 'Zostałeś wylogowany');
        }

}
