<?php

class AdminPasswordsController extends \BaseController {

	public function index(){
                $title = 'Zmiana hasła';
                return View::make('admin.pages.change_password', compact('title'));
        }
        
        public function store(){
            $validator = Validator::make(Input::all(),
                    array(
                        'old_password'      => 'required',
                        'password'          => 'required',
                        'password_confirm'  => 'required|same:password',
                    )
            );
            
            if($validator->fails()){
                return Redirect::back()->withErrors($validator);
            } else {
                
                $user = User::find(Auth::user()->id);
                
                $old_password   = Input::get('old_password');
                $password       = Input::get('password');
                
                if(Hash::check($old_password, $user->getAuthPassword())){
                    
                    if($user->update(['password' => $password]))
                        return Redirect::back()->withSuccess('Hasło zostało zmienione.');
                }
                
            }
            
            return Redirect::back()->withError('Podano złe hasło.');
        }

}
