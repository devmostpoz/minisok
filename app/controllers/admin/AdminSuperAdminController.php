<?php

class AdminSuperAdminController extends \BaseController {
    

	public function index(){
            $title = 'Super Admin';
            $users = RegisteredUser::where('user_id','>','0')->get();
            return View::make('admin.superadmin.index', compact('title', 'users'));
        }
        public function edit($id){
            $title = 'Super Admin';
            $user = RegisteredUser::find($id);
            return View::make('admin.superadmin.edit', compact('title', 'user'));
        }
        public function update($id){
            $registeredUser = RegisteredUser::find($id);
            $user = User::find($registeredUser->user_id);
            if(Input::has('name')){
                $row['name'] = Input::get('name');
            }
            if(Input::has('surname')){
                $row['surname'] = Input::get('surname');
            }
            if(Input::has('email')){
                $row['email'] = Input::get('email');
            }
            if(Input::has('pwz')){
                $row['pwz'] = Input::get('pwz');
            }
            if(Input::has('password')){
                $row['password'] = Input::get('password');
            }
            
            $registeredUser->update($row);
            $user->update($row);
            
            return Redirect::to('admin/superadmin');
        }
        public function updateId(){
            $users = User::all();
            
            
            foreach($users as $user){
                $registeredUser = RegisteredUser::find($user->regular_user_id);
                if($registeredUser){
                    $row['user_id'] = $user->id;
                    $registeredUser->update($row);
                }
            }
            return 'ok';
        }

}

//GET	/resource	index	resource.index
//GET	/resource/create	create	resource.create
//POST	/resource	store	resource.store
//GET	/resource/{resource}	show	resource.show
//GET	/resource/{resource}/edit	edit	resource.edit
//PUT/PATCH	/resource/{resource}	update	resource.update
//DELETE	/resource/{resource}	destroy	resource.destroy