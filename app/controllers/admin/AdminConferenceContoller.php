<?php

class AdminConferenceController extends \BaseController {

	public function conferences(){
                if(Input::get('search_conference'))
                    $data['conference'] = Conference::where('name', 'like', '%'.Input::get('search_conference').'%')->get();
                else
                    $data['conference'] = Conference::all();
                $data['registered_user'] = ConferenceRegisteredUsers::groupBy('conference_id', 'registered_user_id', 'conference')->get();
                
                $reg = array();
                foreach($data['registered_user'] as $user){
                    $id = $user->conference_id;
                    if(!array_key_exists($id,$reg))
                        $reg[$id] = 1;
                    else
                        $reg[$id] = $reg[$id] + 1;
                }
                
                foreach($data['conference'] as $conf){
                    $id = $conf->id;
                    if(!array_key_exists($id,$reg))
                        $reg[$id] = 0;
                }
                
                $title = 'Konferencje';
                return View::make('admin.pages.conference.conferences', compact('title', 'data', 'reg'));
        }
        
        
        public function addConferences(){
                $title = 'Dodaj konferencje';
                return View::make('admin.pages.conference.addConference', compact('title'));
        }
        
        public function createConferences(){
                
                if(Input::get('what') == 'szkoly'){
                    $place_school = Input::get('place_school');
                    $date_school = Input::get('date_school');
                    for($i=0; $i<count($place_school); $i++){
                           $data[$date_school[$i]] = $place_school[$i];
                    }
                }
                
                if(Input::get('what')=="konferencje"){
                    $place_conf = Input::get('place_conf');
                    $price_conf = Input::get('price_conf');
                    $date_conf = Input::get('date_conf');
                    
                    $countDate = count($date_conf);
                    $countPlace = count($place_conf);
                    $countPrice = count($price_conf);
                    $k=0;
                    for($i=0; $i<count($place_conf); $i++){
                            $data = null;
                            for($j=0; $j<count($date_conf); $j++){
                                    $data[$date_conf[$j]] = $price_conf[$k];
                                    $k++;
                            }
                            $data2[$place_conf[$i]] = $data;
                    }
                    $data = $data2;
                }
                
                $row= [
                    'what' => Input::get('what'),
                    'link' => str_replace(" ", "-", Input::get('link')),
                    'description' => Input::get('description'),
                    'name' => Input::get('name'),
                    'title' => Input::get('name'),
                    'form' => json_encode($data),
                    'images' => Input::file('image')
                ];
                
                if (!Input::hasFile('image')) {
                    return Redirect::back()->withErrors('Musisz wgrać obrazek.');
                }
                
                list($imgWidth, $imgHeight) = getimagesize(Input::File('image'));
            
                if($imgWidth != ' 1920' OR $imgHeight != '680') {
                    return Redirect::back()->withErrors('Zły rozmiar obrazka.');
                }
                
                
                
                $validator = Validator::make($row, Conference::$rules, Conference::$errorsTranslate);
                if ($validator->fails()) {
                    return Redirect::back()->withErrors($validator);
                }
                if (Input::hasFile('image')) {
                    $row['image'] = Image::upload(Input::File('image'), 'images/conferences/');
                }
                
                if ($log = Conference::create($row))  {
                    $log = $log->toArray();
                    $log['conference_id'] = $log['id'];
                    $log['user_id'] = Auth::user()->id;
                    unset($log['id']);
                    ConferencesLog::create($log);
                    
                    return Redirect::to('/admin/konferencje')->with('success', 'Konferencja została dodana');
                } else {
                    return Redirect::to('/admin/konferencje')->withErrors('Wystąpił błąd, proszę spróbować później.');
                }
                
        }
        
        public function editConferences($id){
            $conf = Conference::find($id);
            if(!$conf)
                return Redirect::to('/admin/konferencje')->withErrors('Brak podanej konferencji.');
            if($conf->static == 1)
                return Redirect::to('/admin/konferencje')->withErrors('Nie można edytować konferencji.');
            
            $data = json_decode($conf->form, true);
            $title = 'Edycja konferencji';
            $i=0;
            return View::make('admin.pages.conference.editConference', compact('title', 'conf', 'data', 'i'));
        }
        
        public function updateConferences($id){
//            dd(Input::all());
                $conf = Conference::find($id);
                if(!$conf)
                    return Redirect::to('admin/konferencje')->withErrors('Nie znaleziono konferencji.');
                $imageOld = $conf->image;

                if(Input::get('what') == 'szkoly'){
                        $date_conf = Input::get('date_school');
                        $date_conf_old = Input::get('date_school_old');
                        for($i=0; $i<count($date_conf); $i++){
                            if($date_conf[$i]!=$date_conf_old[$i]){
                                $date_old = $conf->form;
                                $date_new = str_replace($date_conf_old[$i], $date_conf[$i], $date_old);
                                $conf->update(['form'=>$date_new]);
                                $users = ConferenceRegisteredUsers::where('conference_id','=',$id)->get();
                                foreach($users as $user){
                                    $date_old = $user->conference;
                                    $date_new = str_replace($date_conf_old[$i], $date_conf[$i], $date_old);
                                    $user->update(['conference'=>$date_new]);
                                }
                            }
                        }
//                        $place_school = Input::get('place_school');
//                        $date_school = Input::get('date_school');
//                        for($i=0; $i<count($place_school); $i++){
//                               $data[$date_school[$i]] = $place_school[$i];
//                        }
                }
                
                if(Input::get('what')=="konferencje"){
                    $date_conf = Input::get('date_conf');
                    $date_conf_old = Input::get('date_conf_old');
                    for($i=0; $i<count($date_conf); $i++){
                        if($date_conf[$i]!=$date_conf_old[$i]){
                            $date_old = $conf->form;
                            $date_new = str_replace($date_conf_old[$i], $date_conf[$i], $date_old);
                            $conf->update(['form'=>$date_new]);
                            $users = ConferenceRegisteredUsers::where('conference_id','=',$id)->get();
                            foreach($users as $user){
                                $date_old = $user->conference;
                                $date_new = str_replace($date_conf_old[$i], $date_conf[$i], $date_old);
                                $user->update(['conference'=>$date_new]);
                            }
                        }
                    }
//                    $place_conf = Input::get('place_conf');
//                    $price_conf = Input::get('price_conf');
//                    $date_conf = Input::get('date_conf');
//                    
//                    $countDate = count($date_conf);
//                    $countPlace = count($place_conf);
//                    $countPrice = count($price_conf);
//                    $k=0;
//                    for($i=0; $i<count($place_conf); $i++){
//                            $data = null;
//                            for($j=0; $j<count($date_conf); $j++){
//                                    $data[$date_conf[$j]] = $price_conf[$k];
//                                    $k++;
//                            }
//                            $data2[$place_conf[$i]] = $data;
//                    }
//                    $data = $data2;
                }
                
                $row = [
//                        'what' => Input::get('what'),
                        'description' => Input::get('description'),
                        'name' => Input::get('name'),
                        'title' => Input::get('name'),
//                        'form' => json_encode($data)
                ];

                if (Input::hasFile('image')) {
                    list($imgWidth, $imgHeight) = getimagesize(Input::File('image'));
                    if($imgWidth != ' 1920' OR $imgHeight != '680') {
                        return Redirect::back()->withErrors('Zły rozmiar obrazka.');
                    }
                    $row['images'] = Input::File('image');
                }

                $validator = Validator::make($row, Conference::$rulesUpdate, Conference::$errorsTranslate);
                if ($validator->fails())
                    return Redirect::back()->withErrors($validator);

                if (Input::hasFile('image')) {
                    $row['image'] = Image::upload(Input::File('image'), 'images/conferences/');
                    if($imageOld)
                        Image::deleteImage($imageOld, 'images/conferences/');
                }
                $log = $conf->toArray();
                $conf->update($row);
                //Log START
//                    $log = $conf->toArray();
                    $log['conference_id'] = $log['id'];
                    $log['user_id'] = Auth::user()->id;
                    unset($log['id']);
                    foreach($row as $key=>$value){
                        $log[$key.'_2']= $value;
                    }
                    ConferencesLog::create($log);
                //LOG END
                return Redirect::to('admin/konferencje')->withSuccess('Konferencja została zaktualizowana');
        }
        
        public function deleteConference(){
                $conf = Conference::find(Input::get('id'));
                if(!$conf){
                    echo 'Nie można znaleźć konferencji.';
                    exit;
                }if($conf->static == 1){
                    echo 'Nie można usunąć tej konferencji.'; 
                    exit;
                }
                $regUser = ConferenceRegisteredUsers::whereConference_id($conf->id);
                $regUser->delete();
                $conf->delete();
                echo 'ok';
                exit;
                
        }
        
        public function registeredUsers(){
                
                if (Request::isMethod('post')){
                    Session::put('sort_konf', Input::get('sort_konf'));
                    Session::put('orderBy', Input::get('orderBy'));
                    Session::put('sort_faktura', Input::get('faktura'));
                    Session::put('searchUser', Input::get('searchUser'));
                } elseif(!Session::get('sort_konf')) 
                    Session::put('sort_konf', '0');
                
                $sort_konf = Session::get('sort_konf');
                $orderBy = Session::get('orderBy')? Session::get('orderBy') : 'desc';
                $groupBy = ['conference_id', 'registered_user_id', 'conference', 'faktura', 'comments'];
                $searchUser = Session::get('searchUser');
                
                if($searchUser) {
                    if(Session::get('sort_faktura')==1)
                        if($sort_konf != '0')
                            $query = ConferenceRegisteredUsers::whereConference_id($sort_konf)->orderBy('id', $orderBy)->faktura()->Namelike($searchUser);
                        else
                            $query = ConferenceRegisteredUsers::orderBy('id', $orderBy)->faktura()->Namelike($searchUser);
                    else
                        if($sort_konf != '0')
                            $query = ConferenceRegisteredUsers::whereConference_id($sort_konf)->orderBy('id', $orderBy)->Namelike($searchUser);
                        else
                            $query = ConferenceRegisteredUsers::orderBy('id', $orderBy)->Namelike($searchUser);
                } else {
                    if(Session::get('sort_faktura')==1)
                        if($sort_konf != '0')
                            $query = ConferenceRegisteredUsers::whereConference_id($sort_konf)->orderBy('id', $orderBy)->faktura();
                        else
                            $query = ConferenceRegisteredUsers::orderBy('id', $orderBy)->faktura();
                    else
                        if($sort_konf != '0')
                            $query = ConferenceRegisteredUsers::whereConference_id($sort_konf)->orderBy('id', $orderBy);
                        else
                            $query = ConferenceRegisteredUsers::orderBy('id', $orderBy);
                }
                
                $regUser = $query->groupBy($groupBy)->paginate(10);
                $conferences = Conference::All();
                
                $user = RegisteredUser::all();
                
                
                $conf['name'][0] = 'Wybierz konferencje:';
                foreach($conferences as $conference){
                    $conf ['name'][$conference->id] = $conference->name; 
                    $conf ['what'][$conference->id] = $conference->what; 
                }
                
                foreach($regUser as $us){
//                    $users[$us->registered_user_id]->name;
                    $usr = User::find($us->registered_user_id);
                    if($usr)
                        $usr2 = RegisteredUser::find($usr->regular_user_id);
                    
                    if(isset($usr2))
                        $users[$us->registered_user_id] = $usr2;
                }
                
                $title = 'Zarejestrowani uczestnicy konferencji';
                
                return View::make('admin.pages.conference.registeredUsers', compact('title', 'conf', 'regUser', 'users'));
        }
        
        public function users(){
                if (Request::isMethod('post')){
                    if(Input::get('search')) 
                        Session::put('searchName', Input::get('search')); 
                    else
                        Session::put('searchName', ''); 
                }
                $search =  Session::get('searchName');
                if($search!='') {
                    $users = RegisteredUser::where("name",$search)->orWhere("surname", $search)->orWhere("email", $search)->paginate(10);
                } else
                    $users = RegisteredUser::paginate(10);
                
                $title = 'Użytkownicy';
                
                return View::make('admin.pages.conference.users', compact('title', 'users', 'search' ));
        }
        
        public function ajaxUser($id, $flag=null){
            if($flag){
                $user = RegisteredUser::find($id); 
            } else {
                $usr = User::find($id);
                $user = RegisteredUser::find($usr->regular_user_id); 
            }
            
            if(!$user) {
                echo "Wystpił błąd.";
                exit;
            }
            echo "<h3>$user->sex $user->science_degree $user->name $user->surname</h3>";
            echo "<table class=\"table\">";
            echo "<tr><td>PWZ</td><td>$user->pwz</td></tr>";
            echo "<tr><td>PESEL</td><td>$user->pesel</td></tr>";
            echo "<tr><td>Specjalizacja: </td><td>".implode("<br>", array_filter(json_decode($user->specialization, false)))."</td></tr>";
           
            for($i=0; $i<count(array_filter(json_decode($user->city, false))); $i++){
                if($i==0){
                    echo "<tr><td>Adres:</td><td> ";
                } else {
                    echo "<tr><td>Adres #".($i+1).":</td><td> ";
                } 
                echo (array_filter(json_decode($user->adress, false))[$i]? array_filter(json_decode($user->adress, false))[$i] : "")." ".(array_filter(json_decode($user->street_name, false))[$i]? array_filter(json_decode($user->street_name, false))[$i] : "")." ".(array_filter(json_decode($user->house_number, false))[$i]? array_filter(json_decode($user->house_number, false))[$i] : ""); 
                    if(isset(array_filter(json_decode($user->flat_number, false))[$i])) echo " m. ".array_filter(json_decode($user->flat_number, false))[$i]; 
                echo "<br>";
                if(isset(json_decode($user->postal_code, false)[$i])){
                    $postal = array_filter(json_decode($user->postal_code, false))[$i];
                    $postal_code[0] = substr($postal, 0,2);
                    $postal_code[1] = substr($postal, 2,5);
                    $postal = $postal_code[0]."-".$postal_code[1];
                } else
                    $postal = "00-000";
                echo $postal." ".(json_decode($user->city, false)[$i]? json_decode($user->city, false)[$i]: "");
                echo "<table><tr><td>Poczta:&nbsp</td><td>".(json_decode($user->post_office, false)[$i]? json_decode($user->post_office, false)[$i] : "")."</td></tr>";
                echo "<tr><td>Województwo:&nbsp</td><td>".(json_decode($user->province, false)[$i]? json_decode($user->province, false)[$i] : "")."</td></tr>";
                echo "<tr><td>Kraj:&nbsp</td><td>".(json_decode($user->country, false)[$i]? json_decode($user->country, false)[$i] : "")."</td></tr>";
                echo "</table>";
                
                echo "</td></tr>";
             }
            
            echo "<tr><td>Email</td><td>$user->email</td></tr>";
            echo "<tr><td>Telefon: </td><td>".implode("<br>", array_filter(json_decode($user->telephone, false)))."</td></tr>";
            
            for($i=0; $i<count(array_filter(json_decode($user->unit_name, false))); $i++){
                if($i==0){
                    echo "<tr><td>Miejsce pracy:</td><td> ";
                } else {
                    echo "<tr><td>Miejsce pracy #".($i+1).":</td><td> ";
                }
                echo "<table>"
                    . "<tr>"
                            . "<td>"
                                    . "nazwa jednostki:&nbsp"
                            . "</td>"
                    
                            . "<td>"
                                    . (array_filter(json_decode($user->unit_name, false))[$i]? array_filter(json_decode($user->unit_name, false))[$i] : '')
                            . "</td>"
                    . "</tr>";
                if(isset($user->unit_type))
                    if(isset(array_filter(json_decode($user->unit_type, false))[$i]))
                        echo "<tr>"
                                . "<td>"
                                        . "rodzaj jednostki:&nbsp"
                                . "</td>"

                                . "<td>"
                                        . (array_filter(json_decode($user->unit_type, false))[$i]? array_filter(json_decode($user->unit_type, false))[$i] : '')
                                . "</td>"
                        . "</tr>";
                    
                    echo "<tr>"
                            . "<td>"
                                    . "oddział:&nbsp"
                            . "</td>"
                    
                            . "<td>"
                                    . (json_decode($user->branch, false)[$i]? array_filter(json_decode($user->branch, false))[$i] : '')
                            . "</td>"
                    . "</tr>"
                    . "<tr>"
                            . "<td>"
                                    . "stanowisko:&nbsp"
                            . "</td>"
                    
                            . "<td>"
                                    . (json_decode($user->position, false)[$i]? array_filter(json_decode($user->position, false))[$i] : '')
                            . "</td>"
                    . "</tr>"
                . "</table>";
                echo (json_decode($user->job_street, false)[$i]? json_decode($user->job_street, false)[$i] : "")." ".(json_decode($user->job_nr, false)[$i]? json_decode($user->job_nr, false)[$i] : ""). "<br>";
                if(isset(json_decode($user->job_postal_code, false)[$i])){
                    $postal = json_decode($user->job_postal_code, false)[$i];
//                    $postal = array_filter(json_decode($user->job_postal_code, false))[$i];
                    if($postal){
                        $postal_code[0] = substr($postal, 0,2);
                        $postal_code[1] = substr($postal, 2,5);
                        $postal = $postal_code[0]."-".$postal_code[1];
                    } else {
                        $postal = "00-000";
                    }
                } else {
                    $postal = "00-000";
                }
                echo $postal." ".(json_decode($user->job_city, false)[$i]? array_filter(json_decode($user->job_city, false))[$i] : "");
                echo "<table><tr><td>Województwo:&nbsp</td><td>".(json_decode($user->job_province, false)[$i]? json_decode($user->job_province, false)[$i] : "")."</td></tr>";
                echo "<tr><td>Kraj:&nbsp</td><td>".(json_decode($user->job_country, false)[$i]? json_decode($user->job_country, false)[$i] : "")."</td></tr>";
                echo "</table>";
                
                echo "</td></tr>";
            }
             
             
            echo "<tr><td>Jest zainteresowany szkoleniami: </td><td>".implode("<br>", array_filter(json_decode($user->interested_specialization, false)))."</td></tr>";
            echo "<tr><td>Preferowany kontakt: </td><td>";
                    if($user->contact_email) echo "e-mail<br>";
                    if($user->contact_telefon) echo "telefon<br>";
                    if($user->contact_post_job) echo "poczta do miejsca pracy<br>";
                    if($user->contact_post_home) echo "poczta na adres korespondencyjny<br>";
                    if(!$user->contact_email AND $user->contact_telefon AND $user->contact_post_job AND $user->contact_post_home) echo "Brak preferowanego kontaktu";
                        
                    
            echo"</td></tr>";
            echo "</table>";
            exit;
        }
        
        public function faktura(){
            $user = ConferenceRegisteredUsers::find(Input::get('id'));
            if(!$user){
                echo 'Brak danych';
                exit;
            } elseif(!$user->faktura) {
                echo 'Brak danych';
                exit;
            } else {
                $data = json_decode($user->faktura, true);
//                echo $data['name'];
                echo "<table>"
                        . "<tr>"
                            . "<th style='padding-right: 20px;'>"
                                        . "Nazwa firmy: "
                            . "</th>"
                            . "<td>"
                                        . $data['name']
                            . "</td>"
                        . "</tr>"
                        . "<tr>"
                            . "<th>"
                                        . "NIP: "
                            . "</th>"
                            . "<td>"
                                        . $data['nip']
                            . "</td>"
                        . "</tr>"
                        . "<tr>"
                            . "<th>"
                                        . "Adres: "
                            . "</th>"
                            . "<td>"
                                        . $data['address']." ".$data['street_name']." ".$data['house_number'];
                                        if($data['flat_number']) echo "/".$data['flat_number'];
                        echo  "</td>"
                        . "</tr>"
                            . "<th>"
                            . "</th>"
                            . "<td>"
                                        . $data['postal_code']. " ". $data['city']." ".$data['post_office']
                            . "</td>"
                        . "</tr>"
                    . "</table>";
                exit;
            }
        }

}
