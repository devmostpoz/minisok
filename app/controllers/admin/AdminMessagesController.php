<?php

class AdminMessagesController extends \BaseController {

	public function index(){
                if(!Session::get('m_sort'))
                    Session::put('m_sort', 'id');
                if(!Session::get('m_sort_konf'))
                    Session::put('m_sort_konf', '0');
                if(!Session::get('m_orderBy'))
                    Session::put('m_orderBy', 'desc');
                if(!Session::get('m_read'))
                    Session::put('m_read', '2');
                
                if(Session::get('m_read')==1)
                    if(Session::get('m_sort_konf')=='0')
                        $messages = Messages::whereStatus('0')->orderBy(Session::get('m_sort'), Session::get('m_orderBy'))->paginate(20);
                    else
                        $messages = Messages::whereConference(Session::get('m_sort_konf'))->whereStatus('0')->orderBy(Session::get('m_sort'), Session::get('m_orderBy'))->paginate(5);
                else
                    if(Session::get('m_sort_konf')=='0')
                        $messages = Messages::orderBy(Session::get('m_sort'), Session::get('m_orderBy'))->paginate(5);
                    else
                        $messages = Messages::whereConference(Session::get('m_sort_konf'))->orderBy(Session::get('m_sort'), Session::get('m_orderBy'))->paginate(5);
                  
                $confName = Conference::confName();
                
                $conferences = Conference::all();
                $conf[0] = 'Wybierz konferencje:';
                foreach($conferences as $conference){
                    $conf [$conference->id] = $conference->name; 
                }
                
                $title = 'Wiadmości użytkowników';
                return View::make('admin.pages.messages.index', compact('title', 'messages', 'confName', 'conf'));
        }
        
        public function messageStatus($id){
                $messages = Messages::find($id);
                if($messages)
                    $messages->update(["status"=> "1"]);
                
        }
        
        public function postWiadomosci(){
                Session::put('m_sort', Input::get('m_sort'));
                Session::put('m_sort_konf', Input::get('m_sort_konf'));
                Session::put('m_orderBy', Input::get('m_orderBy'));
                Session::put('m_read', Input::get('m_read')? '1':'2');
                
                return Redirect::to('admin/wiadomosci');
        }

}
