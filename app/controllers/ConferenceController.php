<?php

class ConferenceController extends \BaseController {

	public function conference($name){
            
                if (Request::is('szkoly/*'))
                    $url = 'szkoly';
                else    
                    $url = 'konferencje';
                
                $conference = Conference::whereLink($name)->whereWhat($url)->first();
                if(!$conference)
                    return Redirect::to('/');
                
                $title = $conference->title;
                if (in_array($conference->id, ["1","3","4","5"]))
                    return View::make('conference.pages.'.$conference->link, compact('title', 'conference'));
                else
                    return View::make('conference.pages.template', compact('title', 'conference'));
            
        }
        
        public function form($name){
                $conference = Conference::whereLink($name)->first();
                if(!$conference)
                    return Redirect::to('/');
                
                $title = "Formularz rejestracji.";
                
                $data['specialization'] = Conference::$specialization;
                $data['science_degree'] = Conference::$science_degree;
                $data['province'] = Conference::$province;
                $data['country'] = Conference::$country;
                $data['unit_type'] = Conference::$unit_type;
                $data['position'] = Conference::$position;
                $data['who'] = Conference::$who;
                $data['required'] = "required";
                
                return View::make('conference.formularz', compact('title', 'conference', 'data'));
        }
        
        public function create($name){
                if(!Input::get('telephone')[0]
                    || !Input::get('specialization')[0]
                    || !Input::get('adress')[0]
                    || !Input::get('street_name')[0]
                    || !Input::get('house_number')[0]
                    || !Input::get('city')[0]
                    || !Input::get('post_office')[0]
                    || !Input::get('province')[0]
                    || !Input::get('country')[0]
                    || !Input::get('unit_name')[0]
                    || !Input::get('unit_type')[0]
                    || !Input::get('branch')[0]
                    || !Input::get('position')[0]
                    || !Input::get('job_street')[0]
                    || !Input::get('job_nr')[0]
                    || !Input::get('job_city')[0]
                    || !Input::get('job_province')[0]
                    || !Input::get('job_country')[0]
                    || !Input::get('interested_specialization')[0]){
                    return Redirect::back()->withErrors("Uzupełnij poprawnie wszystkie pola.")->withInput();
                } elseif(strlen(Input::get('telephone')[0])<9){
                    return Redirect::back()->withErrors("Numer telefonu jest za krótki")->withInput();
                }
                
                $who = Input::get('who');
                if($who == 'lekarz'){
                $pwz = Input::get('pwz.0').Input::get('pwz.1').Input::get('pwz.2').Input::get('pwz.3').Input::get('pwz.4').Input::get('pwz.5').Input::get('pwz.6');
                } elseif($who == 'diagnosta') {
                    $pwz = Input::get('pwz.0').Input::get('pwz.1').Input::get('pwz.2').Input::get('pwz.3').Input::get('pwz.4');
                } elseif($who == 'pielęgniarka') {
                    $pwz = Input::get('pwz.0').Input::get('pwz.1').Input::get('pwz.2').Input::get('pwz.3').Input::get('pwz.4').Input::get('pwz.5').Input::get('pwz.6').Input::get('pwz.7').'P';
                } elseif($who == 'dietetyk'){
                    $temporary = RegisteredUser::whereWho('dietetyk')->orderBy('pwz', 'desc')->first();
                    if(!$temporary)
                        $pwz = 'D0000100';
                    else {
                        $pwz = str_replace("D", "", $temporary->pwz);
                        $pwz = $pwz + 1;
                        for($i = strlen($pwz); $i<7;$i++){
                            $pwz = '0'.$pwz;
                        }
                        $pwz = 'D'.$pwz;
                    }
                } elseif($who == 'student'){
                    $temporary = RegisteredUser::whereWho('student')->orderBy('pwz', 'desc')->first();
                    if(!$temporary)
                        $pwz = 'S0000100';
                    else {
                        $pwz = str_replace("S", "", $temporary->pwz);
                        $pwz = $pwz + 1;
                        for($i = strlen($pwz); $i<7;$i++){
                            $pwz = '0'.$pwz;
                        }
                        $pwz = 'S'.$pwz;
                    }
                }
                $pesel = Input::get('pesel.0').Input::get('pesel.1').Input::get('pesel.2').Input::get('pesel.3').Input::get('pesel.4').Input::get('pesel.5').Input::get('pesel.6').Input::get('pesel.7').Input::get('pesel.8').Input::get('pesel.9').Input::get('pesel.10');
                
                for($i=0; $i<count(Input::get('postal_code')); $i++)
                    $postal_code[] = Input::get("postal_code.$i.0").Input::get("postal_code.$i.1").Input::get("postal_code.$i.2").Input::get("postal_code.$i.3").Input::get("postal_code.$i.4");
                $postal_code = json_encode($postal_code);
                
                for($i=0; $i<count(Input::get('job_postal_code')); $i++)
                    $job_postal_code[] = Input::get("job_postal_code.$i.0").Input::get("job_postal_code.$i.1").Input::get("job_postal_code.$i.2").Input::get("job_postal_code.$i.3").Input::get("job_postal_code.$i.4");
                $job_postal_code = json_encode($job_postal_code);
                
                $row_check = [
                    'who' => Input::get('who'),
                    'sex' => Input::get('sex'), 
                    'science_degree' => Input::get('science_degree'),   
                    'name' => Input::get('name'),
                    'surname' => Input::get('surname'), 
                    'pwz' => $pwz,
                    'pesel' => $pesel,   
                    'specialization' => json_encode(Input::get('specialization')),   
                    'adress' => json_encode(Input::get('adress')),  
                    'street_name' => json_encode(Input::get('street_name')),      
                    'house_number' => json_encode(Input::get('house_number')),     
                    'flat_number' => json_encode(Input::get('flat_number')),      
                    'postal_code' => $postal_code,      
                    'city' => json_encode(Input::get('city')),
                    'post_office' => json_encode(Input::get('post_office')),      
                    'province' => json_encode(Input::get('province')),
                    'country' => json_encode(Input::get('country')), 
                    'email' => Input::get('email'),   
                    'telephone' => json_encode(Input::get('telephone')),       
                    'unit_name' => json_encode(Input::get('unit_name')),
                    'unit_type' => json_encode(Input::get('unit_type')),
                    'branch' => json_encode(Input::get('branch')),  
                    'position' => json_encode(Input::get('position')),
                    'job_street' => json_encode(Input::get('job_street')),       
                    'job_nr' => json_encode(Input::get('job_nr')),  
                    'job_postal_code' => $job_postal_code,  
                    'job_city' => json_encode(Input::get('job_city')),
                    'job_province' => json_encode(Input::get('job_province')),     
                    'job_country' => json_encode(Input::get('job_country')),      
                    'interested_specialization' => json_encode(Input::get('interested_specialization')),
                    'contact_email' => Input::get('contact_email'),
                    'contact_telefon' => Input::get('contact_telefon'),  
                    'contact_post_job' => Input::get('contact_post_job'), 
                    'contact_post_home' => Input::get('contact_post_home'),
                    'password' => Input::get('password'),
                    'password_confirm' => Input::get('password_confirm'),
                    'reg1' => Input::get('reg1')? 1: 0,
                    'reg2' => Input::get('reg2')? 1: 0,
                    'reg3' => Input::get('reg3')? 1: 0,
                ];
                
                $validator = Validator::make($row_check, RegisteredUser::$rules, RegisteredUser::$errorsTranslate);
                if ($validator->fails()) {
                    return Redirect::back()->withErrors($validator)->withInput();
                }
                
                
                $row = [
                    'who' => Input::get('who'),
                    'sex' => Input::get('sex'), 
                    'science_degree' => Input::get('science_degree'),   
                    'name' => Input::get('name'),
                    'surname' => Input::get('surname'), 
                    'pwz' => $pwz,
                    'pesel' => $pesel,   
                    'specialization' => json_encode(Input::get('specialization')),   
                    'adress' => json_encode(Input::get('adress')),  
                    'street_name' => json_encode(Input::get('street_name')),      
                    'house_number' => json_encode(Input::get('house_number')),     
                    'flat_number' => json_encode(Input::get('flat_number')),      
                    'postal_code' => $postal_code,      
                    'city' => json_encode(Input::get('city')),
                    'post_office' => json_encode(Input::get('post_office')),      
                    'province' => json_encode(Input::get('province')),
                    'country' => json_encode(Input::get('country')), 
                    'email' => Input::get('email'),   
                    'telephone' => json_encode(Input::get('telephone')),       
                    'unit_name' => json_encode(Input::get('unit_name')),          
                    'unit_type' => json_encode(Input::get('unit_type')),     
                    'branch' => json_encode(Input::get('branch')),  
                    'position' => json_encode(Input::get('position')),
                    'job_street' => json_encode(Input::get('job_street')),       
                    'job_nr' => json_encode(Input::get('job_nr')),  
                    'job_postal_code' => $job_postal_code,  
                    'job_city' => json_encode(Input::get('job_city')),
                    'job_province' => json_encode(Input::get('job_province')),     
                    'job_country' => json_encode(Input::get('job_country')),      
                    'interested_specialization' => json_encode(Input::get('interested_specialization')),
                    'contact_email' => Input::get('contact_email'),
                    'contact_telefon' => Input::get('contact_telefon'),  
                    'contact_post_job' => Input::get('contact_post_job'), 
                    'contact_post_home' => Input::get('contact_post_home'),
                    'reg1' => Input::get('reg1')? 1: 0,
                    'reg2' => Input::get('reg2')? 1: 0,
                    'reg3' => Input::get('reg3')? 1: 0,
                ];
                $user = RegisteredUser::create($row);
                $user2 = User::create([
                    'email' => Input::get('email'),
                    'password' => Input::get('password'),
                    'regular_user_id' => $user->id,
                ]);
                $user->update(['user_id'=>$user2->id]);
                if (Request::is('szkoly/*'))
                    $url = 'szkoly/'.$name.'/formularz-sukces';
                else    
                    $url = 'konferencje/'.$name.'/formularz-sukces';
                
                return Redirect::to($url);
        }
        
        public function formSuccess($name){
                $title = "Rejestracja zakończona sukcesem.";
                if (Request::is('szkoly/*'))
                    $url = 'szkoly/'.$name;
                else
                    $url = 'konferencje/'.$name;
                
                return View::make('conference.formularz_success', compact('title', 'name', 'url'));
        }
        
        public function messages($id){
                $validator = Validator::make(Input::all(), Messages::$rules, Messages::$errorsTranslate);
                
                if($validator->fails()){
                    $messages = $validator->messages();
                    foreach(array_unique($validator->messages()->all()) as $error)
                        echo $error.'<br>';
                } else {
                    $row = [
                        'name' => Input::get('name'),
                        'email' => Input::get('email'),
                        'text' => Input::get('text'),
                        'conference' => $id
                    ];
                    
                    if(Messages::create($row))
                        echo 'ok';
                    else
                        echo 'Przepraszamy, wystąpił błąd.';
                }
                
                exit;
        }
        
        public function postLogin($name){
            $registerUser =DB::select("select * from registered_users where (pwz = '".Input::get('login')."' and (who = 'diagnosta' or who = 'lekarz' or who = 'pielęgniarka' or who is null)) or 
(email = '".Input::get('login')."' and (who='student' or who = 'dietetyk')) LIMIT 1");
//                $registerUser = RegisteredUser::wherePwz(Input::get('login'))->first();
//            dd($registerUser['0']->email);
                if(!$registerUser)
                    return Redirect::back()->withInput()->withError('Zły login lub hasło.');
                
                $userdata = array(
                    'email' => $registerUser['0']->email,
                    'password' => Input::get('password')
                );
                
                if (Request::is('szkoly/*'))
                    $url = 'szkoly/'.$name.'/rejestracja';
                else
                    $url = 'konferencje/'.$name.'/rejestracja';
                
                if(Auth::attempt($userdata/*, $remember*/)){
                    return Redirect::to($url);
                } else {
                    return Redirect::back()->withInput()->withError('Zły login lub hasło.');
                }
        }
        
        public function conferenceRegister($name){           
                
                if (Request::is('szkoly/*'))
                    $url = 'szkoly';
                else    
                    $url = 'konferencje';
                $conference = Conference::whereLink($name)->whereWhat($url)->first();
                if(!$conference)
                    return Redirect::to('/');
                
                $user = RegisteredUser::find(Auth::user()->regular_user_id);
                if(!$user)
                    return Redirect::to('/');
                

                $data = '';
                $table = json_decode($conference->form);
                $title = $conference->name;
                $date = date('d-m-Y');
                if($conference->what=="konferencje"){
                    foreach($table as $key=>$val){
                        foreach($val as $k=>$v)
                           $data[] = $k;
                        break;
                    }
                    $controller = -1;
                    for($i=0;$i<count($data);$i++){
                        if(strtotime($date)<=strtotime($data[$i])){
                             $controller = $i;
                             break;
                        }
                    }
                    if($controller == -1)
                        $controller = count($data);
                    $data['controller'] = $controller;
                    
                }
                return View::make('conference.rejestracja', compact('title', 'conference', 'table', 'name', 'url', 'user'));
        }
        
        public function confirmRegister($name){ 
                $conference = Conference::whereLink($name)->first();
                $user = RegisteredUser::find(Auth::user()->regular_user_id);
                $postConf = Input::get('conference');
                if(count($postConf)==0){
                    echo "Nic nie zaznaczyłeś.";
                    exit;
                }
                $data = '';
                $table = json_decode($conference->form, true);
                $title = $conference->name." - rejestracja";
                $date = date('d-m-Y');
                
                foreach($table as $key=>$val){
                    foreach($val as $k=>$v)
                       $data[] = $k;
                    break;
                }
                
                $controller = -1;
                for($i=0;$i<=count($data);$i++){
                    if(strtotime($date)<=strtotime($data[$i])){
                         $controller = $i;
                         break;
                    }
                }
                
                if($controller == -1)
                    $controller = count($data);
                
                $i=0;
                $order = array();
                foreach($postConf as $conf){
                    $keys = array_keys($table["$conf"]);
                    $int[$i] = preg_replace('/\D+/', '', $table["$conf"][$keys[$controller]]);
                    preg_match_all('!\d+!', $table["$conf"][$keys[$controller]], $value[]);
                    $order['products'][$i]['name'] = $conf;
                    $order['products'][$i]['unitPrice'] = $int[$i].'00';
                    $order['products'][$i]['quantity'] = 1;
                    $i++;
                }
                $suma = 0;
                foreach($int as $val){
                    $suma = $suma +$val;
                }
                
//########################################################################################################################################
                
//                OpenPayU_Configuration::setEnvironment('secure');
//                OpenPayU_Configuration::setMerchantPosId('182813'); // POS ID (Checkout)
//                OpenPayU_Configuration::setSignatureKey('c1e4b5f382cb029f9ef50a21d25c0aaf'); //Second MD5 key. You will find it in admin panel.
//
//                /* path for example files*/
//                $dir = explode(basename(dirname(__FILE__)) . '/', $_SERVER['SCRIPT_NAME']);
//                $directory = $dir[0] . basename(dirname(__FILE__));
//                $url = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $dir[0];

//                define('HOME_DIR', $url);
//                define('EXAMPLES_DIR', HOME_DIR . 'examples/');
//            
//                $order['notifyUrl'] = url('ordernotify');
//                $order['continueUrl'] = url('success');
//
//                $order['customerIp'] = $_SERVER['REMOTE_ADDR'];
//                $order['merchantPosId'] = OpenPayU_Configuration::getMerchantPosId();
//                $order['description'] = $conference->name;
//                $order['currencyCode'] = 'PLN';
//                $order['totalAmount'] = $suma.'00';
//                $order['extOrderId'] = Auth::user()->id."T".$conference->id;
//                
//                $tel = json_decode($user->telephone);
//                $order['buyer']['email'] = $user->email;
//                $order['buyer']['phone'] = $tel[0];
//                $order['buyer']['firstName'] = $user->name;
//                $order['buyer']['lastName'] = $user->surname;
// 
//                $rsp = OpenPayU_Order::hostedOrderForm($order);
                
                echo "Do zapłacenia: $suma zł. ";//.$rsp;
                exit;
//######################################################################################################################################## 
                
        }
        
        public function ordered($name){
                $conference = Conference::whereLink($name)->first();
                $usr = User::find(Auth::user()->id);
                $user = RegisteredUser::find($usr->regular_user_id);
                $row = [
                    'registered_user_id' => Auth::user()->id,
                    'conference_id' => $conference->id,
                    'conference' => json_encode(Input::get('conference')),
                    'status' => '0',
                    'name' => $user->name,
                    'surname' => $user->surname,
                    'conference_name' => $conference->name,
                    'comments' => Input::get('comments')

                ];
                
                if(Input::get('faktura')==1){
                    $faktura = [
                        'name' => Input::get('name'), 
                        'nip' => Input::get('nip'), 
                        'address' => Input::get('address'), 
                        'street_name' => Input::get('street_name'), 
                        'house_number' => Input::get('house_number'), 
                        'flat_number' => Input::get('flat_number'), 
                        'postal_code' => Input::get('postal_code'), 
                        'city' => Input::get('city') ,
                        'post_office' => Input::get('post_office'),
                    ];
                    $row['faktura'] = json_encode($faktura);
                }
                $create = ConferenceRegisteredUsers::create($row);
                
                $email = Auth::user()->email;
                $data['title'] = $conference->name;
                $data['conference_id'] = $conference->id;
                $data['conference'] = json_encode(Input::get('conference'));
                $data['price'] = Input::get('toPay');
                
                Mail::send('emails.registrationConfirmationConference', $data, function($message) use ($email)
                {
                    $message->subject('Dziękujemy za rejestrację!');
                    $message->from('konferencje@symposion2015.pl', 'konferencje@symposion2015.pl');
                    $message->to($email);
                });  
        }
        
        public function postConferenceRegister($name){
                $conference = Conference::whereLink($name)->first();
                $usr = User::find(Auth::user()->id);
                $user = RegisteredUser::find($usr->regular_user_id);
                $create = ConferenceRegisteredUsers::create([
                    'registered_user_id' => Auth::user()->id,
                    'conference_id' => $conference->id,
                    'conference' => json_encode(Input::get('checkbox')),
                    'status' => '0',
                    'name' => $user->name,
                    'surname' => $user->surname,
                    'conference_name' => $conference->name,
                ]);
                
                if($create){
                    return Redirect::back()->withSuccess('Zostałeś poprawnie zapisany.');
                } else {
                    return Redirect::back()->withError('Wystąpił błąd, proszę spróbować później.');
                }
        }
        
        public function showAmoksiklav(){
            return View::make('conference.pages.amoksikla');
        }
}
