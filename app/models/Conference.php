<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Conference extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
        public $timestamps = false;
        protected $table = 'conferences';
        
        protected $hidden = array('password', 'remember_token');
        
        public static $status = [
            "0" => "w trakcie",
        ];
        
        public static $sort = [
            'id' => 'id',
            'name' => 'Imię',
            'surname' => 'Nazwisko',
            'conference_name' => 'Nazwa konferencji'
        ];
        
        protected $fillable = [
            'name', 
            'form', 
            'date_start', 
            'link', 
            'what',
            'title',
            'image',
            'description'
        ];
        
        public static $rules = [
            'link' => 'required|unique:conferences',
            'imgages' =>'mimes:jpeg,png,gif,jpg',
            'name' => 'required', 
            'form'  => 'required', 
            'what'  => 'required',
            'description' => 'required'
        ];
        public static $rulesUpdate = [
            'imgages' =>'sometimes|mimes:jpeg,png,gif,jpg',
            'name' => 'required', 
//            'form'  => 'required', 
//            'what'  => 'required',
            'description' => 'required'
        ];
        
        public static $errorsTranslate = [
            "unique" => "Istnieje już taki link w bazie danych.",
            "mimes" => "Zły format obrazka.",
            "required" => "Proszę wypełnić wymagane pola",
        ];

        public static function confName(){
            $conferences = self::all();
            foreach($conferences as $conference)
                $conf[$conference->id] = $conference->name;
            
            return $conf;
        }
        
        public function setPasswordAttribute($password)
        {
            $this->attributes['password'] = Hash::make($password);
        }
        
        public static $specialization = [
            "" => "wybierz specjalizację",
            "Alergologia" => "Alergologia",
            "Analityka kliniczna" => "Analityka kliniczna",
            "Analiza leków" => "Analiza leków",
            "Anestezjologia i intensywna terapia" => "Anestezjologia i intensywna terapia",
            "Angiologia" => "Angiologia",
            "Audiologia i foniatria" => "Audiologia i foniatria",
            "Bakteriologia" => "Bakteriologia",
            "Balneologia i medycyna fizykalna" => "Balneologia i medycyna fizykalna",
            "Chemioterapeuta" => "Chemioterapeuta",
            "Chirurgia" => "Chirurgia",
            "Chirurgia dziecięca" => "Chirurgia dziecięca",
            "Chirurgia klatki piersiowej" => "Chirurgia klatki piersiowej",
            "Chirurgia naczyniowa" => "Chirurgia naczyniowa",
            "Chirurgia ogólna" => "Chirurgia ogólna",
            "Chirurgia onkologiczna" => "Chirurgia onkologiczna",
            "Chirurgia plastyczna" => "Chirurgia plastyczna",
            "Chirurgia stomatologiczna" => "Chirurgia stomatologiczna",
            "Chirurgia szczękowo-twarzowa" => "Chirurgia szczękowo-twarzowa",
            "Choroby zakaźne" => "Choroby zakaźne",
            "Dermatologia i wenerologia" => "Dermatologia i wenerologia",
            "Diabetologia" => "Diabetologia",
            "Diagnostyka laboratoryjna" => "Diagnostyka laboratoryjna",
            "Endokrynologia" => "Endokrynologia",
            "Endoskopia" => "Endoskopia",
            "Epidemiologia" => "Epidemiologia",
            "Farmaceutyka" => "Farmaceutyka",
            "Farmakodynamika" => "Farmakodynamika",
            "Farmakologia kliniczna" => "Farmakologia kliniczna",
            "Fizjoterapeuta" => "Fizjoterapeuta",
            "Gastronenterologia" => "Gastronenterologia",
            "Gastronenterologia dziecięca" => "Gastronenterologia dziecięca",
            "Genetyka kliniczna" => "Genetyka kliniczna",
            "Geriatria" => "Geriatria",
            "Ginekologia onkologiczna" => "Ginekologia onkologiczna",
            "Hematologia" => "Hematologia",
            "Hepatolog" => "Hepatolog",
            "Hipertensjologia" => "Hipertensjologia",
            "Homeopata" => "Homeopata",
            "Immunologia kliniczna" => "Immunologia kliniczna",
            "Internista" => "Internista",
            "Kardiochirurgia" => "Kardiochirurgia",
            "Kardiologia" => "Kardiologia",
            "Kardiologia dziecięca" => "Kardiologia dziecięca",
            "Medycyna nuklearna" => "Medycyna nuklearna",
            "Medycyna ogólna" => "Medycyna ogólna",
            "Medycyna paliatywna" => "Medycyna paliatywna",
            "Medycyna pracy" => "Medycyna pracy",
            "Medycyna przemysłowa" => "Medycyna przemysłowa",
            "Medycyna ratunkowa" => "Medycyna ratunkowa",
            "Medycyna rodzinna" => "Medycyna rodzinna",
            "Medycyna sądowa" => "Medycyna sądowa",
            "Medycyna sportowa" => "Medycyna sportowa",
            "Medycyna szkolna" => "Medycyna szkolna",
            "Medycyna transportu" => "Medycyna transportu",
            "Mikrobiologia lekarska" => "Mikrobiologia lekarska",
            "Nefrologia" => "Nefrologia",
            "Neonatologia" => "Neonatologia",
            "Neurochirurgia" => "Neurochirurgia",
            "Neurologia" => "Neurologia",
            "Neurologia dziecięca" => "Neurologia dziecięca",
            "Neuropatologia" => "Neuropatologia",
            "Ochrona zdrowia" => "Ochrona zdrowia",
            "Okulistyka" => "Okulistyka",
            "Onkologia i hematologia dziecięca" => "Onkologia i hematologia dziecięca",
            "Onkologia kliniczna" => "Onkologia kliniczna",
            "Ortodoncja" => "Ortodoncja",
            "Ortopedia i traumatologia narządu ruchu" => "Ortopedia i traumatologia narządu ruchu",
            "Otorynolaryngologia" => "Otorynolaryngologia",
            "Otorynolaryngologia dziecięca" => "Otorynolaryngologia dziecięca",
            "Patomorfologia" => "Patomorfologia",
            "Pediatria" => "Pediatria",
            "Periodontologia" => "Periodontologia",
            "Pielęgniarka" => "Pielęgniarka",
            "Położnictwo i ginekologia" => "Położnictwo i ginekologia",
            "Protetyka stomatologiczna" => "Protetyka stomatologiczna",
            "Psychiatria" => "Psychiatria",
            "Psychiatria dzieci i młodzieży" => "Psychiatria dzieci i młodzieży",
            "Psychologia" => "Psychologia",
            "Psychologia dziecięca" => "Psychologia dziecięca",
            "Pulmonologia" => "Pulmonologia",
            "Radiologia i diagnostyka obrazowa" => "Radiologia i diagnostyka obrazowa",
            "Radioterapia onkologiczna" => "Radioterapia onkologiczna",
            "Rehabilitacja medyzcna" => "Rehabilitacja medyzcna",
            "Reumatologia" => "Reumatologia",
            "Seksuologia" => "Seksuologia",
            "Stażysta" => "Stażysta",
            "Stomatologia dziecięca" => "Stomatologia dziecięca",
            "Stomatologia zachowawcza z endodoncją" => "Stomatologia zachowawcza z endodoncją",
            "Toksykologia kliniczna" => "Toksykologia kliniczna",
            "Transfuzjologia kliniczna" => "Transfuzjologia kliniczna",
            "Transplantologia kliniczna" => "Transplantologia kliniczna",
            "Traumatologia" => "Traumatologia",
            "Tyreologia" => "Tyreologia",
            "Urologia" => "Urologia",
            "Urologia dziecięca" => "Urologia dziecięca"
        ];
        
        public static $country = [
                    "Albania"=> "Albania",
                    "Andora"=> "Andora",
                    "Austria"=> "Austria",
                    "Belgia"=> "Belgia",
                    "Białoruś"=> "Białoruś",
                    "Bośnia i Hercegowina"=> "Bośnia i Hercegowina",
                    "Bułgaria"=> "Bułgaria",
                    "Chorwacja"=> "Chorwacja",
                    "Cypr"=> "Cypr",
                    "Czarnogóra"=> "Czarnogóra",
                    "Czechy"=> "Czechy",
                    "Dania"=> "Dania",
                    "Estonia"=> "Estonia",
                    "Finlandia"=> "Finlandia",
                    "Francja"=> "Francja",
                    "Gibraltar G c.d."=> "Gibraltar G c.d.",
                    "Grecja"=> "Grecja",
                    "Gruzja"=> "Gruzja",
                    "Hiszpania"=> "Hiszpania",
                    "Holandia"=> "Holandia",
                    "Irlandia"=> "Irlandia",
                    "Islandia"=> "Islandia",
                    "Kazachstan"=> "Kazachstan",
                    "Liechtenstein"=> "Liechtenstein",
                    "Litwa"=> "Litwa",
                    "Luksemburg"=> "Luksemburg",
                    "Łotwa"=> "Łotwa",
                    "Macedonia"=> "Macedonia",
                    "Malta"=> "Malta",
                    "Monako"=> "Monako",
                    "Mołdawia"=> "Mołdawia",
                    "Niemcy"=> "Niemcy",
                    "Norwegia"=> "Norwegia",
                    "Polska"=> "Polska ",
                    "Portugalia"=> "Portugalia",
                    "Rosja"=> "Rosja",
                    "Rumunia"=> "Rumunia",
                    "San Marino"=> "San Marino",
                    "Serbia"=> "Serbia",
                    "Szwajcaria"=> "Szwajcaria",
                    "Szwecja"=> "Szwecja",
                    "Słowacja"=> "Słowacja",
                    "Słowenia"=> "Słowenia",
                    "Turcja"=> "Turcja",
                    "Ukraina"=> "Ukraina",
                    "Watykan"=> "Watykan",
                    "Wielka Brytania"=> "Wielka Brytania",
                    "Węgry"=> "Węgry",
                    "Włochy" => "Włochy"     
                ];
        
        public static $province = [
                    "" => "wybierz województwo",
                    "dolnośląskie" => "dolnośląskie",
                    "kujawsko-pomorskie" => "kujawsko-pomorskie",
                    "lubelskie" => "lubelskie",
                    "lubuskie" => "lubuskie",
                    "łódzkie" => "łódzkie",
                    "małopolskie" => "małopolskie",
                    "mazowieckie" => "mazowieckie",
                    "opolskie" => "opolskie",
                    "podkarpackie" => "podkarpackie",
                    "podlaskie" => "podlaskie",
                    "pomorskie" => "pomorskie",
                    "śląskie" => "śląskie",
                    "świętokrzyskie" => "świętokrzyskie",
                    "warmińsko-mazurskie" => "warmińsko-mazurskie",
                    "wielkopolskie" => "wielkopolskie",
                    "zachodniopomorskie" => "zachodniopomorskie"
        ];
                
        public static $science_degree = [
                    "prof." => "prof.",
                    "dr" => "dr",
                    "lek. med." => "lek. med.",
                    "mgr" => "mgr"
        ];
        
        public static $unit_type = [
            "" => "wybierz rodzaj",
            "szpital" => "szpital",
            "przychodnia" => "przychodnia",
            "gabinet prywatny" => "gabinet prywatny"
        ];
        
        public static $position = [
            "" => "wybierz stanowisko",
            "ordynator" => "ordynator",
            "lekarz" => "lekarz",
            "konsultant" => "konsultant",
            "manager zdrowia" => "manager zdrowia"
        ];
        
        public static $who = [
            "lekarz" => "lekarzem",
            "diagnosta" => "diagnostą",
            "pielęgniarka" => "pielęgniarką",
            "dietetyk" => "dietetykiem",
            "student" => "studentem"
        ];
}
