<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
        //public $timestamps = false;
        protected $table = 'users';
        
        protected $hidden = array('password', 'remember_token');
        
        protected $fillable = [
            'email', 
            'password', 
            'name', 
            'username', 
            'last_activity',
            'regular_user_id'
        ];
        
        public static $rules = [
            'email' => 'required|email|unique:users',
            'email_confirm' => 'required|email|same:email',
            'password' => 'required|min:4',
            'password_confirm' => 'required|same:password' 
        ];

        public static $errorsTranslate = [
            "unique" => "Adres email istnieje już w bazie.",
            "min" => "Hasło musi się składać z co najmniej 4 znaków.",
            "required" => "Proszę wypełnić wymagane pola",
            "email" => "Proszę wpisać poprawnie adres email",
            "same" => "Adresy email oraz hasła muszą być takie same"
        ];

        public function setPasswordAttribute($password)
        {
            $this->attributes['password'] = Hash::make($password);
        }

}
