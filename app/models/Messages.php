<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Messages extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
        //public $timestamps = false;
        protected $table = 'messages';
        
        
        protected $fillable = [
            'name', 
            'email', 
            'text', 
            'conference',
            'status'
        ];
        
        public static $rules = [
            'email' => 'required|email',
            'name' => 'required',
            'text' => 'required' 
        ];

        public static $errorsTranslate = [
            "required" => "Proszę wypełnić wymagane pola",
            "email" => "Proszę wpisać poprawnie adres email"
        ];
        
        public static $sort = [
            'id' => 'id',
            'name' => 'Imię nazwisko',
            'conference' => 'Konferencja',
            'email' => 'E-mail',
            'created_at' => 'Data'
        ];
}
