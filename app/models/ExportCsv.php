<?php

class ExportCsv extends \Eloquent {
    
    public static $headers1 = [
        "Nazwa konferencji",
        "Imię",
        "Nazwisko",
    ];
    
    public static $headers3 = [
        "Numer PWZ",
        "Numer PESEL",
        "Specjalizacja 1",
        "Specjalizacja 2",
        "Specjalizacja 3",
        "Specjalizacja 4",
        "Specjalizacja 5",
        "Adres mailowy",
        "Numer telefonu",
        "ulica", 
        "kod", 
        "miasto",
        "województwo",
        "kraj",
        "ulica (praca)", 
        "kod (praca)", 
        "miasto (praca)",
        "województwo (praca)",
        "kraj (praca)",
        "Faktura"
    ];    
    
    public static function headers(){
        $column = Conference::find(Session::get('sort_konf'));
        $headers = json_decode($column->form);
        
        foreach($headers as $key=>$val){
            $data[] = $key;
        }
        $data[] = "Suma";
        return array_merge(array_merge(self::$headers1, $data), self::$headers3);
    }
    
    
    public static function polishLetter($array){ //275
        $from = ["a", "ą", "b", "c", "ć", "d", "e", "ę", "f", "g", "h", "i", "j", "k", "l", "ł", "m", "n", "ń", "o", "ó", "p", "r", "s", "ś", "t", "u", "w", "y", "z", "ź", "ż",
"A", "Ą", "B", "C", "Ć", "D", "E", "Ę", "F", "G", "H", "I", "J", "K", "L", "Ł", "M", "N", "Ń", "O", "Ó", "P", "R", "S", "Ś", "T", "U", "W", "Y", "Z", "Ź", "Ż", "\r\n", "0", "1", "2","3","4","5","6","7","8","9", "V","v", " ", "@", ".", "-", "Q", "_", "/", "X", ",", "x", "+", "q"];
        foreach($array as $key=>$val){

            if(str_replace($from,"",$val)!=""){
                    $from2[0] = str_replace($from,"",$val);
                    $from2[1] = ord(str_replace($from,"",$val));
                    $from2[2] = sprintf(ord(str_replace($from,"",$val)));
            }
            if(isset($from2) && $from2[1]==194)
                    $array[$key] = iconv("UTF-8", "WINDOWS-1250//IGNORE", str_replace($from2, "", $val));
            else
                $array[$key] = iconv("UTF-8", "WINDOWS-1250//IGNORE", $val);
        }
        return $array;
        
    }
    
    public static function getConfData(){
        $conference = Conference::all();
        foreach($conference as $conf){
            $data[$conf->id]['name'] = $conf->name;
            $data[$conf->id]['what'] = $conf->what;
            $data[$conf->id]['form'] = $conf->form;
        }
        return $data;
    }
    
    public static function tel($obj){
        if($obj){
            $telArr = json_decode($obj);
            if($telArr[0])
                return $telArr[0];
            else
                return "Brak";
        } else 
            return "Brak";
    }
    
    
    public static function getUsersData(){
        $users = User::all();
        
        foreach($users as $user){
            if(isset($user->regular_user_id)){
                $usr = RegisteredUser::find($user->regular_user_id);
                if($usr){
                    $data[$user->id]['name'] = $usr->name;
                    $data[$user->id]['surname'] = $usr->surname;
                    $data[$user->id]['pwz'] = $usr->pwz;
                    $data[$user->id]['pesel'] = $usr->pesel;
                    $data[$user->id]['email'] = $usr->email;
                    $data[$user->id]['tel'] = self::tel($usr->telephone);
                    $street = json_decode($usr->adress);
                    $street = $street[0]? $street[0]: '';
                    $street1 = json_decode($usr->street_name);
                    $street1 = $street1[0]? $street1[0]: '';
                    $house_number = json_decode($usr->house_number);
                    $house_number = $house_number[0]? $house_number[0]: '';
                    $flat_number = json_decode($usr->flat_number);
                    $flat_number = $flat_number[0]? '/'.$flat_number[0]: '';
                    $data[$user->id]['ulica'] = $street.' '.$street1.' '.$house_number.$flat_number;
                    $postal = json_decode($usr->postal_code);
                    if($postal[0]){
                        $postal = $postal[0];
                        $postal_code[0] = substr($postal, 0,2);
                        $postal_code[1] = substr($postal, 2,5);
                        $postal = $postal_code[0]."-".$postal_code[1];
                    } else
                        $postal = '00-000';
                    $data[$user->id]['kod'] = $postal;
                    $city = json_decode($usr->city);
                    $data[$user->id]['miasto'] = $city[0]? $city[0]: '';
                    $province = json_decode($usr->province);
                    $data[$user->id]['wojewodztwo'] = $province[0]? $province[0]: '';
                    $country = json_decode($usr->country);
                    $data[$user->id]['kraj'] = $country[0]? $country[0]: '';
                    $street = json_decode($usr->job_street);
                    $street_number = json_decode($usr->job_nr);
                    $street_number = $street_number[0]? $street_number[0]: '';
                    $data[$user->id]['ulicaPraca'] = $street[0] ? $street[0].' '.$street_number:'';
                    $postal = json_decode($usr->job_postal_code);
                    if($postal[0]){
                        $postal = $postal[0];
                        $postal_code[0] = substr($postal, 0,2);
                        $postal_code[1] = substr($postal, 2,5);
                        $postal = $postal_code[0]."-".$postal_code[1];
                    } else
                        $postal = '00-000';
                    $data[$user->id]['kodPraca'] = $postal;
                    $city = json_decode($usr->job_city);
                    $data[$user->id]['miastoPraca'] = $city[0]? $city[0]:'';
                    $province = json_decode($usr->job_province);
                    $data[$user->id]['wojewodztwoPraca'] = $province[0]?$province[0]:'';
                    $country = json_decode($usr->job_country);
                    $data[$user->id]['krajPraca'] = $country[0]?$country[0]:'';
                    $spec = json_decode($usr->specialization);
                    for($i=0; $i<5; $i++){
                        if(isset($spec[$i]))
                            $data[$user->id]['spec'][$i+500] = $spec[$i];
                        else
                            $data[$user->id]['spec'][$i+500] = '';
                    }
                }
            }
        }
        return $data;
    }
    
    public static function regWhat($obj, $regDate){
        $array = json_decode($obj);
        $column = Conference::find(Session::get('sort_konf'));
        $headers = json_decode($column->form);
        
        $regDate = substr($regDate, 0,10);
        $newDate = date("d-m-Y", strtotime($regDate));
        
        foreach($headers as $key=>$val){
            $data[] = $key;
        }
        $i = 0;
        if($column->what == 'szkoly') {
            foreach($data as $key){
                if(in_array($key, $array))
                        $return["confName$i"] = 1;
                else
                        $return["confName$i"] = 0;
                $i++;
            }
            $return["confName$i"] = 'Darmowa';
        } else {
            foreach($headers as $key=>$val){
                if(in_array($key, $array)){
                        $value='';
                        foreach($val as $k=>$v){
                            $value = $v;
                            if(strtotime($newDate)<=strtotime($k)){
                                break;
                            } 
                        }
                        $return["confName$i"] = $value;
                } else
                        $return["confName$i"] = '0';
                $i++;
            }
            $suma = 0;
            foreach($return as $k){
                $suma = $suma + intval(str_replace(["zł", " "], "", $k));
            }
            $return["confName$i"] = $suma.' zł';
        }
        return $return;
    }
    
    public static function getData(){
        $conf = self::getConfData();
        $usr = self::getUsersData();
        $groupBy = ['conference_id', 'registered_user_id', 'conference', 'faktura'];
        $regUsr = ConferenceRegisteredUsers::whereConference_id(Session::get('sort_konf'))->groupBy($groupBy)->get();
        $i=0;
        foreach($regUsr as $reg){
            unset($d1);unset($d2);unset($d3);unset($d4);unset($d5);
            if(isset($usr[$reg->registered_user_id]['name'])){
                if(isset($conf[$reg->conference_id]['name']))
                    $d1[] = $data[$i]['confName'] = $conf[$reg->conference_id]['name']; 
                else
                    $d1[] = $data[$i]['confName'] = '';
                $d1[] = $data[$i]['name'] = $usr[$reg->registered_user_id]['name'];
                $d1[] = $data[$i]['surname'] = $usr[$reg->registered_user_id]['surname'];
                $d2 = self::regWhat($reg->conference, $reg->created_at);
                $d3[] = $data[$i]['pwz'] = $usr[$reg->registered_user_id]['pwz'];
                $d3[] = $data[$i]['pesel'] = $usr[$reg->registered_user_id]['pesel'];
                $d4 = $usr[$reg->registered_user_id]['spec'];
                $d5[] = $data[$i]['email'] = $usr[$reg->registered_user_id]['email'];
                $d5[] = $data[$i]['tel'] = $usr[$reg->registered_user_id]['tel'];
                $d5[] = $data[$i]['ulica'] = $usr[$reg->registered_user_id]['ulica'];
                $d5[] = $data[$i]['kod'] = $usr[$reg->registered_user_id]['kod'];
                $d5[] = $data[$i]['miasto'] = $usr[$reg->registered_user_id]['miasto'];
                $d5[] = $data[$i]['wojewodztwo'] = $usr[$reg->registered_user_id]['wojewodztwo'];
                $d5[] = $data[$i]['kraj'] = $usr[$reg->registered_user_id]['kraj'];
                $d5[] = $data[$i]['ulicaPraca'] = $usr[$reg->registered_user_id]['ulicaPraca'];
                $d5[] = $data[$i]['kodPraca'] = $usr[$reg->registered_user_id]['kodPraca'];
                $d5[] = $data[$i]['miastoPraca'] = $usr[$reg->registered_user_id]['miastoPraca'];
                $d5[] = $data[$i]['wojewodztwoPraca'] = $usr[$reg->registered_user_id]['wojewodztwoPraca'];
                $d5[] = $data[$i]['krajPraca'] = $usr[$reg->registered_user_id]['krajPraca'];
                $d5[] = $data[$i]['faktura'] = $reg->faktura? 'Tak': 'Nie';
                $data[$i] = array_merge(array_merge(array_merge(array_merge($d1, $d2), $d3), $d4), $d5);
            } 
            $i++;
        }
        


        return $data;
    }

}