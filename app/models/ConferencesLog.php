<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ConferencesLog extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
        protected $table = 'conferences_logs';
        
        protected $fillable = [
            'user_id',
            'conference_id',
            'name', 
            'form', 
            'date_start', 
            'link', 
            'what',
            'title',
            'image',
            'description',
            'name_2', 
            'form_2', 
            'date_start_2', 
            'link_2', 
            'what_2',
            'title_2',
            'image_2',
            'description_2',
        ];
        
}
