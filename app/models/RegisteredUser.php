<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class RegisteredUser extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
        //public $timestamps = false;
        protected $table = 'registered_users';
        
        protected $hidden = array('password', 'remember_token');
        
        protected $fillable = [     
            'sex',                          
            'science_degree',               
            'name',                         
            'surname',                      
            'pwz',                          
            'pesel',                        
            'specialization',               
            'adress',                       
            'street_name',                  
            'house_number',                 
            'flat_number',                  
            'postal_code',                  
            'city',                         
            'post_office',                  
            'province',                     
            'country',                      
            'email',                        
            'telephone',                    
            'main_job',                     
            'unit_name',    
            'unit_type',
            'branch',                       
            'position',                     
            'job_street',                   
            'job_nr',                       
            'job_postal_code',              
            'job_city',                     
            'job_province',                 
            'job_country',                  
            'interested_specialization',    
            'contact_email',                
            'contact_telefon',              
            'contact_post_job',             
            'contact_post_home',            
            'password',                     
            'reg1',                         
            'reg2',                         
            'reg3',  
            'who',
            'user_id'
        ];
        
        public static $rules = [
            'sex' => 'required',
            'science_degree' => 'required',               
            'name' => 'required',                         
            'surname' => 'required',                      
            'pwz' => 'required|unique:registered_users',                          
            'pesel' => 'required',                        
            'specialization' => 'required',               
            'adress' => 'required',                       
            'street_name' => 'required',                  
            'house_number' => 'required',                 
//            'flat_number' => 'required',                  
            'postal_code' => 'required',                  
            'city' => 'required',                         
            'post_office' => 'required',                  
            'province' => 'required',                     
            'country' => 'required',                      
            'email' => 'required|email|unique:users',                        
            'telephone' => 'required',                    
//            'main_job' => 'required',                     
            'unit_name' => 'required',  
            'unit_type' => 'required',  
            'branch' => 'required',                       
            'position' => 'required',                     
            'job_street' => 'required',                   
            'job_nr' => 'required',                       
            'job_postal_code' => 'required',              
            'job_city' => 'required',                     
            'job_province' => 'required',                 
            'job_country' => 'required',                  
            'interested_specialization' => 'required',    
            'contact_email' => 'sometimes',                
            'contact_telefon' => 'sometimes',              
            'contact_post_job' => 'sometimes',             
            'contact_post_home' => 'sometimes',            
            'password' => 'required',
            'password_confirm' => 'required|same:password',
            'reg2' => 'required'
        ];

        public static $errorsTranslate = [
            "required" => "Uzupełnij poprawnie wymagane pola.",
            "email" => "Proszę wpisać poprawnie adres email",
            "same" => "Hasła muszą być takie same",
            "unique" => "Istanieje już taki :attribute w bazie"
        ];
        
        public function setPasswordAttribute($password)
        {
            $this->attributes['password'] = Hash::make($password);
        }
        
        public static function getUserById($id){
            $usr = User::find($id);
            if($usr)
                $usr2 = RegisteredUser::find($usr->regular_user_id);
            if(isset($usr2))
                return $usr2->id;
            else 
                return '1';
        }
}

                         