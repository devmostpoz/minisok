<?php

class Image extends \Eloquent
{
    /** @type array $rules  Validation rules */
    public static $rules = ['file' => 'mimes:jpeg,png,gif|max:2000'];

    /** @type array $fillable  Fillable attributes */
    protected $fillable = ['album_id', 'name', 'description'];

   /** @type array $mimeTypes  Allowed extensions & related mime types */
    public static $mimeTypes = [
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'png' => 'image/png',
        'gif' => 'image/gif'
    ];    
    /**     Image managment
     * 
     **/
    public static function uploadFiles($files, $destinationPath) {
        $destinationPath = Image::getDestinationPath($destinationPath);
        foreach($files as $file){
            $filename = date("YmdHisu").'-'.self::slugify($file->getClientOriginalName());
            $upload_success = $file->move($destinationPath, $filename);
            chmod($destinationPath.$filename, 0777);
            if( $upload_success ) {
                self::createThumbnail($destinationPath, $filename);
                $filesName[] = $filename;
            }
        }
        
        return $filesName;
    }
    
    
    public static function upload($file, $destinationPath) 
    {
        $destinationPath = Image::getDestinationPath($destinationPath);
        $filename = date("YmdHis").'-'.self::slugify($file->getClientOriginalName());
        $upload_success = $file->move($destinationPath, $filename);
        chmod($destinationPath.$filename, 0777);
        if( $upload_success ){
            return $filename;
        } else {
           return Response::json('error', 400);
        }
    }
    

    public static function deleteImage($filename, $destinationPath)
    {
        $destinationPath = Image::getDestinationPath($destinationPath);
        
        $tmp_file = $destinationPath.'/' . $filename;
        $succes = false;
        if (File::isFile($tmp_file))    {
            $succes = File::delete($tmp_file);
        }              
        
        return $destinationPath.'/' . $filename;
    }

    public static function slugify($fileName) {
        $fileNameParts = pathinfo($fileName);
        $slugOfName = Str::slug($fileNameParts['filename']);
        $slugifiedFileName = $slugOfName . '.' . $fileNameParts['extension'];

        return $slugifiedFileName;
    }
    
    public static function getDestinationPath($destinationPath){
        $documentRoot = public_path().'/';
        if(substr($destinationPath,0,1)=='/') $destinationPath = substr($destinationPath,1);
        $destinationPath = $documentRoot.$destinationPath;
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        return $destinationPath;
    }
}