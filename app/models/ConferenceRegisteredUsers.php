<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ConferenceRegisteredUsers extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
        //public $timestamps = false;
        protected $table = 'conference_registered_users';
        
        protected $fillable = [     
            'registered_user_id',
            'conference_id',
            'conference',
            'status',
            'faktura',
            'name',
            'surname',
            'conference_name',
            'comments'
            
        ];
        
        public static $rules = [
        ];

        public static $errorsTranslate = [
            "required" => "Uzupełnij poprawnie wymagane pola.",
            "email" => "Proszę wpisać poprawnie adres email",
            "same" => "Hasła muszą być takie same",
            "unique" => "Istanieje już taki :attribute w bazie"
        ];
        
        public function scopeFaktura($query){
            return $query->whereNotNull('faktura');
        }
        
        public function scopeNamelike($query, $searchUser) {
            return $query->where('name', 'like' , "%$searchUser%")->orWhere('surname', 'like' , "%$searchUser%");
        }
}

                         